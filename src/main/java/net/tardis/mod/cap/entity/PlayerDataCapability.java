package net.tardis.mod.cap.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SyncPlayerMessage;

public class PlayerDataCapability implements IPlayerData {

    private PlayerEntity player;
    private SpaceTimeCoord coord = SpaceTimeCoord.UNIVERAL_CENTER;
    private int usedTelepathicsTicks = 0;
    private double displacement;
    private int shakingTicks = 0;
    private float shakeMagnitude = 1F;
    private int countdownTicks = 0;

    public PlayerDataCapability(PlayerEntity ent) {
        this.player = ent;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("coord", this.coord.serialize());
        tag.putInt("telepath_ticks", this.usedTelepathicsTicks);
        tag.putDouble("displacement", this.displacement);
        tag.putInt("shaking_ticks", this.shakingTicks);
        tag.putFloat("shake_magnitude", this.shakeMagnitude);
        tag.putInt("countdown_ticks", this.countdownTicks);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (nbt.contains("coord"))
            this.coord = SpaceTimeCoord.deserialize(nbt.getCompound("coord"));
    	this.usedTelepathicsTicks = nbt.getInt("telepath_ticks");
    	this.displacement = nbt.getDouble("displacement");
    	this.shakingTicks = nbt.getInt("shaking_ticks");
    	if(nbt.contains("shake_magnitude"))
    		this.shakeMagnitude = nbt.getFloat("shake_magnitude");
    	this.countdownTicks = nbt.getInt("countdown_ticks");

    }

    @Override
    public SpaceTimeCoord getDestination() {
        return coord;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {
        this.coord = coord;
    }
	
    @Override
    public void tick() {
        if (player instanceof ServerPlayerEntity) {
        	ServerPlayerEntity serverPlayer = (ServerPlayerEntity)player;
            if (serverPlayer.dimension != null && serverPlayer.dimension.getModType() == TDimensions.VORTEX) {
                if (serverPlayer.posY < 0) {
                    ServerWorld world = player.world.getServer().getWorld(DimensionType.byName(coord.getDimType()));
                    if (world != null) {
                	  if (world.getDimension().getType() == TDimensions.VORTEX_TYPE) {
                		  ServerWorld worldDest = player.world.getServer().getWorld(DimensionType.OVERWORLD);
                		  world = worldDest; //Set dimension to Overworld if they try to teleport whilst in the vortex
                      }
	                  BlockPos pos = coord.getPos();
	                  serverPlayer.fallDistance = 0;
	                  serverPlayer.teleport(world, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0);
                    }
                }
            }
        }
        else {//Client- only stuff
        	//Shake dat ass
        	if(this.shakingTicks > 0  && TConfig.CLIENT.interiorShake.get()) {
        		player.rotationYaw = Helper.getFixedRotation(player.rotationYaw) + (player.getRNG().nextFloat() - 0.5F) * this.shakeMagnitude;
        		player.rotationPitch += (player.getRNG().nextFloat() - 0.5F) * this.shakeMagnitude;
        	}
        }
        if(this.countdownTicks > 0)//Decrease countdown ticks on common to allow for sanity checks to be made in other objects
    		--this.countdownTicks;
        
        if(this.shakingTicks > 0)
        	--this.shakingTicks;
        
        if(this.usedTelepathicsTicks > 0) {
        	--this.usedTelepathicsTicks;
        	if(this.usedTelepathicsTicks == 0 && player.isServerWorld()) {
        		//If no longer connected
        		this.player.sendStatusMessage(new TranslationTextComponent("status.tardis.telepathic.not_connected"), true);
        	}
        }
    }

	@Override
	public double getDisplacement() {
		return this.displacement;
	}

	@Override
	public void calcDisplacement(BlockPos start, BlockPos finish) {
		this.displacement = Math.sqrt(start.distanceSq(finish));
	}

	@Override
	public void setShaking(int shakingTicks) {
		this.setShaking(shakingTicks, 1F);
	}
	
	@Override
	public void setShaking(int shakingTicks, float magnitude) {
		this.shakingTicks = shakingTicks;
		this.shakeMagnitude = magnitude;
	}

	@Override
	public int getShaking() {
		return this.shakingTicks;
	}
	
	@Override
	public void update() {
		if(!player.world.isRemote)
			Network.sendTo(new SyncPlayerMessage(player.getEntityId(), this.serializeNBT()), (ServerPlayerEntity)player);
	}

	@Override
	public void startCountdown(int time) {
		this.countdownTicks = time;
	}

	@Override
	public int getCountdown() {
		return this.countdownTicks;
	}

	@Override
	public void setConnectedToTARDISTicks(int connectedToTARDIS) {
		this.usedTelepathicsTicks = connectedToTARDIS;
	}

	@Override
	public int getConnectedToTARDISTicks() {
		return this.usedTelepathicsTicks;
	}
}
