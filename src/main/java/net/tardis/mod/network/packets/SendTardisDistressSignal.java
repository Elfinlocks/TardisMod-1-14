package net.tardis.mod.network.packets;

import java.util.UUID;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.tileentities.console.misc.DistressSignal;

public class SendTardisDistressSignal {

	String message;
	UUID player;
	
	public SendTardisDistressSignal(String message, UUID player) {
		this.message = message;
		this.player = player;
	}
	
	public static void encode(SendTardisDistressSignal mes, PacketBuffer buf) {
		int size = mes.message.length();
		buf.writeInt(size);
		buf.writeString(mes.message, size);
		
		boolean hasID = mes.player != null;
		buf.writeBoolean(hasID);
		
		if(hasID)
			buf.writeUniqueId(mes.player);
		
	}
	
	public static SendTardisDistressSignal decode(PacketBuffer buf) {
		int size = buf.readInt();
		String message = buf.readString(size);
		
		UUID id = null;
		boolean hasID = buf.readBoolean();
		if(hasID)
			id = buf.readUniqueId();
		
		return new SendTardisDistressSignal(message, id);
	}
	
	public static void handle(SendTardisDistressSignal mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			//If in TARDIS world
			TardisHelper.getConsoleInWorld(cont.get().getSender().getServerWorld()).ifPresent(tile -> {
				
				DistressSignal signal = new DistressSignal(mes.message, tile.getPositionInFlight());
				
				//If antenna has health
				tile.getSubsystem(AntennaSubsystem.class).ifPresent(sys -> {
					if(sys.canBeUsed()) {
						UUID id = mes.player;
						if(id == null) {
							for(ServerWorld world : cont.get().getSender().getServer().getWorlds()) {
								TardisHelper.getConsoleInWorld(world).ifPresent(other -> {
									other.addDistressSignal(signal);
								});
							}
						}
						else {
							TardisHelper.getConsole(cont.get().getSender().getServer(), id).ifPresent(other -> {
								other.addDistressSignal(signal);
							});
						}
					}
				});
			});
		});
		cont.get().setPacketHandled(true);
	}
}
