package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.network.Network;

public class RequestBotiMessage {

	BlockPos pos;
	
	public RequestBotiMessage(BlockPos pos) {
		this.pos = pos;
	}
	
	public static void encode(RequestBotiMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static RequestBotiMessage decode(PacketBuffer buf) {
		return new RequestBotiMessage(buf.readBlockPos());
	}
	
	public static void handle(RequestBotiMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TileEntity te = context.get().getSender().world.getTileEntity(mes.pos);
			if(te instanceof IBotiEnabled) {
				IBotiEnabled boti = (IBotiEnabled)te;
				if(boti.getBotiWorld() != null)
					Network.sendTo(new BOTIMessage(mes.pos, boti.getBotiWorld()), context.get().getSender());
			}
		});
		context.get().setPacketHandled(true);
	}
}
