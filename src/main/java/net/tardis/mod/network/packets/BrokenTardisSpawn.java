package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.misc.BrokenExteriorType;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class BrokenTardisSpawn {
	
	private BlockPos pos;
	
	public BrokenTardisSpawn(BlockPos pos) {
		this.pos = pos;
	}
	
	public static void encode(BrokenTardisSpawn mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static BrokenTardisSpawn decode(PacketBuffer buf) {
		return new BrokenTardisSpawn(buf.readBlockPos());
	}
	
	public static void handle(BrokenTardisSpawn mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(mes.pos);
			if(te instanceof BrokenExteriorTile) {
				BrokenExteriorType type = ((BrokenExteriorTile)te).getBrokenType();
				type.spawnHappyPart(te.getWorld(), mes.pos);
			}
		});
		cont.get().setPacketHandled(true);
	}

}
