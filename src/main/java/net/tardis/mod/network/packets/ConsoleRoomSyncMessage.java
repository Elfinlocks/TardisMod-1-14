package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.ars.ConsoleRoom;

public class ConsoleRoomSyncMessage {

	private List<ConsoleRoom> consoleRooms = new ArrayList<ConsoleRoom>();

	public ConsoleRoomSyncMessage(List<ConsoleRoom> consoleRooms) {
		this.consoleRooms.clear(); //Clear the client's list and readd the entries from the server
        this.consoleRooms.addAll(consoleRooms);
	}

	public static void encode(ConsoleRoomSyncMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.consoleRooms.size());
        for(ConsoleRoom consoleRoom : mes.consoleRooms) {
            buf.writeCompoundTag(consoleRoom.serialize());
        }
	}
	
	public static ConsoleRoomSyncMessage decode(PacketBuffer buf) {
		int sizeConsoleRoom = buf.readInt();
		List<ConsoleRoom> consoleroom = new ArrayList<ConsoleRoom>();

		for(int i = 0; i < sizeConsoleRoom; ++i) {
			consoleroom.add(ConsoleRoom.deserialize(buf.readCompoundTag()));
		}
		
		return new ConsoleRoomSyncMessage(consoleroom);
	}
	
	public static void handle(ConsoleRoomSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			ConsoleRoom.REGISTRY.clear();
			for (ConsoleRoom interior : mes.consoleRooms){
				ConsoleRoom.register(interior, interior.getRegistryName()); //Use Register method to set registryname.
			}
		});
	}
}
