package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.registries.TardisForgeRegistries;

public class MissionUpdateMessage {

	BlockPos pos;
	ResourceLocation key;
	int range;
	CompoundNBT nbt;
	
	public MissionUpdateMessage(BlockPos pos, ResourceLocation key, int range, CompoundNBT nbt) {
		this.pos = pos;
		this.key = key;
		this.range = range;
		this.nbt = nbt;
	}
	
	public MissionUpdateMessage(MiniMission mis) {
		this(mis.getPos(), mis.getType().getRegistryName(), mis.getRange(), mis.serializeNBT());
	}
	
	public static void encode(MissionUpdateMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeResourceLocation(mes.key);
		buf.writeInt(mes.range);
		buf.writeCompoundTag(mes.nbt);
	}
	
	public static MissionUpdateMessage decode(PacketBuffer buf) {
		return new MissionUpdateMessage(buf.readBlockPos(), buf.readResourceLocation(), buf.readInt(), buf.readCompoundTag());
	}
	
	public static void handle(MissionUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> doClientWork(mes));
		
		cont.get().setPacketHandled(true);
	}
	
	@OnlyIn(Dist.CLIENT)
	public static void doClientWork(MissionUpdateMessage mes) {
		Minecraft.getInstance().world.getCapability(Capabilities.MISSION).ifPresent(misCap -> {
			MiniMission mis = misCap.getMissionForPos(mes.pos);
			if(mis != null)
				mis.deserializeNBT(mes.nbt);
			else {
				mis = TardisForgeRegistries.MISSIONS.getValue(mes.key).create(Minecraft.getInstance().world, mes.pos, mes.range);
				mis.deserializeNBT(mes.nbt);
				misCap.addMission(mis);
			}
		});
	}
}
