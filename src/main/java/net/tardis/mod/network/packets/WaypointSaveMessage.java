package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointSaveMessage {

	private static final String SUCCESS = "status.tardis.waypoint.saved";
	private static final String FAIL = "status.tardis.waypoint.save_fail";
	private static final String FAIL_DIM = "status.tardis.waypoint.save_fail_dimension";
	private String name;
	
	public WaypointSaveMessage(String name) {
		this.name = name;
	}
	
	public static void encode(WaypointSaveMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.name.length());
		buf.writeString(mes.name, mes.name.length());
	}
	
	public static WaypointSaveMessage decode(PacketBuffer buf) {
		int len = buf.readInt();
		return new WaypointSaveMessage(buf.readString(len));
	}
	
	public static void handle(WaypointSaveMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			
			TardisHelper.getConsoleInWorld(cont.get().getSender().getServerWorld()).ifPresent(console -> {
				
				if(Helper.canTravelToDimension(console.getDimension())) {
					boolean success = false;
					
					for(TileEntity te : cont.get().getSender().getServerWorld().loadedTileEntityList) {
						
						SpaceTimeCoord coord = new SpaceTimeCoord(console.getDimension(), console.getLocation(), console.getExteriorDirection());
						coord.setName(mes.name);
						
						if(te instanceof WaypointBankTile) {
							success = ((WaypointBankTile)te).addWaypoint(coord);
							if(success) {
								cont.get().getSender().sendStatusMessage(new TranslationTextComponent(SUCCESS, mes.name), true);
								break;
							}
						}
					}
					
					if(!success)
						cont.get().getSender().sendStatusMessage(new TranslationTextComponent(FAIL, mes.name), true);
				}
				else cont.get().getSender().sendStatusMessage(new TranslationTextComponent(FAIL_DIM, mes.name), true);
				
			});
			
		});
		cont.get().setPacketHandled(true);
	}
}
