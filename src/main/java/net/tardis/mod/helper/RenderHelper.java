package net.tardis.mod.helper;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

/**
 * Created by Swirtzly
 * on 25/03/2020 @ 13:03
 */
public class RenderHelper {

    private static float lastBrightnessX = GLX.lastBrightnessX;
    private static float lastBrightnessY = GLX.lastBrightnessY;

    public static void setLightmapTextureCoords(float x, float y) {
        lastBrightnessX = GLX.lastBrightnessX;
        lastBrightnessY = GLX.lastBrightnessY;
        GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, x, y);
    }

    public static Minecraft mc() {
	    return Minecraft.getInstance();
	}

    /**
     * Gets partialTicks for client side rendering
     * @return
     */
    public static float getRenderPartialTicks() {
	    return mc().getRenderPartialTicks();
	}

    public static void restoreLightMap() {
        GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, lastBrightnessX, lastBrightnessY);
    }

    public static void drawGlowingLine(Vec3d start, Vec3d end, float thickness, Vec3d color, float alpha) {
        if (start == null || end == null)
            return;

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bb = tessellator.getBuffer();
        int smoothFactor = Minecraft.getInstance().gameSettings.ambientOcclusionStatus.func_216572_a();
        int layers = 10 + smoothFactor * 20;

        GlStateManager.pushMatrix();
        GlStateManager.disableTexture();
        start = start.scale(-1D);
        end = end.scale(-1D);
        GlStateManager.translated(-start.x, -start.y, -start.z);
        start = end.subtract(start);
        end = end.subtract(end);

        {
            double x = end.x - start.x;
            double y = end.y - start.y;
            double z = end.z - start.z;
            double diff = MathHelper.sqrt(x * x + z * z);
            float yaw = (float) (Math.atan2(z, x) * 180.0D / Math.PI) - 90.0F;
            float pitch = (float) -(Math.atan2(y, diff) * 180.0D / Math.PI);
            GlStateManager.rotatef(-yaw, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotatef(pitch, 1.0F, 0.0F, 0.0F);
        }

        for (int layer = 0; layer <= layers; ++layer) {
            if (layer < layers) {
                GlStateManager.color4f((float) color.x, (float) color.y, (float) color.z, 1.0F / layers / 2);
                GlStateManager.depthMask(false);
            } else {
                GlStateManager.color4f(1.0F, 1.0F, 1.0F, alpha); // SUB does this actually do anything? We're always passing in an alpha of 0...
                GlStateManager.depthMask(true);
            }
            double size = thickness + (layer < layers ? layer * (1.25D / layers) : 0.0D);
            double d = (layer < layers ? 1.0D - layer * (1.0D / layers) : 0.0D) * 0.1D;
            double width = 0.0625D * size;
            double height = 0.0625D * size;
            double length = start.distanceTo(end) + d;

            bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);
            bb.pos(-width, height, length).endVertex();
            bb.pos(width, height, length).endVertex();
            bb.pos(width, height, -d).endVertex();
            bb.pos(-width, height, -d).endVertex();
            bb.pos(width, -height, -d).endVertex();
            bb.pos(width, -height, length).endVertex();
            bb.pos(-width, -height, length).endVertex();
            bb.pos(-width, -height, -d).endVertex();
            bb.pos(-width, -height, -d).endVertex();
            bb.pos(-width, -height, length).endVertex();
            bb.pos(-width, height, length).endVertex();
            bb.pos(-width, height, -d).endVertex();
            bb.pos(width, height, length).endVertex();
            bb.pos(width, -height, length).endVertex();
            bb.pos(width, -height, -d).endVertex();
            bb.pos(width, height, -d).endVertex();
            bb.pos(width, -height, length).endVertex();
            bb.pos(width, height, length).endVertex();
            bb.pos(-width, height, length).endVertex();
            bb.pos(-width, -height, length).endVertex();
            bb.pos(width, -height, -d).endVertex();
            bb.pos(width, height, -d).endVertex();
            bb.pos(-width, height, -d).endVertex();
            bb.pos(-width, -height, -d).endVertex();
            tessellator.draw();
        }

        GlStateManager.enableTexture();
        GlStateManager.popMatrix();
    }

    public static void setupRenderLightning() {
        GlStateManager.pushMatrix();
        GlStateManager.disableTexture();
        GlStateManager.disableLighting();
        GlStateManager.disableCull();
        GlStateManager.enableBlend();
        GlStateManager.enableAlphaTest();
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.SourceFactor.CONSTANT_ALPHA.value);
        GlStateManager.alphaFunc(GL11.GL_GREATER, 0.003921569F);
        setLightmapTextureCoords(240, 240);
    }

    public static void finishRenderLightning() {
        restoreLightMap();
        GlStateManager.enableLighting();
        GlStateManager.enableTexture();
        GlStateManager.alphaFunc(GL11.GL_GREATER, 0.1F);
        GlStateManager.disableBlend();
        GlStateManager.disableAlphaTest();
        GlStateManager.popMatrix();
    }

	public static void renderInsideBox(int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		
		float per = Minecraft.getInstance().world.getGameTime() % 200 / 200.0F;
		float maxU = 1, maxV = per + 0.25F;
		float minV = per;
		
		bb.pos(minX, minY, minZ).tex(0, minV).endVertex();
		bb.pos(maxX, minY, minZ).tex(maxU, minV).endVertex();
		bb.pos(maxX, maxY, minZ).tex(maxU, maxV).endVertex();
		bb.pos(minX, maxY, minZ).tex(0, maxV).endVertex();
		
		bb.pos(minX, minY, maxZ).tex(0, minV).endVertex();
		bb.pos(minX, maxY, maxZ).tex(0, maxV).endVertex();
		bb.pos(maxX, maxY, maxZ).tex(maxU, maxV).endVertex();
		bb.pos(maxX, minY, maxZ).tex(maxU, minV).endVertex();
		
		bb.pos(minX, minY, minZ).tex(0, minV).endVertex();
		bb.pos(minX, maxY, minZ).tex(0, maxV).endVertex();
		bb.pos(minX, maxY, maxZ).tex(maxU, maxV).endVertex();
		bb.pos(minX, minY, maxZ).tex(maxU, minV).endVertex();
		
		bb.pos(maxX, minY, minZ).tex(0, minV).endVertex();
		bb.pos(maxX, minY, maxZ).tex(maxU, minV).endVertex();
		bb.pos(maxX, maxY, maxZ).tex(maxU, maxV).endVertex();
		bb.pos(maxX, maxY, minZ).tex(0, maxV).endVertex();
		
		Tessellator.getInstance().draw();
	}

    public static void drawSelectionBoxMask(AxisAlignedBB box, float red, float green, float blue, float alpha) {
        drawMask(box.minX, box.minY, box.minZ, box.maxX, box.maxY, box.maxZ, red, green, blue, alpha);
    }

    public static void drawMask(double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR);
        drawMask(bufferbuilder, minX, minY, minZ, maxX, maxY, maxZ, red, green, blue, alpha);
        tessellator.draw();
    }

    public static void drawMask(BufferBuilder b, double minX, double minY, double minZ, double maxX, double maxY, double maxZ, float red, float green, float blue, float alpha) {
        // up
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();

        // down
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();

        // north
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();

        // south
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();

        // east
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(maxX, minY, minZ).color(red, green, blue, alpha).endVertex();

        // west
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, maxZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, maxY, minZ).color(red, green, blue, alpha).endVertex();
        b.pos(minX, minY, minZ).color(red, green, blue, alpha).endVertex();
    }

	public static void renderAABB(AxisAlignedBB box, float red, float green, float blue, float alpha) {
		GlStateManager.disableTexture();
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
	
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		
		bb.pos(box.minX, box.minY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.minY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.maxY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.maxY, box.maxZ).color(red, green, blue, alpha).endVertex();
		
		bb.pos(box.minX, box.minY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.maxY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.maxY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.minY, box.minZ).color(red, green, blue, alpha).endVertex();
		
		bb.pos(box.minX, box.minY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.minY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.maxY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.maxY, box.minZ).color(red, green, blue, alpha).endVertex();
		
		bb.pos(box.maxX, box.minY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.maxY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.maxY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.minY, box.maxZ).color(red, green, blue, alpha).endVertex();
		
		bb.pos(box.minX, box.minY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.minY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.minY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.minY, box.maxZ).color(red, green, blue, alpha).endVertex();
		
		bb.pos(box.minX, box.maxY, box.minZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.minX, box.maxY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.maxY, box.maxZ).color(red, green, blue, alpha).endVertex();
		bb.pos(box.maxX, box.maxY, box.minZ).color(red, green, blue, alpha).endVertex();
		
		Tessellator.getInstance().draw();
		GlStateManager.enableTexture();
	}

	public static void renderSineWave(int x, int y, int width, int height, double amp) {
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);
		
		double t = System.currentTimeMillis() * 0.02;
		
		for(int point = 0; point < width; point++) {
			
			bb.pos(x + (point * 0.75), y + Math.sin((point + t) * amp) * height, 0).color(1F, 1, 1, 1).endVertex();
			bb.pos(x + (point + 1) * 0.75, y + Math.sin((point + t + 1) * amp) * height, 0).color(1F, 1, 1, 1).endVertex();
		}
		
		Tessellator.getInstance().draw();
	}
}
