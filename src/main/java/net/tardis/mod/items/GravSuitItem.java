package net.tardis.mod.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.tardis.api.space.items.IGravArmor;

public class GravSuitItem extends SpaceSuitItem implements IGravArmor{

	public GravSuitItem(EquipmentSlotType slot) {
		super(slot);
	}

	@Override
	public boolean useNormalGrav(LivingEntity entity, ItemStack stack) {
		BlockPos pos = entity.getPosition();
		for(int i = 0; i < 16; ++i) {
			if(entity.world.getBlockState(pos.down(i)).isSolid())
				return true;
		}
		return false;
	}

}
