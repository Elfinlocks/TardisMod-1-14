package net.tardis.mod.ars;


import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.item.LeashKnotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IFutureReloadListener;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Unit;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.LogicalSidedProvider;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerAboutToStartEvent;
import net.minecraftforge.items.CapabilityItemHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.DataCrystalItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleRoomSyncMessage;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ReclamationTile;
import net.tardis.mod.tileentities.WaypointBankTile;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class ConsoleRoom {

	public static HashMap<ResourceLocation, ConsoleRoom> REGISTRY = new HashMap<ResourceLocation, ConsoleRoom>();
	
	public static ConsoleRoom STEAM;
	public static ConsoleRoom JADE;
	public static ConsoleRoom NAUTILUS;
	public static ConsoleRoom OMEGA;
	public static ConsoleRoom ALABASTER;
	public static ConsoleRoom CORAL;
	public static ConsoleRoom METALLIC;
	public static ConsoleRoom ARCHITECT;
	public static ConsoleRoom TOYOTA;
	public static ConsoleRoom PANAMAX;
	public static ConsoleRoom TRAVELER;
	public static ConsoleRoom ENVOY;

	//Broken exteriors
	public static ConsoleRoom BROKEN_STEAM;
	public static ConsoleRoom BROKEN_PANAMAX;
	
	//Debug Use Only
	public static ConsoleRoom DEBUG_CLEAN;

	private ResourceLocation registryName;
	private ResourceLocation texture;
	private BlockPos offset;
	private ResourceLocation filePath;
	private TranslationTextComponent displayName;
	private boolean unlockedByDefault = true;
	private boolean isDatapack = false; //If this is a datapack entry. May be useful for modifying behaviour of datapack entries
	private boolean has_remote_url = false;
	private String remote_url = "";

	public ConsoleRoom(BlockPos offset, boolean unlockedByDefault, ResourceLocation filePath, ResourceLocation texture, String translationKey, boolean isDatapack, String imageUrl, boolean has_remote_url) {
		this.has_remote_url = has_remote_url;
		this.remote_url = imageUrl;
		this.offset = offset;
		this.filePath = filePath;
		this.texture = texture;
		this.displayName = new TranslationTextComponent(translationKey);
		this.unlockedByDefault = unlockedByDefault;
		this.isDatapack = isDatapack;
	}

	/**
	 * Convenience Constructor for non-datapack entries
	 * @param offset
     * @param unlockedByDefault
     * @param fileName
     * @param texture
     */
	public ConsoleRoom(BlockPos offset, boolean unlockedByDefault, String fileName, String texture) {
		this(offset, unlockedByDefault,
				new ResourceLocation(Tardis.MODID, "tardis/structures/" + fileName),
				new ResourceLocation(Tardis.MODID, "textures/gui/interiors/" + texture + ".png"),
				"interiors.tardis." + fileName, false, "", false);
	}

	public ConsoleRoom setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}
	
	public ResourceLocation getRegistryName() {
		return registryName;
	}
	
	public ResourceLocation getTexture() {
		return this.texture;
	}
	
	public TranslationTextComponent getDisplayName() {
		return this.displayName;
	}

    /**
     * If this console room is unlocked by default
     * @return
     */
	public boolean isUnlockedByDefault() {
		return this.unlockedByDefault;
	}

	/**
	 * Check if this console room is from a datapack
	 *
	 * @implSpec If true, render the TranslationTextComponent key in InteriorChangeScreen, else use the formatted text
	 */
	public boolean isDataPack() {
		return this.isDatapack;
	}

	public String getImageUrl() {
		return remote_url;
	}

	public boolean isUsingRemoteImage() {
		return has_remote_url;
	}
	
    /**Registers all ConsoleRooms before the Server starts so that the ConsoleTile won't have a null ConsoleRoom when the server starts and loads the world
     */
	@SubscribeEvent
	public static void registerConsoleRooms(FMLServerAboutToStartEvent event) {
	    boolean isServer = true;
        try {
            LogicalSidedProvider.INSTANCE.get(LogicalSide.SERVER);
        }
        catch(NullPointerException e){
            isServer = false;
        }
        if (isServer == true) {// if we're on the server and we are configured to send syncing packets, send syncing packets
            event.getServer().getResourceManager().addReloadListener(new IFutureReloadListener() {
			@Override
			public CompletableFuture<Void> reload(IStage stage, IResourceManager resourceManager, IProfiler preparationsProfiler, IProfiler reloadProfiler, Executor backgroundExecutor, Executor gameExecutor) {
				    return stage.markCompleteAwaitingOthers(Unit.INSTANCE).thenRunAsync(() -> reloadOrRegisterConsoleRooms(event.getServer())).thenAccept(action -> {
					    Network.sendPacketToAll(new ConsoleRoomSyncMessage(new ArrayList<ConsoleRoom>(ConsoleRoom.REGISTRY.values())));
					    Tardis.LOGGER.log(Level.INFO, "Reloaded ConsoleRooms for all players!");
				    });
			    } //Allow console rooms to be reloaded with vanilla /reload command
            });
            reloadOrRegisterConsoleRooms(event.getServer()); //Register all Console Rooms
        }
	}

	public static ConsoleRoom register(ConsoleRoom room, ResourceLocation registryName) {
		REGISTRY.put(registryName, room.setRegistryName(registryName));
		if(registryName == null)
			throw new NullPointerException();
		return room;
	}

	public static ConsoleRoom register(ConsoleRoom room, String registryName) {
		return ConsoleRoom.register(room, new ResourceLocation(Tardis.MODID, registryName));
	}
	
	public static void registerCoreConsoleRooms() {
	    ConsoleRoom.REGISTRY.clear();
	    Tardis.LOGGER.log(Level.INFO, "Cleared ConsoleRoom Registry!");
        ConsoleRoom.STEAM = register(new ConsoleRoom(new BlockPos(13, 8, 30), true, "interior_steam", "steam"), "interior_steam");
        ConsoleRoom.JADE = register(new ConsoleRoom(new BlockPos(9, 12, 30), true, "interior_jade", "jade"), "interior_jade");
        ConsoleRoom.NAUTILUS = register(new ConsoleRoom(new BlockPos(14, 13, 30), false, "interior_nautilus", "nautilus"), "nautilus");
        ConsoleRoom.OMEGA = register(new ConsoleRoom(new BlockPos(14, 15, 30), true, "interior_omega", "omega"), "omega"); //Enable this as a test
        ConsoleRoom.ALABASTER = register(new ConsoleRoom(new BlockPos(15, 8, 30), true, "interior_alabaster", "alabaster"), "alabaster");
        ConsoleRoom.ARCHITECT = register(new ConsoleRoom(new BlockPos(14, 12, 30), true, "interior_architect", "architect"), "architect");
        ConsoleRoom.CORAL = register(new ConsoleRoom(new BlockPos(16, 7, 30), false, "interior_coral", "coral"), "coral");
        ConsoleRoom.PANAMAX = register(new ConsoleRoom(new BlockPos(14, 11, 30), false, "interior_panamax", "panamax"), "panamax");
        ConsoleRoom.TOYOTA = register(new ConsoleRoom(new BlockPos(20, 7, 30), false, "interior_toyota", "toyota"), "toyota");

        ConsoleRoom.TRAVELER = register(new ConsoleRoom(new BlockPos(14, 8, 30), false, "interior_traveler", "traveler"), "traveler");
        ConsoleRoom.ENVOY = register(new ConsoleRoom(new BlockPos(19, 3, 30), false, "interior_envoy", "envoy"), "envoy");
        
        ConsoleRoom.DEBUG_CLEAN = register(new ConsoleRoom(new BlockPos(2, 1, 4), false, "interior_debug_clean", "debug"), "debug");
        
        ConsoleRoom.BROKEN_STEAM = register(new ConsoleRoom(new BlockPos(13, 8, 30), false, "interior_broken_steam", "steam"), "broken_steam");
        ConsoleRoom.BROKEN_PANAMAX = register(new ConsoleRoom(new BlockPos(14, 11, 30), false, "panamax_broken", "panamax_broken"), "panamax_broken");
        REGISTRY.values().forEach(interior -> {
            Tardis.LOGGER.log(Level.INFO, "Registered Console Room: " + interior.getRegistryName());
        });
	}
	
	public static void reloadOrRegisterConsoleRooms(MinecraftServer server) {
	    registerCoreConsoleRooms();
		String registryFileName = "console_rooms";
		String filePathKey = "schematic_resource_location";
		/**This is actually the key we use for the displayName TranslationTextComponent. 
		 * Why? Because we can't use language packs when serialising to the server, so we take the translation's key value when we want to render
		 * We call this variable display_name because users won't know that it's actually a key, not a value.
		 */
		String displayNameKey = "display_name";
		String nameSpacedIDKey = "namespaced_id";
		String imageFilePath = "image_resource_location";
		String unlockedKey = "unlocked_by_default";
		String xOffsetKey = "x_offset";
		String yOffsetKey = "y_offset";
		String zOffsetKey = "z_offset";

		String usingRemoteImage = "using_remote_image";
		String remote_image = "remote_image";

		try {
			/**Read all datapacks that have this file structure.
			 * Use getAllResources instead of getAllResourceLocations. Returns an IResource to prevent users from creating weird issues that result from unusual file structures.
			 */
			List<IResource> resources = server.getResourceManager().getAllResources(new ResourceLocation(Tardis.MODID, "structures/tardis/structures/" + registryFileName + ".json"));
			resources.forEach(resource -> {
				try {
					JsonArray root = new JsonParser().parse(new InputStreamReader(resource.getInputStream())).getAsJsonArray();
					for (JsonElement ele : root) {
						JsonObject obj = ele.getAsJsonObject();
						ResourceLocation filePath = new ResourceLocation(obj.get(filePathKey).getAsString().toLowerCase()); //Make these lowercase in case user uses uppercase or doesn't read documentation properly
						ResourceLocation nameSpacedID = new ResourceLocation(obj.get(nameSpacedIDKey).getAsString().toLowerCase());
						ResourceLocation imageFile = new ResourceLocation(obj.get(imageFilePath).getAsString().toLowerCase());
						String translationKey = obj.get(displayNameKey).getAsString();
						int xOffset = obj.get(xOffsetKey).getAsInt();
						int yOffset = obj.get(yOffsetKey).getAsInt();
						int zOffset = obj.get(zOffsetKey).getAsInt();
						boolean isRemoteImage = false;
						if (obj.has(usingRemoteImage)) {
							isRemoteImage = obj.get(usingRemoteImage).getAsBoolean();
						}
						String remote_image_url = obj.has(remote_image) ? obj.get(remote_image).getAsString() : "";
						boolean unlocked = obj.get(unlockedKey).getAsBoolean();
						register(new ConsoleRoom(new BlockPos(xOffset, yOffset, zOffset), unlocked, filePath, imageFile, translationKey, true, remote_image_url, isRemoteImage), nameSpacedID);
						Tardis.LOGGER.log(Level.INFO, "Registered DataPack Console Room: " + nameSpacedID);
				}
				resource.close();
				} catch (Exception e) {
					Tardis.LOGGER.catching(Level.ERROR, e);
				}
			});
		} catch (Exception e) {
			Tardis.LOGGER.catching(Level.ERROR, e);
			Tardis.LOGGER.log(Level.INFO, "If you are NOT loading any datapack interiors, ignore this error.");
		}
	}
	
	public HashMap<ResourceLocation, ConsoleRoom> getRegistry(){
		return REGISTRY;
	}
	
	public CompoundNBT serialize() {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("schematic_resource_location", this.filePath.toString());
		tag.putString("namespaced_id", this.registryName.toString());
		tag.putString("display_name", this.displayName.getKey()); //Use the translation key instead of formatted text if this interior is a datapack type
		tag.putString("image_resource_location", this.texture.toString());
		tag.putBoolean("unlocked_by_default", this.unlockedByDefault);
		tag.putLong("offset", this.offset.toLong());
		tag.putBoolean("is_datapack", this.isDatapack);
		tag.putBoolean("has_remote_url", has_remote_url);
		tag.putString("remote_url", remote_url);
		return tag;
    }

    public static ConsoleRoom deserialize(CompoundNBT tag) {
		String fallBackImageRLKey = "tardis:textures/gui/interiors/generic_preview.png";

		ResourceLocation file = new ResourceLocation(tag.getString("schematic_resource_location"));
		ResourceLocation registryName = new ResourceLocation(tag.getString("namespaced_id"));
		ResourceLocation imageFile = new ResourceLocation(tag.getString("image_resource_location").isEmpty() || tag.getString("image_resource_location").contentEquals("minecraft:") ? fallBackImageRLKey : tag.getString("image_resource_location"));
		String name = tag.getString("display_name");
		BlockPos offset = BlockPos.fromLong(tag.getLong("offset"));
		boolean unlocked = tag.getBoolean("unlocked_by_default");
		boolean isDatapack = tag.getBoolean("is_datapack");
		String remote_url = tag.getString("remote_url");
		boolean hasRemoteUrl = tag.getBoolean("has_remote_url");
		return new ConsoleRoom(offset, unlocked, file, imageFile, name, isDatapack, remote_url, hasRemoteUrl).setRegistryName(registryName);
	}

    
	public void spawnConsoleRoom(ServerWorld world, boolean overrideStructure) {
		ConsoleTile console = null;
		CompoundNBT consoleData = null;
		BlockState consoleState = null;
		
		int radius = 30;

		//Save the console
		if(world.getTileEntity(TardisHelper.TARDIS_POS) instanceof ConsoleTile) {
			console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
			consoleData = console.serializeNBT();
			consoleState = world.getBlockState(TardisHelper.TARDIS_POS);
		}

		List<ItemStack> allInvs = new ArrayList<ItemStack>();
		
		boolean hasCorridors = world.getBlockState(new BlockPos(0, 129, -30)).getBlock() != TBlocks.corridor_spawn;

		//Delete all old blocks
		BlockPos clearRadius = new BlockPos(radius, radius, radius);
		BlockPos.getAllInBox(TardisHelper.TARDIS_POS.subtract(clearRadius), TardisHelper.TARDIS_POS.add(clearRadius)).forEach((pos) -> {
			TileEntity tile = world.getTileEntity(pos);
			if(tile instanceof IInventory) {
				IInventory inv = (IInventory)tile;
				for(int i = 0; i < inv.getSizeInventory(); ++ i) {
					ItemStack stack = inv.getStackInSlot(i);
					if(!stack.isEmpty())
						allInvs.add(stack.copy());
				}
			}
			else if(tile != null) {
				//Handle vanilla containers and those that use item handler capability
				tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
					for(int i = 0; i < cap.getSlots(); ++i) {
						ItemStack stack = cap.getStackInSlot(i);
						if(!stack.isEmpty())
							allInvs.add(stack.copy());
					}
				});
			}
			//Waypoint saving
			if(tile instanceof WaypointBankTile) {
				List<SpaceTimeCoord> stored = Lists.newArrayList(((WaypointBankTile)tile).getWaypoints());
				stored.removeIf(coord -> coord.equals(SpaceTimeCoord.UNIVERAL_CENTER));
				
				if(!stored.isEmpty()) {
					ItemStack stack = new ItemStack(TItems.DATA_CRYSTAL);
					DataCrystalItem.setStoredWaypoints(stack, stored);
					allInvs.add(stack);
				}
			}
			
			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
		});


		//Kill all non-living entities
		AxisAlignedBB killBox = new AxisAlignedBB(-radius, -radius, -radius, radius, radius, radius).offset(TardisHelper.TARDIS_POS);
		BlockPos tpPos = TardisHelper.TARDIS_POS.offset(Direction.NORTH);
		for(Entity entity : world.getEntitiesWithinAABB(Entity.class, killBox)){
			
			if(entity.getTags().contains("tardis_tardis_killed"))
				entity.remove();
			
			if(!(entity instanceof LivingEntity))
				entity.remove();
			if(entity instanceof PlayerEntity)
				entity.setPositionAndUpdate(tpPos.getX() + 0.5, tpPos.getY() + 0.5, tpPos.getZ() + 0.5);
			if(entity instanceof ArmorStandEntity) {
				for (ItemStack armor : entity.getArmorInventoryList())
					allInvs.add(armor.copy());
				entity.remove();
			}
			if (entity instanceof ItemFrameEntity) {
				allInvs.add(((ItemFrameEntity)entity).getDisplayedItem());
				entity.remove();
			}
			if (entity instanceof LeashKnotEntity) {
				allInvs.add(new ItemStack(Items.LEAD));
				entity.remove();
			}
		}

		//Spawn console room
		Template temp = world.getStructureTemplateManager().getTemplate(filePath);
		temp.addBlocksToWorld(world, TardisHelper.TARDIS_POS.subtract(this.offset), new PlacementSettings().setIgnoreEntities(false));

		//Remove End Cap if corridors have already been generated
		if(hasCorridors && !overrideStructure) {
			BlockPos corridor = new BlockPos(0, 129, -30);
			for(int x = -1; x < 2; ++x) {
				for(int y = -1; y < 2; ++y) {
					world.setBlockState(corridor.add(x, y, 0), Blocks.AIR.getDefaultState());
				}
			}
		}
		
		//Re-add console
		if(console != null) {
			world.setBlockState(TardisHelper.TARDIS_POS, consoleState);
			world.getTileEntity(TardisHelper.TARDIS_POS).deserializeNBT(consoleData);
		}

		//Spawn Reclamation Unit
		if(!allInvs.isEmpty()) {
			world.setBlockState(TardisHelper.TARDIS_POS.south(2), TBlocks.reclamation_unit.getDefaultState());
			TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS.south(2));
			if(te instanceof ReclamationTile) {
				ReclamationTile unit = (ReclamationTile)te;
				for(ItemStack stack : allInvs) {
					unit.addItemStack(stack);
				}
			}
		}

		//Update lighting
		ChunkPos startPos = world.getChunk(TardisHelper.TARDIS_POS.subtract(temp.getSize())).getPos();
		ChunkPos endPos = world.getChunk(TardisHelper.TARDIS_POS.add(temp.getSize())).getPos();

		for(int x = startPos.x; x < endPos.x; ++x) {
			for(int z = startPos.z; z < endPos.z; ++z) {
				world.getChunkProvider().getLightManager().lightChunk(world.getChunk(x, z), true);
			}
		}

	}

	public static List<ConsoleRoom> getDefaults() {
		List<ConsoleRoom> list = Lists.newArrayList();
		for(ConsoleRoom room : REGISTRY.values()) {
			if(room.isUnlockedByDefault())
				list.add(room);
		}
		return list;
	}
}
