package net.tardis.mod.ars;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.CorridorKillTile;

public class ARSPiece implements IRegisterable<ARSPiece>{
	
	private ResourceLocation name;
	
	private ResourceLocation struct;
	private BlockPos offset;
	
	/**
	 * 
	 * @param loc - Registry name
	 * @param offset - The offset from the ARS block to spawn this
	 * @implSpec Use BlockPos difference between structure Block and the point where the corridor starts
	 * @implNote The Tape Measure item calculates the diff 1 block larger for the y and 1 block smaller for the z value. 
	 * Hence, subtract 1 to your y and add 1 to your z value to allow the structure to spawn successfully
	 */
	public ARSPiece(ResourceLocation loc, BlockPos offset) {
		this.struct = loc;
		this.offset = offset;
	}
	/**
	 * 
	 * @param loc - Registry name
	 * @param offset - The offset from the ARS block to spawn this 
	 * @implSpec Use BlockPos difference between structure Block and the point where the corridor starts
	 * @implNote The Tape Measure item calculates the diff 1 block larger for the y and 1 block smaller for the z value. 
	 * Hence, subtract 1 to your y and add 1 to your z value to allow the structure to spawn successfully
	 */
	public ARSPiece(String loc, BlockPos offset) {
		this(new ResourceLocation(Tardis.MODID, "tardis/structures/ars/" + loc), offset);
	}
	
	public boolean spawn(ServerWorld world, BlockPos pos, PlayerEntity player, Direction dir) {
		Template temp = world.getStructureTemplateManager().getTemplate(struct);
		
		//this.offset = new BlockPos(11, 3, 25);
		
		BlockPos offset = Helper.rotateBlockPos(this.offset, dir);
		
		if(temp != null) {
			
			BlockPos spawnStart = pos.subtract(offset);
			BlockPos endPos = spawnStart.add(Helper.rotateBlockPos(temp.getSize().north().west(), dir));
			for(BlockPos check : BlockPos.getAllInBoxMutable(spawnStart, endPos)) {
				if(!world.getBlockState(check).isAir(world, check)) {
					player.sendStatusMessage(Constants.Translations.CORRIDOR_BLOCKED, true);
					System.out.println("Structure tried to start spawning at: " + spawnStart + " and End Pos of: " + endPos + "With Offset of: " + offset + " from Start Pos");
					System.out.println("Conflict in structure creation detected at: " + check);
					return false;
				}
			}
			
			for(int x = -1; x <= 1; ++x) {
				for(int y = -1; y < 3; ++y) {
					if(dir == Direction.SOUTH || dir == Direction.NORTH)
						world.setBlockState(pos.add(x, y, 0), Blocks.AIR.getDefaultState());
					else world.setBlockState(pos.add(0, y, x), Blocks.AIR.getDefaultState());
				}
			}
			
			PlacementSettings set = new PlacementSettings().setIgnoreEntities(false).setRotation(Helper.getRotationFromDirection(dir));
			temp.addBlocksToWorld(world, spawnStart, set);
			for(BlockInfo info : temp.func_215381_a(spawnStart, set, TBlocks.corridor_kill)){
				TileEntity te = world.getTileEntity(info.pos);
				if(te instanceof CorridorKillTile) {
					CorridorKillTile tile = (CorridorKillTile)te;
					tile.setStart(spawnStart);
					tile.setEnd(endPos);
					tile.setRestorePos(pos, dir);
				}
			}
			return true;
			
		}
		else System.err.println("WARNING: Could not load structure " + this.struct);
		return false;
		
	}

	@Override
	public ARSPiece setRegistryName(ResourceLocation regName) {
		this.name = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.name;
	}
	
	public TranslationTextComponent getTranslation() {
		return new TranslationTextComponent("ars.piece." + this.name.getNamespace() + "." + this.name.getPath());
	}
}