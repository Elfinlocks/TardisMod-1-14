package net.tardis.mod.events;

import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.registries.Registry;

public class TardisRegistryEvent<T> extends Event{

	private Registry<T> registry;
	
	public TardisRegistryEvent(Registry<T> registry) {
		this.registry = registry;
	}
	
	public Registry<T> getRegistry(){
		return this.registry;
	}
}
