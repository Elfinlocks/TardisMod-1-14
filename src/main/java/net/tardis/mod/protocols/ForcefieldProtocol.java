package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.ForcefieldData;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ForcefieldProtocol extends Protocol {
	
	public static final TranslationTextComponent TRANS_ON = new TranslationTextComponent("protocol.tardis.forcefield_on");
	public static final TranslationTextComponent TRANS_OFF = new TranslationTextComponent("protocol.tardis.forcefield_off");
	
	@Override
	public void call(World world, PlayerEntity playerIn, ConsoleTile console) {
		if (!world.isRemote) {
		    console.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> {
			    shield.setActivated(!shield.getActivted());
			    Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FORCEFIELD, new ForcefieldData(shield.getActivted())), playerIn.dimension, playerIn.getPosition(), 64);
			    for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(console.getPos()).grow(16))) {
				    if (!shield.canBeUsed()) {
				    	player.sendStatusMessage(new TranslationTextComponent("status.tardis.forcefield.broken"), true);
				    }
				    else {
				    	player.sendStatusMessage(new TranslationTextComponent("status.tardis.forcefield." + shield.getActivted()), true);
				    }
			    }
		    });
		}
		else Tardis.proxy.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public String getDisplayName(ConsoleTile tile) {
		
		ObjectWrapper<TranslationTextComponent> wrapper = ObjectWrapper.create(TRANS_ON);
		
		System.out.println(tile);
		
		tile.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(sys -> {
			System.out.println("Has shield");
			if(sys.getActivted()) {
				System.out.println("Shield is active");
				wrapper.setValue(TRANS_OFF);
				System.out.println("Set trans off");
			}
		});
		
		return wrapper.getValue().getFormattedText();
	}

	@Override
	public String getSubmenu() {
		return "security";
	}

}
