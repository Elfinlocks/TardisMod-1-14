package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.tileentities.ConsoleTile;

public class ToggleAlarmProtocol extends Protocol {
    public static TranslationTextComponent TRANS_MON = new TranslationTextComponent("protocol.tardis.alarm");

    @Override
	public void call(World world, PlayerEntity player, ConsoleTile console) {
		if(!world.isRemote) 
			console.getInteriorManager().setAlarmOn(!console.getInteriorManager().isAlarmOn());
		else Tardis.proxy.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public String getDisplayName(ConsoleTile tile) {
		return TRANS_MON.getFormattedText();
	}

	@Override
	public String getSubmenu() {
		return "security";
	}

}
