package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.INeedItem;

public class FloorClutterBlock extends Block implements INeedItem {

    private BlockItem BLOCKITEM = new BlockItem(this, new Item.Properties().group(TItemGroups.FUTURE)) {
    };

    public static final VoxelShape SHAPE = Block.makeCuboidShape(0, 0, 0, 16, 0.1, 16);

    public FloorClutterBlock(Properties prop, SoundType sound, float hardness, float resistance) {

        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return false;
    }

    @Override
    public boolean causesSuffocation(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return false;
    }

    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public Item getItem() {
        return BLOCKITEM;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }
}
