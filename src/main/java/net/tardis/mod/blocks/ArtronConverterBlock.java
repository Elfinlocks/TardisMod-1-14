package net.tardis.mod.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.properties.Prop;

public class ArtronConverterBlock extends TileBlock {

	public static BooleanProperty EMIT = BooleanProperty.create("emit");
	
	public ArtronConverterBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
		this.setDefaultState(this.getDefaultState().with(EMIT, Boolean.TRUE));
	}
	
	@Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        if (!worldIn.isRemote) {
            boolean flag = state.get(EMIT);
            if (flag != worldIn.isBlockPowered(pos)) {
                if (flag) {
                    worldIn.getPendingBlockTicks().scheduleTick(pos, this, 4);
                } else {
                    worldIn.setBlockState(pos, state.cycle(EMIT), 2);
                }
            }

        }
    }
	
	@Override
    public void tick(BlockState state, World worldIn, BlockPos pos, Random random) {
        if (!worldIn.isRemote) {
            if (state.get(EMIT) && !worldIn.isBlockPowered(pos)) {
                worldIn.setBlockState(pos, state.cycle(EMIT), 2);
            }
        }
    }
	
	@Override
    public BlockState getStateForPlacement(BlockState state, Direction facing, BlockState state2, IWorld world, BlockPos pos1, BlockPos pos2, Hand hand) {
        return this.getDefaultState().with(EMIT, true);
    }
	
	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(EMIT);
	}

	@Override
	public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
			ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(Constants.Translations.TOOLTIP_REDSTONE_REQUIRED);
		tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, stack.getDisplayName());
			tooltip.add(new TranslationTextComponent("tooltip.artron_converter.purpose"));
			tooltip.add(new TranslationTextComponent("tooltip.artron_converter.use"));
		}
		
	}
	
	
}
