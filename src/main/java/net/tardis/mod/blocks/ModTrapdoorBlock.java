package net.tardis.mod.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.TrapDoorBlock;
import net.minecraft.util.BlockRenderLayer;
import net.tardis.mod.ars.IARS;

public class ModTrapdoorBlock extends TrapDoorBlock implements IARS {

    public ModTrapdoorBlock(Properties prop, SoundType sound, float hardness, float resistance) {
        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }
    
    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }


}
