package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.INeedItem;

public class CubeBlock extends Block implements INeedItem {

    private BlockItem BLOCKITEM = new BlockItem(this, new Item.Properties().group(TItemGroups.FUTURE));

    public CubeBlock(Properties prop, SoundType sound, float hardness, float resistance) {

        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }
    
    public CubeBlock(Properties prop) {
    	super(prop);
    }

    @Override
    public Item getItem() {
        return BLOCKITEM;
    }
}
