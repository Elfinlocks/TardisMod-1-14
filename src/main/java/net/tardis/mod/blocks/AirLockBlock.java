package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.sounds.TSounds;

public class AirLockBlock extends Block implements INeedItem{

	public static BooleanProperty TOP = BooleanProperty.create("top");
	public static BooleanProperty OPEN = BooleanProperty.create("open");
	
	public static VoxelShape TOP_SHAPE = Block.makeCuboidShape(0, 0, 0, 16, 1, 16);
	public static VoxelShape BOTTOM_SHAPE = Block.makeCuboidShape(0, 15, 0, 16, 16, 16);
	
	public BlockItem item = new AirLockBlockItem(this, Prop.Items.SIXTY_FOUR.get().group(TItemGroups.FUTURE));
	
	public AirLockBlock(Properties properties) {
		super(properties);
		this.setDefaultState(this.getDefaultState().with(OPEN, false).with(TOP, false));
	}

	@Override
	public boolean isSolid(BlockState state) {
		return !state.get(OPEN);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(TOP, OPEN);
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		if(!state.get(OPEN))
			return VoxelShapes.fullCube();
		
		if(state.get(TOP))
			return BOTTOM_SHAPE;
		
		return TOP_SHAPE;
	}

	@Override
	public boolean causesSuffocation(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return !state.get(OPEN);
	}

	@Override
	public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
		super.onReplaced(state, worldIn, pos, newState, isMoving);
		
		if(state.getBlock() == newState.getBlock())
			return;
		
		BlockPos other = null;
		if(state.get(TOP))
			other = pos.down();
		else other = pos.up();
		
		worldIn.setBlockState(other, Blocks.AIR.getDefaultState());
		
	}

	@Override
	public Item getItem() {
		return item;
	}
	
	public static class AirLockBlockItem extends BlockItem{

		public AirLockBlockItem(Block blockIn, Properties builder) {
			super(blockIn, builder);
		}

		@Override
		public ActionResultType tryPlace(BlockItemUseContext context) {
			return context.getWorld().getBlockState(context.getPos().up()).isReplaceable(context) ? super.tryPlace(context) : ActionResultType.FAIL;
		}

		@Override
		protected boolean onBlockPlaced(BlockPos pos, World worldIn, PlayerEntity player, ItemStack stack, BlockState state) {
			worldIn.setBlockState(pos.up(), state.with(TOP, true));
			return super.onBlockPlaced(pos, worldIn, player, stack, state);
		}
		
	}

	@Override
	public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
		boolean open = state.get(OPEN);
		if(worldIn.isBlockPowered(pos) && !open)
			this.setDoorState(worldIn, pos, true);
		else if(!worldIn.isBlockPowered(pos) && open)
			this.setDoorState(worldIn, pos, false);
		super.neighborChanged(state, worldIn, pos, blockIn, fromPos, isMoving);
	}
	
	public void setDoorState(World world, BlockPos pos, boolean open) {
		BlockState state = world.getBlockState(pos);
		
		world.setBlockState(pos, state.with(OPEN, open));
		if(state.get(TOP))
			world.setBlockState(pos.down(), world.getBlockState(pos.down()).with(OPEN, open));
		else world.setBlockState(pos.up(), world.getBlockState(pos.up()).with(OPEN, open));
	}

}
