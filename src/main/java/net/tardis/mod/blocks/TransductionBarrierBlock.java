package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;

public class TransductionBarrierBlock extends TileBlock {

	public TransductionBarrierBlock(Properties prop) {
		super(prop);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn,
			BlockRayTraceResult hit) {
		ItemStack sonic = new ItemStack(TItems.SONIC);
		if (player.getHeldItem(handIn).isItemEqual(sonic)) {
			return true;
		}
		else {
			PlayerHelper.sendMessageToPlayer(player, Constants.Translations.REQUIRES_SONIC, true);
			return true;
		}
	}

}
