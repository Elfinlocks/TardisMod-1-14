package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.blocks.multiblock.MultiblockPatterns;
import net.tardis.mod.blocks.multiblock.MultiblockPatterns.MultiblockPattern;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.items.MultiblockBlockItem;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.properties.Prop;


public class WaypointBankBlock extends MultiblockBlock implements INeedItem, IWaterLoggable{
	public final WaypointBankItem item = new WaypointBankItem(this, MultiblockPatterns.COMPUTER, Prop.Items.SIXTY_FOUR.get());
	
	public WaypointBankBlock(Properties prop) {
		super(prop);
		this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false));
	}
	
	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		IFluidState fluid = context.getWorld().getFluidState(context.getPos());
		return super.getStateForPlacement(context)
				.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer()
						.getHorizontalFacing().getOpposite())
				.with(BlockStateProperties.WATERLOGGED, fluid.getFluidState().isTagged(FluidTags.WATER));
	}

	@Override
	public Item getItem() {
		return item;
	}
	
	public static class WaypointBankItem extends MultiblockBlockItem{

		public WaypointBankItem(Block blockIn, MultiblockPattern pattern, Properties builder) {
			super(blockIn, pattern, builder);
		}

		@Override
		public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
			super.addInformation(stack, worldIn, tooltip, flagIn);
			tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
			if (Screen.hasShiftDown()) {
				tooltip.clear();
				tooltip.add(0, stack.getDisplayName());
				tooltip.add(new TranslationTextComponent("tooltip.waypoint_bank.desc"));
			}
		}
		
		
		
	}

}
