package net.tardis.mod.blocks;

import java.util.Random;
import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.sounds.TSounds;

public class SteamGrateBlock extends Block {
	
	public static final VoxelShape SHAPE = createShape();

	public SteamGrateBlock(Properties properties) {
		super(properties);
	}

	@Override
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
		super.animateTick(stateIn, worldIn, pos, rand);
		if(worldIn.getGameTime() % 300 == 0) {
			for(int i = 0; i < 300; ++i) {
				double horSpeed = (i / 300.0) * 0.2;
				worldIn.addParticle(ParticleTypes.CLOUD, pos.getX() + 0.5, pos.getY() + 0.1 + (i / 300.0) * 2, pos.getZ() + 0.5, ((rand.nextDouble() * horSpeed) - (horSpeed) / 2.0), 0.05, ((rand.nextDouble() * horSpeed) - (horSpeed) / 2.0));
			}
			this.playSoundOnClient(worldIn, pos);
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	public void playSoundOnClient(World world, BlockPos pos) {
		world.playSound(Minecraft.getInstance().player, pos, TSounds.STEAM_HISS, SoundCategory.BLOCKS, 1.0F, 1.0F);
	}

	@Override
	public BlockRenderLayer getRenderLayer() {
		return BlockRenderLayer.CUTOUT_MIPPED;
	}

	public static VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(10.65, 0, 4, 12.65, 1, 12),
				Block.makeCuboidShape(0, 0, 3, 16, 1, 4),
				Block.makeCuboidShape(0, 0, 12, 16, 1, 13),
				Block.makeCuboidShape(0, 0, 4, 2, 1, 12),
				Block.makeCuboidShape(14, 0, 4, 16, 1, 12),
				Block.makeCuboidShape(3.4, 0, 4, 5.4, 1, 12),
				Block.makeCuboidShape(6.9, 0, 4, 8.9, 1, 12)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return SHAPE;
	}

}
