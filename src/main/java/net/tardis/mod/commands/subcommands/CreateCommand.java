package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class CreateCommand extends TCommand {
    private static final CreateCommand CMD = new CreateCommand();

    public CreateCommand() {
        super(PermissionEnum.CREATE);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("create").requires(context -> context.hasPermissionLevel(2))
                .executes(context -> createTardis(context, context.getSource().asPlayer()))
        		    .then(Commands.argument("player", EntityArgument.player())
                        .executes(context -> createTardis(context, EntityArgument.getPlayer(context, "player"))));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return Command.SINGLE_SUCCESS;
    }
    
    private static int createTardis(CommandContext<CommandSource> context, ServerPlayerEntity player)  throws CommandSyntaxException {
        final CommandSource source = context.getSource();
        if(TardisHelper.hasTARDIS(source.getServer(), player.getUniqueID())) {
            source.sendErrorMessage(Constants.Translations.EXISTING_TARDIS);
            return Command.SINGLE_SUCCESS;
        }
        else {
            BlockPos pos = player.getPosition().up().offset(player.getHorizontalFacing(), 2);
            player.world.setBlockState(pos, TBlocks.broken_exterior.getDefaultState());
            TileEntity te = player.world.getTileEntity(pos);
            if (te instanceof BrokenExteriorTile) {
                BrokenExteriorTile ext = (BrokenExteriorTile)te;
                ext.getBrokenType().setup(source.getWorld(), player, ext);
                ext.tameTardis(player);
            }
            return Command.SINGLE_SUCCESS;
        }
    }
}
