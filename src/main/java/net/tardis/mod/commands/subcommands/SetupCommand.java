package net.tardis.mod.commands.subcommands;

import java.util.Collection;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class SetupCommand extends TCommand{

	public static final SetupCommand CMD = new SetupCommand(PermissionEnum.CREATE);
	
	public SetupCommand(PermissionEnum permission) {
		super(permission);
	}
	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
		return Commands.literal("setup").requires(context -> context.hasPermissionLevel(2))
		        .executes(context -> { return setUpTardis(context, context.getSource().asPlayer());})
		            .then(Commands.argument("players", EntityArgument.players())
		                .executes(context -> {return setUpTardis(context, EntityArgument.getPlayers(context, "players"));}));
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	private static int setUpTardis(CommandContext<CommandSource> context, ServerPlayerEntity ent){
        CommandSource source = context.getSource();
        ServerWorld world = source.getServer().getWorld(TardisHelper.getTardisByUUID(ent.getUniqueID()));
        world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            PanelInventory comp = cap.getEngineInventoryForSide(Direction.NORTH);
            PanelInventory artron = cap.getEngineInventoryForSide(Direction.WEST);
            
            //Add capacitors
            for(int i = 0; i < artron.getSizeInventory(); ++i) {
                artron.setInventorySlotContents(i, new ItemStack(TItems.ARTRON_CAPACITOR_HIGH));
            }
            
            TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
                
                int index = 0;
                for(Subsystem s : tile.getSubSystems()) {
                    
                    if(index >= comp.getSizeInventory())
                        break;
                    
                    comp.setInventorySlotContents(index, new ItemStack(s.getItemKey()));
                    ++index;
                }
                
                tile.updateArtronValues();
                tile.setArtron(1000000F);
            });
            source.sendFeedback(new TranslationTextComponent("command.tardis.setup.success", ent.getDisplayName()), true);
        });
        ent.addItemStackToInventory(new ItemStack(TItems.KEY));
        return Command.SINGLE_SUCCESS;
    }
	
	private static int setUpTardis(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets) throws CommandSyntaxException {
	    targets.forEach(ent -> {
            setUpTardis(context, ent);
	    });
	    return Command.SINGLE_SUCCESS;
	}

}
