package net.tardis.mod.commands.subcommands;

import java.util.Collection;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.helper.TardisHelper;

/**
 * Created by 50ap5ud5
 * on 25 Oct 2020 @ 12:55:51 pm
 * <br> Debug command to clear a console room contents of blocks. Useful for builders if they want an easy way to build a new room
 */
public class ClearConsoleRoomCommand extends TCommand{
    
    public ClearConsoleRoomCommand(PermissionEnum permission) {
        super(permission);
    }
    
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("clear_interior")
                .requires(context -> context.hasPermissionLevel(4))
                .executes(context-> clearTardisIntForSelf(context, context.getSource().asPlayer()))
                    .then(Commands.argument("players", EntityArgument.players())
                        .executes(context -> clearTardisInts(context, EntityArgument.getPlayers(context, "players"))));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return Command.SINGLE_SUCCESS;
    }
    
    private static int clearTardisInt(CommandContext<CommandSource> context, ServerPlayerEntity player) {
        final CommandSource source = context.getSource();
        ServerWorld world = source.getServer().getWorld(TardisHelper.getTardisByUUID(player.getUniqueID()));
        TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).ifPresent(tile -> {
            ConsoleRoom room = ConsoleRoom.DEBUG_CLEAN;
            if (room != null) {
                tile.setConsoleRoom(room);
                room.spawnConsoleRoom(world, false);
            }
        });
        source.sendFeedback(new TranslationTextComponent("message.tardis.clear_interior.success", player.getDisplayName()), true);
        return Command.SINGLE_SUCCESS;
    }
    
    private static int clearTardisIntForSelf(CommandContext<CommandSource> context, ServerPlayerEntity player) throws CommandSyntaxException{
        return clearTardisInt(context, player);
    }
    
    private static int clearTardisInts(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> players) throws CommandSyntaxException{
        players.forEach(player -> {
            clearTardisInt(context, player);
        });
        return Command.SINGLE_SUCCESS;
    }

}
