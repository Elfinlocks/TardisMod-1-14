package net.tardis.mod.commands.subcommands;

import java.util.Collection;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.helper.TardisHelper;

public class RefuelCommand extends TCommand {

    public RefuelCommand() {
        super(PermissionEnum.REFUEL);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("refuel")
                .requires(context -> context.hasPermissionLevel(2))
                .then(Commands.argument("amount", IntegerArgumentType.integer())
                    .executes(context -> {
                       return refuelTardisForSelf(context, context.getSource().asPlayer());
                }).then(Commands.argument("targets", EntityArgument.players()) 
                        .executes(context -> {
                         return refuelTardis(context, EntityArgument.getPlayers(context, "targets"));   
                        })));

    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return Command.SINGLE_SUCCESS;
    }
    
    private static int refuelTardis(CommandContext<CommandSource> context, ServerPlayerEntity player){
        CommandSource source = context.getSource();
        Integer amount = context.getArgument("amount", Integer.class);
        String displayName = player.getDisplayName().getString();
        TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).ifPresent(console -> {
            console.setArtron(console.getArtron() + amount.floatValue());
            source.sendFeedback(new TranslationTextComponent("message.tardis.refuel_command", displayName, console.getArtron()), true);
        });
        return Command.SINGLE_SUCCESS;
    }
    
    private static int refuelTardisForSelf(CommandContext<CommandSource> context, ServerPlayerEntity player) throws CommandSyntaxException {
        return refuelTardis(context, player);
    }
    
    private static int refuelTardis(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets) throws CommandSyntaxException {
        targets.forEach(player -> {
            refuelTardis(context, player);
        });
        return Command.SINGLE_SUCCESS;
    }
}
