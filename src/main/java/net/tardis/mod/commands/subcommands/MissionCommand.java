package net.tardis.mod.commands.subcommands;

import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.registries.TardisForgeRegistries;

public class MissionCommand extends TCommand{

	public static final MissionCommand CMD = new MissionCommand(PermissionEnum.UNLOCK);
	
	public MissionCommand(PermissionEnum permission) {
		super(permission);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		try {
			ServerPlayerEntity player = context.getSource().asPlayer();
			
			//Stop if the command executor has no TARDIS
			if(!TardisHelper.hasTARDIS(context.getSource().getServer(), player.getUniqueID())) {
				//context.getSource().sendErrorMessage(TCommand.NO_TARDIS_ERROR);
				return 0;
			}
			
			TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).ifPresent(tardis -> {
				MiniMissionType mission = TardisForgeRegistries.MISSIONS.getValue(new ResourceLocation(context.getArgument("mission", String.class)));
				//mission.spawnMissionStage(world, pos)
			});
			
		}
		catch(CommandSyntaxException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
		return Commands.literal("mission")
				.then(Commands.literal("start")
				.then(Commands.argument("mission", StringArgumentType.greedyString()).suggests((context, builder) -> {
					return ISuggestionProvider.suggest(convert(TardisForgeRegistries.MISSIONS.getKeys()), builder);
				}).executes(CMD)));
	}
	
	public static List<String> convert(Iterable<ResourceLocation> locations){
		List<String> newList = Lists.newArrayList();
		for(ResourceLocation list : locations) {
			newList.add(list.toString());
		}
		return newList;
	}
	
}