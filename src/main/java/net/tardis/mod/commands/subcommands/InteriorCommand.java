package net.tardis.mod.commands.subcommands;

import java.util.Collection;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TeleportUtil;

public class InteriorCommand extends TCommand {

    public InteriorCommand() {
        super(PermissionEnum.INTERIOR);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("interior")
                .requires(context -> context.hasPermissionLevel(2))
                   .executes(context -> {
                        return teleportToInterior(context, context.getSource().asPlayer(), context.getSource().asPlayer());
                        })
                    .then(Commands.argument("players", EntityArgument.players())
                    .then(Commands.argument("target", EntityArgument.player())
                        .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                    .executes(context -> {return teleportToInterior(context, EntityArgument.getPlayers(context, "players"), EntityArgument.getPlayer(context, "target"));})));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        return Command.SINGLE_SUCCESS;
    }
    
    private static int teleportEntityToInterior(CommandContext<CommandSource> context, Entity entityToTeleport, ServerPlayerEntity target){
        final CommandSource source = context.getSource();
        final DimensionType consoleDimension = TardisHelper.getTardisByUUID(target.getUniqueID());
        TeleportUtil.teleportEntity(entityToTeleport, consoleDimension, TardisHelper.TARDIS_POS.getX() + 2, TardisHelper.TARDIS_POS.getY(), TardisHelper.TARDIS_POS.getZ());
        TardisHelper.getConsole(source.getServer(), consoleDimension).ifPresent(tile -> {
            tile.getDoor().ifPresent(door -> door.setOpenState(EnumDoorState.CLOSED));
        });
        source.sendFeedback(new TranslationTextComponent("message.tardis.interior_command", entityToTeleport.getDisplayName(), target.getDisplayName()), true);
        return Command.SINGLE_SUCCESS;
    }
    
    private static int teleportToInterior(CommandContext<CommandSource> context, ServerPlayerEntity playerToTeleport, ServerPlayerEntity target) throws CommandSyntaxException {
        return teleportEntityToInterior(context, playerToTeleport, target);
    }
    
    
    private static int teleportToInterior(CommandContext<CommandSource> context, Collection<? extends Entity> entitiesToTeleport, ServerPlayerEntity target) throws CommandSyntaxException {
        entitiesToTeleport.forEach(entity -> {
            teleportEntityToInterior(context, entity, target);
        });
        return Command.SINGLE_SUCCESS;
    }

}
