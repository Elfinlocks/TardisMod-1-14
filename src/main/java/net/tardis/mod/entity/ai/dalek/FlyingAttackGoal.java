package net.tardis.mod.entity.ai.dalek;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.DalekEntity;

/**
 * Created by Swirtzly
 * on 19/10/2019 @ 11:35
 */
public class FlyingAttackGoal extends Goal {

    private float heightOffset = 0.5F;
    private int heightOffsetUpdateTime;
    private CreatureEntity livingEntity;

    public FlyingAttackGoal(CreatureEntity livingEntity) {
        this.livingEntity = livingEntity;
    }

    @Override
    public boolean shouldExecute() {
        return livingEntity.getAttackTarget() != null && shouldFly(livingEntity);
    }

    private boolean shouldFly(CreatureEntity livingEntity) {
        if (livingEntity instanceof DalekEntity) {
            DalekEntity dalek = (DalekEntity) livingEntity;
            return dalek.getDalekType().canFly();
        }
        return true;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return shouldExecute();
    }

    @Override
    public void tick() {
        super.tick();
        Vec3d vec3d = livingEntity.getMotion();
        LivingEntity victim = livingEntity.getAttackTarget();
        boolean victimFlag = victim != null && victim.posY + (double) victim.getEyeHeight() > livingEntity.posY + (double) livingEntity.getEyeHeight() + (double) this.heightOffset && livingEntity.canAttack(victim) || livingEntity.isInWater();
        if (victimFlag) {

            --this.heightOffsetUpdateTime;
            if (this.heightOffsetUpdateTime <= 0) {
                this.heightOffsetUpdateTime = 100;
                this.heightOffset = 0.5F + (float) livingEntity.world.rand.nextGaussian() * 3.0F;
            }

            livingEntity.setMotion(livingEntity.getMotion().add(0.0D, ((double) 0.3F - vec3d.y) * (double) 0.3F, 0.0D));
            livingEntity.isAirBorne = true;

            if (livingEntity.getEntitySenses().canSee(victim) && livingEntity.getDistance(victim) < 32 && livingEntity.getDistance(victim) > 5) {
                livingEntity.getMoveHelper().setMoveTo(victim.posX, victim.posY, victim.posZ, 1.0D);
            }
        } else {
            heightOffset = 0.5F;
        }
    }

}
