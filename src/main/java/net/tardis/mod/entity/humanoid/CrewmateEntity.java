package net.tardis.mod.entity.humanoid;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.humanoids.DoCrewWorkGoal;
import net.tardis.mod.entity.ai.humanoids.SitInChairGoal;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.missions.misc.Dialog;

public class CrewmateEntity extends AbstractHumanoidEntity{

	public static final ResourceLocation TEXTURE = new ResourceLocation("textures/entity/steve.png");
	
	public CrewmateEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public CrewmateEntity(World worldIn) {
		super(TEntities.CREWMATE, worldIn);
	}

	@Override
	public Dialog getCurrentDialog() {
		return null;
	}

	@Override
	public ResourceLocation getSkin() {
		return TEXTURE;
	}

	@Override
	protected boolean processInteract(PlayerEntity player, Hand hand) {
		return super.processInteract(player, hand);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		//Tasks
		this.goalSelector.addGoal(3, new SitInChairGoal(this, 0.2334, 16));
		this.goalSelector.addGoal(2, new DoCrewWorkGoal(this, 0.2334, 16));
	}

	@Override
	public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag) {
		this.setupDefaultEquipment();
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}
	
	public void setupDefaultEquipment() {
		if(!world.isRemote()) {
			this.setItemStackToSlot(EquipmentSlotType.CHEST, new ItemStack(TItems.SPACE_CHEST));
			this.setItemStackToSlot(EquipmentSlotType.LEGS, new ItemStack(TItems.SPACE_LEGS));
			this.setItemStackToSlot(EquipmentSlotType.FEET, new ItemStack(TItems.SPACE_BOOTS_GRAV));
		}
	}
}
