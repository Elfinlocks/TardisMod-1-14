package net.tardis.mod.entity.humanoid;

import java.util.UUID;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.HelpFlyTARDISGoal;
import net.tardis.mod.entity.ai.humanoids.CompanionFollowGoal;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;

public class CompanionEntity extends AbstractHumanoidEntity{

	private boolean isStaying = false;
	private UUID owner = null;
	private PlayerEntity ownerEntity;
	
	public CompanionEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public CompanionEntity(World worldIn) {
		this(TEntities.COMPANION, worldIn);
	}

	@Override
	public Dialog getCurrentDialog() {
		
		Dialog root = new Dialog("What's up?");
		
		//Follow type
		DialogOption followType = new DialogOption(null, this.isStaying ? "I need you to wait here" : "Follow me");
		followType.setOptionAction((companion, player) -> {
			CompanionEntity comp = (CompanionEntity)companion;
			comp.setSitting(!comp.isStaying);
		});
		DialogOption learnConsole = new DialogOption(null, "I want you to watch me, learn how to fly the TARDIS");
		
		root.addDialogOption(learnConsole);
		root.addDialogOption(followType);
		
		return root;
	}

	@Override
	public ResourceLocation getSkin() {
		return new ResourceLocation("textures/entity/steve.png");
	}

	@Override
	protected boolean processInteract(PlayerEntity player, Hand hand) {
		
		return super.processInteract(player, hand);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		this.goalSelector.addGoal(3, new CompanionFollowGoal(this, 0.2334));
		this.goalSelector.addGoal(2, new HelpFlyTARDISGoal(this, 0.24, 16));
	}

	public UUID getOwner() {
		return this.owner;
	}
	
	public PlayerEntity getOwnerEntity() {
		
		if(this.owner == null)
			return null;
		
		if(this.ownerEntity != null && this.ownerEntity.isAlive())
			return ownerEntity;
		
		if(!world.isRemote) {
			Entity ent = ((ServerWorld)world).getEntityByUuid(owner);
			if(ent instanceof PlayerEntity)
				return this.ownerEntity = (PlayerEntity)ent;
		}
		return null;
	}
	
	public boolean isSitting() {
		return this.isStaying;
	}
	
	public void setSitting(boolean sit) {
		this.isStaying = sit;
	}
}
