package net.tardis.mod.entity.projectiles;

import net.minecraft.block.BlockState;
import net.minecraft.block.TNTBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.dalek.types.DalekType.IDalekWeapon;

/**
 * Created by Swirtzly
 * on 06/10/2019 @ 12:17
 */
public class LaserEntity extends ThrowableEntity {

    public float damage = 10F, scale = 0.5F;
    public Vec3d color = new Vec3d(0, 1, 1);
    private DamageSource source = DamageSource.GENERIC;
    IDalekWeapon weapon;

    public LaserEntity(EntityType<? extends ThrowableEntity> type, World worldIn) {
        super(type, worldIn);
    }
    
    public LaserEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn) {
        super(type,x,y,z, worldIn);
    }

    
    public LaserEntity(EntityType<? extends ThrowableEntity> type, LivingEntity livingEntityIn, World worldIn) {
        super(type, livingEntityIn, worldIn);
    }
    
    public LaserEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn ,LivingEntity livingEntityIn, float damage, DamageSource source, Vec3d color) {
        super(type,x, y, z,worldIn);
    	this.damage = damage;
		this.color = color;
		this.source = source;
    }


    public LaserEntity(World world) {
        this(TEntities.LASER, world);
    }
    

	public void setWeaponType(IDalekWeapon weap) {
    	this.weapon = weap;
    }
	
	@Override
	public void tick() {
		double speed = new Vec3d(posX, posY, posZ).distanceTo(new Vec3d(prevPosX, prevPosY, prevPosZ));
		if (!this.world.isRemote && (ticksExisted > 30 * 20 || speed < 0.01)) {
			this.remove(); //This removes the laser if it goes too far away
		}
		
		if(this.isAlive()) {
			super.tick();
		}
	}

    @Override
    protected void onImpact(RayTraceResult result) {
        if (result == null || !isAlive())
            return;

        //Entity Hit
        if (result.getType() == RayTraceResult.Type.ENTITY) {
            EntityRayTraceResult entityHitResult = ((EntityRayTraceResult) result);
            if (entityHitResult.getEntity() == this.getThrower() || entityHitResult == null) return;
            Entity hitEntity = entityHitResult.getEntity();
            hitEntity.attackEntityFrom(source, damage);
        }
            //Block Hit
        else if (result.getType() == RayTraceResult.Type.BLOCK) {
            BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
            BlockState block = world.getBlockState(blockResult.getPos());
            if (block.getBlock() instanceof TNTBlock) {
                BlockPos pos = blockResult.getPos();
                world.removeBlock(pos, true);
                TNTEntity tntEntity = new TNTEntity(world, (double) ((float) pos.getX() + 0.5F), (double) pos.getY(), (double) ((float) pos.getZ() + 0.5F), getThrower());
                tntEntity.setFuse(0);
                world.addEntity(tntEntity);
                world.playSound(null, tntEntity.posX, tntEntity.posY, tntEntity.posZ, SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
                if (world.isRemote()) {
                	world.addParticle(ParticleTypes.SMOKE, true, this.posX, this.posY, this.posZ, 0, 0, 0);
                }
            }
        }
        
        if(this.weapon != null)
        	this.weapon.onHit(this.getThrower() instanceof DalekEntity ? (DalekEntity)this.getThrower() : null, world, result);
        
        if (!this.world.isRemote) {
            this.remove();
        }
    }

    public void setColor(Vec3d color) {
        this.color = color;
    }

    public Vec3d getColor() {
        return color;
    }

    public void setSource(DamageSource source) {
        this.source = source;
    }

    public DamageSource getSource() {
        return source;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    @Override
    protected void registerData() {

    }

    @Override
    public void readAdditional(CompoundNBT compoundNBT) {
        super.readAdditional(compoundNBT);
        this.damage = compoundNBT.getFloat("damage");
        this.color = new Vec3d(compoundNBT.getDouble("red"), compoundNBT.getDouble("green"), compoundNBT.getDouble("blue"));
        this.scale = compoundNBT.getFloat("scale");
    }

    @Override
    public void writeAdditional(CompoundNBT compoundNBT) {
        super.writeAdditional(compoundNBT);
        compoundNBT.putFloat("damage", damage);
        compoundNBT.putDouble("red", color.x);
        compoundNBT.putDouble("green", color.y);
        compoundNBT.putDouble("blue", color.z);
        compoundNBT.putFloat("scale", scale);

    }

    @Override
    public boolean isInWater() {
        return false;
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
    
    @Override
    protected float getGravityVelocity() {
        return 0.00001F;
    }
}
