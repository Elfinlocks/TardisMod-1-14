package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

/**
 *
 * A class for places the TARDIS should show up, but only for display purposes, think 3D particle
 * 
 * @author Spectre0987 8-12-2020
 *
 *
 */
public class TardisDisplayEntity extends Entity implements IEntityAdditionalSpawnData{

	
	private ExteriorTile tile;
	private int ticksUntilDeath = 100;
	private float friction = 0.97F;
	
	public TardisDisplayEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		this.setInvulnerable(true);
		this.noClip = true;
	}
	
	public TardisDisplayEntity(World worldIn) {
		this(TEntities.DISPLAY_TARDIS, worldIn);
	}
	
	public TardisDisplayEntity setTile(ExteriorTile tile) {
		this.tile = tile;
		return this;
	}
	
	public void setTicksUntilDeath(int death) {
		this.ticksUntilDeath = death;
	}
	
	public int getTicksUntilDeath() {
		return this.ticksUntilDeath;
	}
	
	public void setFriction(float friction) {
		this.friction = friction;
	}

	@Override
	protected void registerData() {}

	@Override
	protected void readAdditional(CompoundNBT compound) {}

	@Override
	protected void writeAdditional(CompoundNBT compound) {}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void writeSpawnData(PacketBuffer buffer) {
		buffer.writeResourceLocation(this.getTile().getType().getRegistryName());
	}

	@Override
	public void readSpawnData(PacketBuffer buffer) {
		TileEntityType<?> type = ForgeRegistries.TILE_ENTITIES.getValue(buffer.readResourceLocation());
		if(type != null) {
			this.tile = (ExteriorTile)type.create();
			this.tile.setWorld(world);
		}
	}

	@Override
	public void tick() {
		super.tick();
		this.noClip = true;
		
		this.move(MoverType.SELF, this.getMotion());
		this.setMotion(this.getMotion().scale(this.friction));
		
		if(!world.isRemote) {
			--this.ticksUntilDeath;
			if(this.ticksUntilDeath <= 0)
				this.remove();
		}
	}

	public ExteriorTile getTile() {
		if(this.tile == null) {
			this.tile = TTiles.EXTERIOR_STEAMPUNK.create();
			tile.setWorld(world);
		}
		return this.tile;
	}

}
