package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class ZControl extends AxisControl{
	
	public ZControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity, Axis.Z);
	}

	@Override
	public EntitySize getSize() {
		if (this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		if(getConsole() instanceof CoralConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);

		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-6 / 16.0, 10 / 16.0, 6 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.4059090875328474, 0.625, 0.3479865154450874);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.22546609302967927, 0.5, -0.7);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.7824804304370165, 0.5625, 0.5757301742949457);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.0842288148454582, 0.4375, 0.14702252220259904);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.4806918248077612, 0.71875, 0.4727898888202058);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.74, 0.4, 0.2924539413501982);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.8132539480909782, 0.46875, 0.737394527955187);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.759197527988781, 0.4375, -0.5713981345208767);
		
		
		return new Vec3d(-8.85 / 16.0, 9.85 / 16.0, -3.35 / 16.0);
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
