package net.tardis.mod.sounds;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;
import net.tardis.mod.Tardis;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
@ObjectHolder(Tardis.MODID)
public class TSounds {

    public static final SoundEvent BESSIE_HORN = null;
    /**
     * SOUND REGISTRY
     * This class is responsible for registering all sounds within the mod
     * Each sound requires a field below with a matching name in the registry event
     * ~ Swirtzly
     */

    public static final SoundEvent TARDIS_LAND = null;
    public static final SoundEvent TARDIS_TAKEOFF = null;
    public static final SoundEvent TARDIS_FLY_LOOP = null;
    public static final SoundEvent TAKEOFF_TV = null;
    public static final SoundEvent LAND_TV = null;
    public static final SoundEvent FLY_LOOP_TV = null;
    public static final SoundEvent MASTER_TAKEOFF = null;
    public static final SoundEvent MASTER_LAND = null;
    
    public static final SoundEvent ROTOR_START = null;
    public static final SoundEvent ROTOR_TICK = null;
    public static final SoundEvent ROTOR_END = null;
    
    public static final SoundEvent DOOR_OPEN = null;
    public static final SoundEvent DOOR_CLOSE = null;
    public static final SoundEvent DOOR_LOCK = null;
    public static final SoundEvent DOOR_UNLOCK = null;
    public static final SoundEvent DOOR_KNOCK = null;
    public static final SoundEvent CAR_LOCK = null;
    public static final SoundEvent WOOD_DOOR_OPEN = null;
    public static final SoundEvent WOOD_DOOR_CLOSE = null;

    public static final SoundEvent HANDBRAKE_ENGAGE = null;
    public static final SoundEvent HANDBRAKE_RELEASE = null;
    public static final SoundEvent THROTTLE = null;
    public static final SoundEvent RANDOMISER = null;
    public static final SoundEvent GENERIC_ONE = null;
    public static final SoundEvent GENERIC_TWO = null;
    public static final SoundEvent GENERIC_THREE = null;
    public static final SoundEvent DIMENSION = null;
    public static final SoundEvent DIRECTION = null;
    public static final SoundEvent LANDING_TYPE_UP = null;
    public static final SoundEvent LANDING_TYPE_DOWN = null;
    public static final SoundEvent CANT_START = null;
    public static final SoundEvent REFUEL_START = null;
    public static final SoundEvent REFUEL_STOP = null;
    public static final SoundEvent STABILIZER_ON = null;
    public static final SoundEvent STABILIZER_OFF = null;
    public static final SoundEvent TELEPATHIC_CIRCUIT = null;
    
    public static final SoundEvent SINGLE_CLOISTER = null;
    public static final SoundEvent ALARM_LOW = null;
    public static final SoundEvent SONIC_GENERIC = null;
    public static final SoundEvent VM_TELEPORT = null;
    public static final SoundEvent VM_BUTTON = null;
    public static final SoundEvent VM_TELEPORT_DEST = null;
    public static final SoundEvent WATCH_MALFUNCTION = null;
    public static final SoundEvent WATCH_TICK = null;
    public static final SoundEvent REMOTE_ACCEPT = null;
    
    public static final SoundEvent PAPER_DROP = null;

    public static final SoundEvent DALEK_EXTERMINATE = null;
    public static final SoundEvent DALEK_FIRE = null;
    public static final SoundEvent DALEK_HOVER = null;
    public static final SoundEvent DALEK_DEATH = null;
    public static final SoundEvent DALEK_MOVES = null;
    public static final SoundEvent DALEK_SW_AIM = null;
    public static final SoundEvent DALEK_SW_FIRE = null;
    public static final SoundEvent DALEK_SW_HIT_EXPLODE = null;

    public static final SoundEvent TARDIS_HUM_63 = null;
    public static final SoundEvent TARDIS_HUM_70 = null;
    public static final SoundEvent TARDIS_HUM_80 = null;
    public static final SoundEvent TARDIS_HUM_COPPER = null;
    public static final SoundEvent TARDIS_HUM_CORAL = null;
    public static final SoundEvent TARDIS_HUM_TOYOTA = null;
    
    public static final SoundEvent AMBIENT_CREAKS = null;
    public static final SoundEvent SONIC_TUNING = null;
    public static final SoundEvent REACHED_DESTINATION = null;
    public static final SoundEvent SONIC_FAIL = null;
    public static final SoundEvent SONIC_BROKEN = null;
    public static final SoundEvent BESSIE_DRIVE = null;
    public static final SoundEvent SHIELD_HUM = null;
    public static final SoundEvent STEAM_HISS = null;


    public static final SoundEvent COMMUNICATOR_BEEP = null;
    public static final SoundEvent COMMUNICATOR_RING = null;
    public static final SoundEvent COMMUNICATOR_STEAM = null;
    public static final SoundEvent COMMUNICATOR_PHONE_PICKUP = null;

    public static final SoundEvent ELECTRIC_SPARK = null;

    public static final SoundEvent EYE_MONITOR_INTERACT = null;
    public static final SoundEvent STEAMPUNK_MONITOR_INTERACT = null;

    public static final SoundEvent SNAP = null;
    
    public static final SoundEvent LASER_GUN_FIRE = null;

    @SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
        event.getRegistry().registerAll(
        		
        	// TARDIS
	        setupSound("tardis_takeoff"),
	        setupSound("tardis_land"),
	        setupSound("tardis_fly_loop"),
	        setupSound("rotor_start"),
	        setupSound("rotor_tick"),
	        setupSound("rotor_end"),

	        setupSound("door_open"),
	        setupSound("door_close"),
	        setupSound("door_lock"),
	        setupSound("door_unlock"),
	        setupSound("door_knock"),
            setupSound("car_lock"),
            setupSound("wood_door_close"),
            setupSound("wood_door_open"),

            setupSound("handbrake_engage"),
            setupSound("handbrake_release"),
            setupSound("throttle"),
            setupSound("randomiser"),
            setupSound("single_cloister"),
            setupSound("alarm_low"),
            setupSound("generic_one"),
            setupSound("generic_two"),
	        setupSound("generic_three"),
            setupSound("dimension"),
            setupSound("direction"),
            setupSound("landing_type_up"),
            setupSound("landing_type_down"),
            setupSound("cant_start"),
            setupSound("reached_destination"),
            setupSound("refuel_start"),
            setupSound("refuel_stop"),
            setupSound("takeoff_tv"),
            setupSound("land_tv"),
            setupSound("fly_loop_tv"),
            setupSound("master_takeoff"),
            setupSound("master_land"),

            // Communicator
            setupSound("communicator_beep"),
            setupSound("communicator_ring"),
            setupSound("communicator_phone_pickup"),
            setupSound("communicator_steam"),

            setupSound("stabilizer_on"),
            setupSound("stabilizer_off"),
            setupSound("telepathic_circuit"),


            // Sonic Devices
            setupSound("sonic_generic"),
            setupSound("sonic_tuning"),
            setupSound("sonic_fail"),
            setupSound("sonic_broken"),
            
            // Vortex Manipulator
            setupSound("vm_teleport"),
            setupSound("vm_button"),
            setupSound("vm_teleport_dest"),
            
            //Stattenheim Remote
            setupSound("remote_accept"),
            
            // Blocks
            setupSound("paper_drop"),
            //Entity
            setupSound("dalek_exterminate"),
            setupSound("dalek_fire"),
            setupSound("dalek_hover"),
            setupSound("dalek_death"),
            setupSound("dalek_moves"),
            setupSound("bessie_drive"),
            setupSound("bessie_horn"),
            setupSound("shield_hum"),

            //Special Weapon
            setupSound("dalek_sw_fire"),
            setupSound("dalek_sw_aim"),
            setupSound("dalek_sw_hit_explode"),

            //Hums
            setupSound("tardis_hum_63"),
            setupSound("tardis_hum_70"),
            setupSound("tardis_hum_80"),
            setupSound("tardis_hum_copper"),
            setupSound("tardis_hum_coral"),
            setupSound("tardis_hum_toyota"),
            
            //Ambient noises
            setupSound("ambient_creaks"),
            setupSound("electric_spark"),
            setupSound("steam_hiss"),

            //Monitor Sounds
            setupSound("eye_monitor_interact"),
            setupSound("steampunk_monitor_interact"),

            //Items
            setupSound("watch_malfunction"),
            setupSound("watch_tick"),
            setupSound("laser_gun_fire"),

            setupSound("snap")

        );
    }

    private static SoundEvent setupSound(String soundName) {
        return new SoundEvent(new ResourceLocation(Tardis.MODID, soundName)).setRegistryName(soundName);
    }
    
}
