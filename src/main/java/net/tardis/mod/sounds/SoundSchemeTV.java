package net.tardis.mod.sounds;

public class SoundSchemeTV extends SoundSchemeBasic{

	public SoundSchemeTV() {
		super(() -> TSounds.TAKEOFF_TV, () -> TSounds.LAND_TV, () -> TSounds.TARDIS_FLY_LOOP);
	}

	@Override
	public int getLandTime() {
		return 240;
	}

	@Override
	public int getTakeoffTime() {
		return 240;
	}
	
}
