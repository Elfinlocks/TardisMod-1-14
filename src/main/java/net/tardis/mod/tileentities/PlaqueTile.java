package net.tardis.mod.tileentities;

import java.util.UUID;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.client.ClientHelper;

public class PlaqueTile extends TileEntity{
	
	private String[] text;

	public PlaqueTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public PlaqueTile() {
		this(TTiles.PLAQUE);
	}
	
	public String[] getText() {
		if(this.text == null && this.world != null && world.isRemote) {
			this.text = new String[] {
					"TT Type 40",
					"Kasterborous Blackhole Shipyards",
					("Captain: " + this.getUsername())
			};
		}
		return text;
	}

	@OnlyIn(Dist.CLIENT)
	public String getUsername() {
		try {
			UUID id = UUID.fromString(DimensionType.getKey(this.getWorld().getDimension().getType()).getPath());
			return ClientHelper.getUsernameFromUUID(id).getFormattedText();
		}
		catch(IllegalArgumentException e) {}
		return "";
	}
}
