package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.common.util.Constants;

public class TardisEngineTile extends TileEntity{
	
	public List<Direction> openDoors = new ArrayList<Direction>();
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -2, -2, 2, 5, 2);
	
	public TardisEngineTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public TardisEngineTile() {
		this(TTiles.ENGINE);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.openDoors.clear();
		ListNBT list = pkt.getNbtCompound().getList("directions", Constants.NBT.TAG_INT);
		for(INBT face : list) {
			this.openDoors.add(Direction.values()[((IntNBT)face).getInt()]);
		}
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), 99, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = new CompoundNBT();
		ListNBT directions = new ListNBT();
		for(Direction dir : this.openDoors) {
			directions.add(new IntNBT(dir.ordinal()));
		}
		tag.put("directions", directions);
		return this.write(tag);
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
		this.openDoors.clear();
		ListNBT list = tag.getList("directions", Constants.NBT.TAG_INT);
		for(INBT face : list) {
			this.openDoors.add(Direction.values()[((IntNBT)face).getInt()]);
		}
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

}
