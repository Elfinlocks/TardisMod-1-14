package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;

public class MultiblockTile extends TileEntity implements IMultiblock{

	private BlockPos masterPos = BlockPos.ZERO;
	
	public MultiblockTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public MultiblockTile() {
		super(TTiles.MUTIBLOCK);
	}

	@Override
	public BlockPos getMaster() {
		return this.masterPos;
	}

	@Override
	public BlockState getMasterState() {
		return world.getBlockState(masterPos);
	}

	@Override
	public MultiblockMasterTile getMasterTile() {
		return (MultiblockMasterTile)world.getTileEntity(masterPos);
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		this.masterPos = BlockPos.fromLong(compound.getLong("master_pos"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putLong("master_pos", this.masterPos.toLong());
		return super.write(compound);
	}

	@Override
	public void setMasterPos(BlockPos pos) {
		this.masterPos = pos;
		this.markDirty();
	}
	
	
}
