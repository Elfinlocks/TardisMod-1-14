package net.tardis.mod.tileentities.console.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.tileentities.ConsoleTile;

public class MonitorOverride implements INBTSerializable<CompoundNBT>{

	String[] text;
	long time;
	
	public MonitorOverride(ConsoleTile tile, int time, String... text) {
		this.time = time + tile.getWorld().getGameTime();
		this.text = text;
	}
	
	public MonitorOverride(CompoundNBT compound) {
		this.deserializeNBT(compound);
	}

	public boolean shouldRemove(ConsoleTile tile) {
		return tile.getWorld().getGameTime() >= this.time;
	}
	
	public String[] getText() {
		return this.text;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putLong("time", this.time);
		
		ListNBT text = new ListNBT();
		for(String s : this.text) {
			text.add(new StringNBT(s));
		}
		
		tag.put("text", text);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.time = tag.getLong("time");
		
		
		ListNBT list = tag.getList("text", NBT.TAG_STRING);
		this.text = new String[list.size()];
		
		int i = 0;
		for(INBT n : list) {
			this.text[i] = ((StringNBT)n).getString();
			++i;
		}
	}
}
