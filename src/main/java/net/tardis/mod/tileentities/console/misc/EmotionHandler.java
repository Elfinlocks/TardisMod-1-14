package net.tardis.mod.tileentities.console.misc;

import java.util.List;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.registries.TardisForgeRegistries;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.traits.TardisTraitType;

public class EmotionHandler implements ITickable, INBTSerializable<CompoundNBT>{
	
	private static final ResourceLocation SAVE_KEY = new ResourceLocation(Tardis.MODID, "emotion");
	private TardisTrait[] traits = new TardisTrait[5];
	private ConsoleTile tile;
	/*
	 * A value between -100 and 100
	 * 100 a Tardis has all the perks (Transmat when low health, snap to open door, etc)
	 */
	private int loyalty = -100;
	private double mood = EnumHappyState.CONTENT.threshhold;
	//The last time a player occupied this TARDIS interior
	private long lastTimePlayer = 0l;
	private boolean hasGeneratedTraits = false;
	
	public EmotionHandler(ConsoleTile console) {
		console.registerDataHandler(SAVE_KEY, this);
		console.registerTicker(this);
		this.tile = console;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.loyalty = tag.getInt("loyalty");
		this.mood = tag.getInt("mood");
		this.hasGeneratedTraits = tag.getBoolean("has_generated_traits");
		
		traits = new TardisTrait[5];
		
		if(tag.contains("trait_list")) {
			ListNBT list = tag.getList("trait_list", NBT.TAG_COMPOUND);
			int i = 0;
			for(INBT base : list) {
				CompoundNBT nbt = (CompoundNBT)base;
				TardisTrait trait = TardisForgeRegistries.TRAITS.getValue(new ResourceLocation(nbt.getString("registry_name"))).create();
				trait.setWeight(nbt.getDouble("weight"));
				this.traits[i] = trait;
				++i;
			}
		}
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("loyalty", this.loyalty);
		tag.putDouble("mood", this.mood);
		tag.putBoolean("has_generated_traits", this.hasGeneratedTraits);
		
		ListNBT traits = new ListNBT();
		for(TardisTrait trait : this.traits) {
			if(trait != null) {
				CompoundNBT traitTag = new CompoundNBT();
				traitTag.putString("registry_name", trait.getType().getRegistryName().toString());
				traitTag.putDouble("weight", trait.getWeight());
				traits.add(traitTag);
			}
		}
		tag.put("trait_list", traits);
		return tag;
	}

	@Override
	public void tick(ConsoleTile console) {
		
		if(!console.getWorld().isRemote)
			fixOldData();
		
		if(console.getWorld().getGameTime() % 80 == 0) {
			double prevMood = mood;
			
			//Tardis in flight mood stuff
			if(console.isInFlight()) {
				//Unstabilized Flight doubles the mood
				if(mood < EnumHappyState.ECSTATIC.getTreshold() * 1.5) {
					console.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
						mood += (sys.isActivated() ? 1 : 2);
					});
				}
					
			}
			
			//If server, and interior empty, and player is on
			if(!console.getWorld().isRemote) {
				if(((ServerWorld)console.getWorld()).getPlayers().isEmpty() && this.mood > EnumHappyState.SAD.getTreshold() * 1.75) {
					
					//If player is online
					if(console.getWorld().getPlayerByUuid(console.getOwner()) != null) {
						long abandonedTime = console.getWorld().getGameTime() - this.lastTimePlayer;
						//If left for more than a day
						if(abandonedTime > 24000) {
							//If abandoned for more than 1 day, tank mood
							this.mood -= 0.5;
						}
					}
				}
				else {
					//If not empty, set that the lastPlayerTime
					this.lastTimePlayer = console.getWorld().getGameTime();
				}
				
				//Leave if pissed
				if(this.loyalty <= -50 && this.mood < EnumHappyState.DISCONTENT.threshhold) {
					if(ConsoleTile.rand.nextDouble() < 0.01) {
						getPissyAndLeave(console);
					}
				}
				
			}
			
			//Particle Spawning
			if(prevMood > this.mood) {
				console.getWorld().addParticle(ParticleTypes.ANGRY_VILLAGER,
						console.getPos().getX() + 0.5, console.getPos().getY() + 2, console.getPos().getZ() + 0.5, 0, 0.5, 0);
			}
			else if(prevMood < this.mood) {
				console.getWorld().addParticle(ParticleTypes.HEART,
						console.getPos().getX() + 0.5, console.getPos().getY() + 2, console.getPos().getZ() + 0.5, 0, 0.5, 0);
			}
			
		}
		
		
		for(TardisTrait trait : this.traits) {
			if(trait != null)
				trait.tick(console);
		}
		
	}
	
	private void getPissyAndLeave(ConsoleTile console) {
		if(!console.isInFlight()) {
			if(console.getControl(HandbrakeControl.class).isFree()) {
				console.getControl(ThrottleControl.class).setAmount(1.0F);
				console.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> sys.setActivated(true));
				console.setDestination(console.getDimension(), console.randomizeCoords(console.getLocation(), 150));
				console.takeoff();
				Tardis.LOGGER.log(Level.DEBUG, console.getOwner() + "'s TARDIS left them!");
			}
		}
	}
	
	public void fixOldData() {
		if(!this.hasGeneratedTraits)
			this.onInitialSpawn();
	}
	
	public void setLoyalty(int loyalty) {
		this.loyalty = loyalty;
	}
	
	public int getLoyalty() {
		return loyalty;
	}
	
	public double getMood() {
		return this.mood;
	}
	
	public EnumHappyState getMoodState() {
		for(EnumHappyState state : EnumHappyState.values()) {
			if(mood >= state.getTreshold())
				return state;
		}
		return EnumHappyState.SAD;
	}
	
	public void setMood(double mood) {
		this.mood = mood;
	}
	
	public void addMood(double mood) {
		this.mood += mood;
	}
	
	public void addLoyalty(int loyalty) {
		this.loyalty += loyalty;
	}
	
	public TardisTrait[] getTraits() {
		return this.traits;
	}
	
	public void onInitialSpawn() {
		
		traits = new TardisTrait[5];
		
		int i = 0;
		//Make a copy of registered traits
		List<TardisTraitType> possibleTraits = Lists.newArrayList(TardisForgeRegistries.TRAITS.getValues());
		
		for(int x = 0; x < 25; ++x) {
			//If there are no traits to choose from or the TARDIS has three, stop
			if(possibleTraits.isEmpty() || x >= 3)
				break;
			
			//Generated Type
			TardisTraitType type = possibleTraits.get(ConsoleTile.rand.nextInt(possibleTraits.size()));
			
			//If this TARDIS already has a trait that conflicts with this one
			if(hasIncompatibleTrait(type))
				continue;
			
			this.traits[i] = type.create().setWeight(ConsoleTile.rand.nextDouble());
			possibleTraits.remove(type);
			++i;
			
		}
		
		this.hasGeneratedTraits = true;
		
	}
	
	private boolean hasIncompatibleTrait(TardisTraitType type) {
		for(int y = 0; y < this.traits.length; ++y) {
			TardisTrait old = this.traits[y];
			//Stop if a pre-existing trait exists already
			if(old != null && !old.getType().isCompatible(type))
				return true;
		}
		return false;
	}
	
	public boolean addLoyaltyIfOwner(int loyalty, @Nullable PlayerEntity player) {
		if(player != null && player.getUniqueID().equals(tile.getOwner())) {
			this.addLoyalty(loyalty);
			return true;
		}
		return false;
	}

	public static enum EnumHappyState{
		ECSTATIC(100, "ecstatic"),
		HAPPY(90, "happy"),
		CONTENT(50, "content"),
		APATHETIC(0, "apathetic"),
		DISCONTENT(-20, "discontent"),
		SAD(-100, "sad");
		
		int threshhold = 0;
		TranslationTextComponent trans;
		
		EnumHappyState(int threshold, String key){
			this.threshhold = threshold;
			this.trans = new TranslationTextComponent("mood.tardis." + key);
		}
		
		public int getTreshold() {
			return this.threshhold;
		}
		
		public TranslationTextComponent getTranslationComponent() {
			return this.trans;
		}
	}
}
