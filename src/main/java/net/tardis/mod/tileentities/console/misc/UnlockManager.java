package net.tardis.mod.tileentities.console.misc;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.registries.consoles.Console;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * To Fix
 * @author Spectre0987
 *
 */
public class UnlockManager implements INBTSerializable<CompoundNBT>{

	private ConsoleTile tile;
	
	private List<Console> unlockedConsoles = Lists.newArrayList();
	private List<IExterior> unlockedExteriors = Lists.newArrayList();
	private List<ConsoleRoom> unlockedConsoleRoom = Lists.newArrayList();
	
	public UnlockManager(ConsoleTile tile) {
		this.tile = tile;
		this.unlockedExteriors.addAll(ExteriorRegistry.getDefaultExteriors());
		this.unlockedConsoleRoom.addAll(ConsoleRoom.getDefaults());
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		//Console Units
		ListNBT consolesList = new ListNBT();
		for(Console console : this.unlockedConsoles) {
			consolesList.add(new StringNBT(console.getRegistryName().toString()));
		}
		tag.put("consoles", consolesList);
		
		//Exteriors
		ListNBT exteriorList = new ListNBT();
		for(IExterior ext : this.unlockedExteriors) {
			exteriorList.add(new StringNBT(ext.getRegistryName().toString()));
		}
		tag.put("exteriors", exteriorList);
		
		//Console Room
		ListNBT consoleRoomList = new ListNBT();
		for(ConsoleRoom room : this.unlockedConsoleRoom) {
			consoleRoomList.add(new StringNBT(room.getRegistryName().toString()));
		}
		tag.put("console_room", consoleRoomList);
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		
		this.unlockedExteriors.clear();
		this.unlockedConsoleRoom.clear();
		this.unlockedConsoles.clear();
		
		//Exteriors
		ListNBT exteriorList = tag.getList("exteriors", NBT.TAG_STRING);
		for(INBT nbt : exteriorList) {
			IExterior ext = ExteriorRegistry.getExterior(new ResourceLocation(((StringNBT)nbt).getString()));
			if(ext != null && !this.unlockedExteriors.contains(ext))
				this.unlockedExteriors.add(ext);
		}
		
		//ConsoleRooms
		ListNBT roomList = tag.getList("console_room", NBT.TAG_STRING);
		for(INBT nbt : roomList) {
			ResourceLocation key = new ResourceLocation(((StringNBT)nbt).getString());
			ConsoleRoom room = ConsoleRoom.REGISTRY.get(key);
			if(room != null)
				this.unlockedConsoleRoom.add(room);
		}
		
	}
	
	public Collection<IExterior> getUnlockedExteriors() {
		return Collections.unmodifiableCollection(this.unlockedExteriors);
	}

	public void addExterior(IExterior ext) {
		this.unlockedExteriors.add(ext);
		tile.updateClient();
	}
	
	public Collection<ConsoleRoom> getUnlockedConsoleRooms() {
		return Collections.unmodifiableCollection(this.unlockedConsoleRoom);
	}

	public void addConsoleRoom(ConsoleRoom room) {
		this.unlockedConsoleRoom.add(room);
		tile.updateClient();
	}
	
}
