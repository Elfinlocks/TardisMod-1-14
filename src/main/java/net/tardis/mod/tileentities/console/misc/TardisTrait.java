package net.tardis.mod.tileentities.console.misc;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.traits.TardisTraitType;

public abstract class TardisTrait{
	
	public static final TranslationTextComponent DISLIKES_LOCATION = new TranslationTextComponent("trait.tardis.dislike_location");
	public static final TranslationTextComponent LIKES_LOCATION = new TranslationTextComponent("trait.tardis.like_location");
	
	public static final TranslationTextComponent GENERIC_LIKE = new TranslationTextComponent("trait.tardis.generic_like");
	public static final TranslationTextComponent GENERIC_DISLIKE = new TranslationTextComponent("trait.tardis.generic_dislike");
	
	private double weight = 1.0;
	private TardisTraitType type;
	
	private TardisTrait() {}
	
	public TardisTrait(TardisTraitType type) {
		this.type = type;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public double getModifier() {
		return 1.0F - this.getWeight();
	}
	
	public TardisTrait setWeight(double weight) {
		this.weight = weight;
		return this;
	}
	
	public TardisTraitType getType() {
		return this.type;
	}
	
	public abstract void tick(ConsoleTile tile);
	
	public void warnPlayer(ConsoleTile tile, TranslationTextComponent text) {
		if(!tile.getWorld().isRemote) {
			ServerPlayerEntity player = tile.getWorld().getServer().getPlayerList().getPlayerByUUID(tile.getOwner());
			if(player != null) {
				player.getCapability(Capabilities.PLAYER_DATA).ifPresent(data -> {
					if(data.getConnectedToTARDISTicks() > 0)
						player.sendStatusMessage(text, true);
				});
			}
		}
	}
	
	public void warnPlayer(ConsoleTile tile, TranslationTextComponent trans, int interval) {
		if(tile.getWorld().getGameTime() % interval == 0) {
			this.warnPlayer(tile, trans);
		}
	}
	
}
