package net.tardis.mod.contexts.gui;

import net.tardis.mod.controls.IControl;
import net.tardis.mod.misc.GuiContext;

public class ControlContext extends GuiContext {

	public IControl control;
	
	public ControlContext(IControl control) {
		this.control = control;
	}
}
