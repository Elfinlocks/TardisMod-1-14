package net.tardis.mod.registries;

import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.sounds.SoundSchemeBase;
import net.tardis.mod.sounds.SoundSchemeBasic;
import net.tardis.mod.sounds.SoundSchemeMaster;
import net.tardis.mod.sounds.SoundSchemeTV;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class SoundSchemeRegistry {

	public static final SoundSchemeBasic BASIC = register(new SoundSchemeBasic(), "basic");
	public static final SoundSchemeTV TV = register(new SoundSchemeTV(), "tv");
	public static final SoundSchemeMaster MASTER = register(new SoundSchemeMaster(), "master");
	
	public static <T extends SoundSchemeBase> T register(T scheme, String name) {
		scheme.setRegistryName(Helper.createRL(name));
		return scheme;
	}
	
	@SubscribeEvent
	public static void onRegister(final Register<SoundSchemeBase> event) {
		event.getRegistry().registerAll(
				BASIC,
				TV,
				MASTER
		);
	}
}
