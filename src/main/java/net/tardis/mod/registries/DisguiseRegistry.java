package net.tardis.mod.registries;

import java.util.function.BiFunction;

import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.Disguise;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class DisguiseRegistry {
	
	public static Disguise OAK_TREE;


	public static Disguise CACTUS;
	public static Disguise CACTUS_TALL;
	public static Disguise DESERT_HOUSE;
	public static Disguise STONE_PILLAR;

	public static Disguise BROWN_MUSHROOM_SMALL;
	public static Disguise BROWN_MUSHROOM_MEDIUM;
	public static Disguise BROWN_MUSHROOM_BIG;

	public static Disguise SPRUCE_TREE;
	public static Disguise SPRUCE_TREE_A;
	public static Disguise SPRUCE_TREE_B;
	public static Disguise SPRUCE_TREE_C;
	public static Disguise SPRUCE_TREE_D;
	public static Disguise SPRUCE_TREE_E;

	public static Disguise BIRCH_TREE;
	public static Disguise BIRCH_TREE_A;
	public static Disguise BIRCH_TREE_B;
	public static Disguise BIRCH_TREE_C;
	public static Disguise BIRCH_TREE_D;
	public static Disguise BIRCH_TREE_E;
	public static Disguise BIRCH_TREE_F;

	public static Disguise ACACIA_TREE;
	public static Disguise ACACIA_TREE_A;
	public static Disguise ACACIA_TREE_B;
	public static Disguise ACACIA_TREE_C;
	public static Disguise ACACIA_TREE_D;
	public static Disguise ACACIA_TREE_E;
	
	//Ocean
	public static Disguise CORAL_RED;
	public static Disguise CORAL_TUBE;
	public static Disguise CORAL_BRAIN;
	public static Disguise CORAL_BUBBLE;
	
	public static Disguise OCEAN_DEFAULT;
	
	//Nether
	public static Disguise NETHERRACK_PILLAR;
	public static Disguise NETHER_FORT;


	public static BiFunction<World, BlockPos, Boolean> DEFAULT_TREE_CRITERON = (world, pos) -> world.canBlockSeeSky(pos) && world.getDimension().isSurfaceWorld();
	
	@SubscribeEvent
	public static void registerAll(RegistryEvent.Register<Disguise> event) {
		
		BiFunction<World, BlockPos, Boolean> coral_criteron = (world, pos) -> world.getBiome(pos) == Biomes.WARM_OCEAN;
		BiFunction<World, BlockPos, Boolean> nether = (world, pos) -> world.getDimension().getType() == DimensionType.THE_NETHER;
		
		event.getRegistry().registerAll(
				
			OAK_TREE = register(new Disguise(() -> Blocks.OAK_LOG.getDefaultState()), "oak_tree", (world, pos) -> (world.getBiome(pos).getCategory() == Category.FOREST || world.getBiome(pos).getCategory() == Category.PLAINS) && DEFAULT_TREE_CRITERON.apply(world, pos)),


			// Brown Mushrooms
			BROWN_MUSHROOM_SMALL = register(new Disguise(() -> Blocks.MUSHROOM_STEM.getDefaultState()), "brown_mushroom_small", (world, pos) -> world.getBiome(pos).getCategory() == Category.MUSHROOM && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BROWN_MUSHROOM_MEDIUM = register(new Disguise(() -> Blocks.MUSHROOM_STEM.getDefaultState()), "brown_mushroom_medium", (world, pos) -> world.getBiome(pos).getCategory() == Category.MUSHROOM && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BROWN_MUSHROOM_BIG = register(new Disguise(() -> Blocks.MUSHROOM_STEM.getDefaultState()), "brown_mushroom_big", (world, pos) -> world.getBiome(pos).getCategory() == Category.MUSHROOM && DEFAULT_TREE_CRITERON.apply(world, pos)),

			// Spruce Tree
			
			SPRUCE_TREE_A = register(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), "spruce_tree_a", (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			SPRUCE_TREE_B = register(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), "spruce_tree_b", (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			SPRUCE_TREE_C = register(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), "spruce_tree_c", (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			SPRUCE_TREE_D = register(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), "spruce_tree_d", (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			SPRUCE_TREE_E = register(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), "spruce_tree_e", (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)),

			// Acacia Tree
			ACACIA_TREE = register(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), "acacia_tree", (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			ACACIA_TREE_A = register(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), "acacia_tree_a", (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			ACACIA_TREE_B = register(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), "acacia_tree_b", (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			ACACIA_TREE_C = register(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), "acacia_tree_c", (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			ACACIA_TREE_D = register(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), "acacia_tree_d", (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			ACACIA_TREE_E = register(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), "acacia_tree_e", (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)),

			// Birch Tree
			BIRCH_TREE = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BIRCH_TREE_A = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree_a", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BIRCH_TREE_B = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree_b", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BIRCH_TREE_C = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree_c", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BIRCH_TREE_D = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree_d", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BIRCH_TREE_E = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree_e", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),
			BIRCH_TREE_F = register(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), "birch_tree_f", (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)),

			SPRUCE_TREE = register(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), "spruce_tree", (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)),
			
			CACTUS = register(new Disguise(() -> Blocks.CACTUS.getDefaultState()), "cactus", (world, pos) -> world.getBiome(pos).getCategory() == Category.DESERT && DEFAULT_TREE_CRITERON.apply(world, pos)),
			CACTUS_TALL = register(new Disguise(() -> Blocks.CACTUS.getDefaultState()), "cactus_tall", (world, pos) -> world.getBiome(pos).getCategory() == Category.DESERT && DEFAULT_TREE_CRITERON.apply(world, pos)),
            
			CORAL_RED = register(new Disguise(() -> Blocks.FIRE_CORAL_BLOCK.getDefaultState()), "coral_fire", coral_criteron),
			CORAL_TUBE = register(new Disguise(() -> Blocks.TUBE_CORAL_BLOCK.getDefaultState()), "coral_tube", coral_criteron),
			CORAL_BRAIN = register(new Disguise(() -> Blocks.BRAIN_CORAL_BLOCK.getDefaultState()), "coral_brain", coral_criteron),
			CORAL_BUBBLE = register(new Disguise(() -> Blocks.BUBBLE_CORAL_BLOCK.getDefaultState()), "coral_bubble", coral_criteron),
			
			OCEAN_DEFAULT = register(new Disguise(() -> Blocks.SAND.getDefaultState()), "default_ocean", coral_criteron),
			
			NETHERRACK_PILLAR = register(new Disguise(() -> Blocks.NETHERRACK.getDefaultState()), "netherrack_pillar", nether),
			
			//Disable house structures until we can serialise blockstate rotations
//			DESERT_HOUSE = register(new Disguise(() -> Blocks.SPRUCE_DOOR.getDefaultState().with(DoorBlock.HALF, DoubleBlockHalf.UPPER), () -> Blocks.SPRUCE_DOOR.getDefaultState()), "desert_house", (world, pos) -> {
//				BlockPos n = world.findNearestStructure("village", pos, 20, false);
//				return world.getBiome(pos).getCategory() == Category.DESERT && n != null;
//			}),
			
			
			
			STONE_PILLAR = register(new Disguise(() -> Blocks.STONE.getDefaultState()), "stone", (world, pos) -> false)
		);
		
	}
	
	public static <T extends Disguise> T register(T dis, String name, BiFunction<World, BlockPos, Boolean> valid) {
		dis.setRegistryName(new ResourceLocation(Tardis.MODID, name));
		dis.setValidationFunction(valid);
		return dis;
	}

}
