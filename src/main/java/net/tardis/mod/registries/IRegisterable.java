package net.tardis.mod.registries;

import net.minecraft.util.ResourceLocation;

public interface IRegisterable<T> {

	T setRegistryName(ResourceLocation regName);
	ResourceLocation getRegistryName();
}
