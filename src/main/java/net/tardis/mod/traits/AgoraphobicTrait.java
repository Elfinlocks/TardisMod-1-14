package net.tardis.mod.traits;

import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.TardisTrait;

public class AgoraphobicTrait extends TardisTrait{

	public AgoraphobicTrait(TardisTraitType type) {
		super(type);
		
	}

	@Override
	public void tick(ConsoleTile tile) {}

}
