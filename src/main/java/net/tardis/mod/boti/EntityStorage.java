package net.tardis.mod.boti;

import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class EntityStorage extends AbstractEntityStorage{

	public double posX, posY, posZ;
	public float yaw, pitch;
	public ResourceLocation type;
	public UUID id;
	public CompoundNBT data;
	
	public EntityStorage(Entity e) {
		posX = e.posX;
		posY = e.posY;
		posZ = e.posZ;
		
		yaw = e.rotationYaw;
		pitch = e.rotationPitch;
		
		id = e.getUniqueID();
		type = e.getType().getRegistryName();
		data = e.serializeNBT();
	}
	
	public EntityStorage(PacketBuffer buf) {
		this.decode(buf);
	}

	@Override
	public void encode(PacketBuffer buf) {
		buf.writeDouble(posX);
		buf.writeDouble(posY);
		buf.writeDouble(posZ);
		
		buf.writeFloat(yaw);
		buf.writeFloat(pitch);
		
		buf.writeUniqueId(id);
		buf.writeResourceLocation(type);
		buf.writeCompoundTag(data);
		
	}

	@Override
	public void decode(PacketBuffer buf) {
		posX = buf.readDouble();
		posY = buf.readDouble();
		posZ = buf.readDouble();
		
		yaw = buf.readFloat();
		pitch = buf.readFloat();
		
		id = buf.readUniqueId();
		type = buf.readResourceLocation();
		data = buf.readCompoundTag();
	}

	public void updateEntity(Entity e) {
		
		e.lastTickPosX = e.prevPosX = e.posX;
		e.lastTickPosY = e.prevPosY = e.posY;
		e.lastTickPosZ = e.prevPosZ = e.posZ;
		
		e.posX = posX;
		e.posY = posY;
		e.posZ = posZ;
		
		e.prevRotationYaw = this.yaw;
		e.prevRotationPitch = this.pitch;
		e.rotationYaw = this.yaw;
		e.rotationPitch = this.pitch;
	}
}
