package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;

public class FlightEventFactory extends ForgeRegistryEntry<FlightEventFactory>{

	private Function<ArrayList<ResourceLocation>, FlightEvent> factory;
	private Supplier<ArrayList<ResourceLocation>> sequence;
	private boolean isNormal = true;
	
	public FlightEventFactory(Function<ArrayList<ResourceLocation>, FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence) {
		this.factory = event;
		this.sequence = sequence;
	}
	
	public FlightEventFactory(Function<ArrayList<ResourceLocation>, FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence, boolean normal) {
		this(event, sequence);
		this.isNormal = normal;
	}
	
	public FlightEvent create(ConsoleTile tile) {
		FlightEvent event = this.factory.apply(this.sequence.get());
		event.setRegistryName(getRegistryName());
		event.calcTime(tile);
		return event;
	}
	
	public boolean isNormal() {
		return this.isNormal;
	}
	
	public void setNormal(boolean normal) {
		this.isNormal = normal;
	}

}
