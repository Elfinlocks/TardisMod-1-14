package net.tardis.mod.flight;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.controls.IControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class FlightEvent {

	private TranslationTextComponent trans;
	protected ResourceLocation registryKey;
	private List<ResourceLocation> controlsLeft;
	private int timeUntilMiss = 0;
	private boolean isComplete = false;
	
	public FlightEvent(ResourceLocation... controls) {
		this(Lists.newArrayList(controls));
	}
	
	public FlightEvent(List<ResourceLocation> controls) {
		this();
		this.controlsLeft = controls;
	}
	
	private FlightEvent() {}
	
	/**
	 * 
	 * @param tile
	 * @return - True if completed successfully, false if missed
	 */
	public boolean onComplete(ConsoleTile tile) {
		if(!this.controlsLeft.isEmpty()) {
			this.onMiss(tile);
			
			Random rand = tile.getWorld().rand;
			
			tile.getWorld().playSound(null, tile.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 0.5F, 1F);
			for (LivingEntity ent : tile.getWorld().getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(tile.getPos()).grow(20))) {
				ent.setMotion(ent.getMotion().add(rand.nextDouble() - 0.5, rand.nextDouble(), rand.nextDouble() - 0.5));
				if (ent instanceof ServerPlayerEntity) {
					Network.sendTo(new MissControlMessage(), (ServerPlayerEntity) ent);
				}
			}
			tile.getEmotionHandler().setMood(tile.getEmotionHandler().getMood() - 10);
			tile.getEmotionHandler().setLoyalty(tile.getEmotionHandler().getLoyalty() - 5);
			for(Subsystem s : tile.getSubSystems()) {
				s.explode(true);
			}
			return false;
		}
		tile.getEmotionHandler().addLoyaltyIfOwner(1, tile.getPilot());
		return true;
	}
	
	public void onMiss(ConsoleTile tile) {}
	
	public boolean onControlHit(ConsoleTile console, IControl control) {
		if(this.controlsLeft.contains(control.getRegistryName())) {
			this.controlsLeft.remove(control.getRegistryName());
			if(this.controlsLeft.isEmpty()) {
				this.isComplete = true;
				console.updateClient();
			}
			return true;
		}
		return false;
	}
	
	public int calcTime(ConsoleTile console) {
		
		//flight time + 5 seconds for base + 0-5 seconds depending on throttle
		return this.timeUntilMiss = console.flightTicks + (5 * 20) + (int)Math.floor((1.0F - console.getControl(ThrottleControl.class).getAmount()) * 5 * 20);
	}
	
	public int getMissedTime() {
		return this.timeUntilMiss;
	}
	
	public List<ResourceLocation> getControls(){
		return this.controlsLeft;
	}
	
	public TranslationTextComponent getTranslation() {
		return this.trans;
	}
	
	public FlightEvent setRegistryName(ResourceLocation name) {
		this.registryKey = name;
		this.trans = new TranslationTextComponent("flight_events." + name.getNamespace() + "." + name.getPath());
		return this;
	}

	public ResourceLocation getRegistryName() {
		return this.registryKey;
	}

	public static interface IFlightConsequence{
		void onMissed(ConsoleTile tile);
	}

	public void warnPlayers(World world, BlockPos pos) {
		if(!world.isRemote) {
			for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(pos).grow(26))) {
				player.sendStatusMessage(getTranslation(), true);
			}
		}
	}
	
	public boolean isComplete() {
		return this.isComplete;
	}
    
}
