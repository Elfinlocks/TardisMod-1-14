package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisCollideRecieve extends FlightEvent{

	public static final Supplier<ArrayList<ResourceLocation>> CONTROLS = () -> Lists.newArrayList(
			ControlRegistry.COMMUNICATOR.getRegistryName(),
			ControlRegistry.RANDOM.getRegistryName(),
			ControlRegistry.DIMENSION.getRegistryName()
	);
	
	ConsoleTile other;
	
	public TardisCollideRecieve(ArrayList<ResourceLocation> list) {
		super(list);
	}
	
	public TardisCollideRecieve setOtherTARDIS(ConsoleTile other) {
		this.other = other;
		return this;
	}

	@Override
	public boolean onComplete(ConsoleTile tile) {
		boolean success = super.onComplete(tile);
		
		if(success && other != null) {
			other.setDestination(tile.getWorld().getDimension().getType(), TardisHelper.TARDIS_POS.south(3 + tile.getWorld().rand.nextInt(5)));
			other.initLand();
		}
		
		return success;
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		tile.damage(40);
		
		if(other != null) {
			other.damage(40);
			
			other.crash();
			other.updateClient();
		}
		
		tile.crash();
		tile.updateClient();
		
	}
	
	@Override
	public int calcTime(ConsoleTile console) {
		return console.flightTicks + 200;
	}
}
