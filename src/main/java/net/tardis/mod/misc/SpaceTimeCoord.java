package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;

public class SpaceTimeCoord{
	
	public static final SpaceTimeCoord UNIVERAL_CENTER = new SpaceTimeCoord(DimensionType.getKey(DimensionType.OVERWORLD), BlockPos.ZERO, Direction.NORTH);
	
	private BlockPos pos;
	private ResourceLocation dimType;
	private Direction facing;
	private String name = "";
	
	public SpaceTimeCoord(DimensionType type, BlockPos pos, Direction dir) {
		this(DimensionType.getKey(type), pos, dir);
	}
	
	public SpaceTimeCoord(ResourceLocation type, BlockPos pos, Direction dir) {
		this.dimType = type;
		this.pos = pos;
		this.facing = dir;
	}
	
	public SpaceTimeCoord(DimensionType type, BlockPos pos) {
		this(type, pos, Direction.NORTH);
	}
	
	public BlockPos getPos() {
		return pos;
	}

	public ResourceLocation getDimType() {
		return dimType;
	}
	
	public DimensionType getDim() {
		return DimensionType.byName(dimType);
	}

	public Direction getFacing() {
		return facing;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public CompoundNBT serialize() {
		CompoundNBT tag = new CompoundNBT();
		tag.putLong("pos", this.pos.toLong());
		tag.putString("dim", this.dimType.toString());
		tag.putInt("dir", this.facing.ordinal());
		tag.putString("name", this.name);
		return tag;
	}
	
	public static SpaceTimeCoord deserialize(CompoundNBT tag) {
		SpaceTimeCoord coord = new SpaceTimeCoord(new ResourceLocation(tag.getString("dim")), BlockPos.fromLong(tag.getLong("pos")), Direction.values()[tag.getInt("dir")]);
		if(tag.contains("name"))
			coord.setName(tag.getString("name"));
		return coord;
	}
	
	public SpaceTimeCoord toImmutable() {
		
		SpaceTimeCoord coord = new SpaceTimeCoord(DimensionType.byName(this.dimType), this.pos);
		if(this.name != null)
			coord.setName(name);
		return coord;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		if(!(obj instanceof SpaceTimeCoord))
			return false;
		SpaceTimeCoord other = (SpaceTimeCoord)obj;
		if(pos.equals(other.pos) && dimType.equals(other.dimType))
			return true;
		return false;
	}

}
