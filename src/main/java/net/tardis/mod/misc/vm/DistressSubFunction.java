package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;


/**
 * Created by 50ap5ud5
 * on 9 Jul 2020 @ 7:56:02 pm
 */
public class DistressSubFunction extends SubFunction {

	public DistressSubFunction(ParentFunction parent) {
		super(parent);
	}

	@Override
	public void onActivated(World world, PlayerEntity player) {
		if (world.isRemote && Helper.canVMTravelToDimension(world.getDimension().getType())) {
			Tardis.proxy.openGUI(Constants.Gui.VORTEX_DISTRESS, null);
			super.onActivated(world, player);
		}
		else {
			player.sendStatusMessage(new TranslationTextComponent("message.vm.forbidden"), false);
		}
	}

	@Override
	public Boolean isServerSide() {
		return super.isServerSide();
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("subfunction.vm.distress_beacon").getFormattedText();
	}

}
