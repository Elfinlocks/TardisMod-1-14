package net.tardis.mod.misc;

import net.minecraft.util.SoundEvent;

public interface IDoorSoundScheme {

	SoundEvent getOpenSound();
	SoundEvent getClosedSound();
}
