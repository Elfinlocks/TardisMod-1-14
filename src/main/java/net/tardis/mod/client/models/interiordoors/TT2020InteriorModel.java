package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.TT2020CapsuleExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class TT2020InteriorModel extends Model implements IInteriorDoorRenderer{
	
	private final RendererModel door;
	private final RendererModel door_west_rotate_y;
	private final RendererModel door_dot_west_a;
	private final RendererModel door_dot_west_b;
	private final RendererModel door_dot_west_c;
	private final RendererModel door_dot_west_d;
	private final RendererModel door_dot_west_e;
	private final RendererModel door_east_rotate_y;
	private final RendererModel door_dot_east_a;
	private final RendererModel door_dot_east_b;
	private final RendererModel door_dot_east_c;
	private final RendererModel door_dot_east_d;
	private final RendererModel door_dot_east_e;
	private final RendererModel door_jam;
	private final RendererModel boti;

	public TT2020InteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		door = new RendererModel(this);
		door.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		door_west_rotate_y = new RendererModel(this);
		door_west_rotate_y.setRotationPoint(-60.0F, -72.0F, -30.0F);
		door.addChild(door_west_rotate_y);
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 397, 249, 36.0F, 25.0F, 19.0F, 24, 44, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 397, 206, 36.0F, -19.0F, 19.0F, 24, 44, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 397, 163, 36.0F, -63.0F, 19.0F, 24, 44, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 56, 105, 38.0F, -61.0F, 23.0F, 20, 42, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 56, 105, 38.0F, 25.0F, 23.0F, 20, 42, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 56, 105, 38.0F, -19.0F, 23.0F, 20, 44, 4, 0.0F, false));

		door_dot_west_a = new RendererModel(this);
		door_dot_west_a.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_a);
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -18.0F, -129.0F, -10.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -17.0F, -130.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -19.0F, -128.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -6.0F, -128.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -5.0F, -126.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -20.0F, -126.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -17.0F, -117.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -15.0F, -116.0F, -10.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -15.0F, -131.0F, -10.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_b = new RendererModel(this);
		door_dot_west_b.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_b);
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -18.0F, -102.0F, -10.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -17.0F, -103.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -19.0F, -101.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -6.0F, -101.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -5.0F, -99.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -20.0F, -99.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -17.0F, -90.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -15.0F, -89.0F, -10.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -15.0F, -104.0F, -10.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_c = new RendererModel(this);
		door_dot_west_c.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_c);
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -18.0F, -75.0F, -10.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -17.0F, -76.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -19.0F, -74.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -6.0F, -74.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -5.0F, -72.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -20.0F, -72.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -17.0F, -63.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -15.0F, -62.0F, -10.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -15.0F, -77.0F, -10.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_d = new RendererModel(this);
		door_dot_west_d.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_d);
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -18.0F, -48.0F, -10.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -17.0F, -49.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -19.0F, -47.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -6.0F, -47.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -5.0F, -45.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -20.0F, -45.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -17.0F, -36.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -15.0F, -35.0F, -10.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -15.0F, -50.0F, -10.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_e = new RendererModel(this);
		door_dot_west_e.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_e);
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -18.0F, -21.0F, -10.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -17.0F, -22.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -19.0F, -20.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -6.0F, -20.0F, -10.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -5.0F, -18.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -20.0F, -18.0F, -10.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -17.0F, -9.0F, -10.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -15.0F, -8.0F, -10.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -15.0F, -23.0F, -10.0F, 6, 1, 8, 0.0F, false));

		door_east_rotate_y = new RendererModel(this);
		door_east_rotate_y.setRotationPoint(60.0F, -72.0F, -30.0F);
		door.addChild(door_east_rotate_y);
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 422, 249, -60.0F, 25.0F, 19.0F, 24, 44, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 419, 207, -60.0F, -19.0F, 19.0F, 24, 44, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 422, 163, -60.0F, -63.0F, 19.0F, 24, 44, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 56, 105, -58.0F, -61.0F, 23.0F, 20, 42, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 56, 105, -58.0F, 25.0F, 23.0F, 20, 42, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 56, 105, -58.0F, -19.0F, 23.0F, 20, 44, 4, 0.0F, false));

		door_dot_east_a = new RendererModel(this);
		door_dot_east_a.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_a);
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 5.9734F, -129.0F, -10.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 6.9734F, -130.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 4.9734F, -128.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 17.9734F, -128.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 18.9734F, -126.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 3.9734F, -126.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 6.9734F, -117.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 8.9734F, -116.0F, -10.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 8.9734F, -131.0F, -10.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_b = new RendererModel(this);
		door_dot_east_b.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_b);
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 5.9734F, -102.0F, -10.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 6.9734F, -103.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 4.9734F, -101.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 17.9734F, -101.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 18.9734F, -99.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 3.9734F, -99.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 6.9734F, -90.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 8.9734F, -89.0F, -10.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 8.9734F, -104.0F, -10.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_c = new RendererModel(this);
		door_dot_east_c.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_c);
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 5.9734F, -75.0F, -10.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 6.9734F, -76.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 4.9734F, -74.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 17.9734F, -74.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 18.9734F, -72.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 3.9734F, -72.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 6.9734F, -63.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 8.9734F, -62.0F, -10.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 8.9734F, -77.0F, -10.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_d = new RendererModel(this);
		door_dot_east_d.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_d);
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 5.9734F, -48.0F, -10.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 6.9734F, -49.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 4.9734F, -47.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 17.9734F, -47.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 18.9734F, -45.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 3.9734F, -45.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 6.9734F, -36.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 8.9734F, -35.0F, -10.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 8.9734F, -50.0F, -10.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_e = new RendererModel(this);
		door_dot_east_e.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_e);
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 5.9734F, -21.0F, -10.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 6.9734F, -22.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 4.9734F, -20.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 17.9734F, -20.0F, -10.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 18.9734F, -18.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 3.9734F, -18.0F, -10.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 6.9734F, -9.0F, -10.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 8.9734F, -8.0F, -10.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 8.9734F, -23.0F, -10.6101F, 6, 1, 8, 0.0F, false));

		door_jam = new RendererModel(this);
		door_jam.setRotationPoint(0.0F, 0.0F, 0.0F);
		door.addChild(door_jam);
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, 22.0F, -68.0F, -6.0F, 4, 64, 14, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, 22.0F, -132.0F, -6.0F, 4, 64, 14, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, -26.0F, -68.0F, -6.0F, 4, 64, 14, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, -26.0F, -132.0F, -6.0F, 4, 64, 14, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 34, 116, 0.0F, -140.0F, -6.0F, 26, 8, 14, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 34, 116, -26.0F, -140.0F, -6.0F, 26, 8, 14, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 34, 76, 0.0F, -4.0F, -8.0F, 26, 4, 16, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 39, 103, -26.0F, -4.0F, -8.0F, 26, 4, 16, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 0.0F, 0.0F);
		door.addChild(boti);
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -64.0F, -12.0F, 52, 64, 1, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, 26.0F, -68.0F, -12.0F, 1, 68, 8, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -27.0F, -68.0F, -12.0F, 1, 68, 8, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, 26.0F, -136.0F, -12.0F, 1, 68, 8, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -27.0F, -136.0F, -12.0F, 1, 68, 8, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -136.0F, -12.0F, 52, 60, 1, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -136.0F, -11.0F, 52, 1, 7, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -1.0F, -11.0F, 52, 1, 3, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -76.0F, -12.0F, 52, 12, 1, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 1.125, -0.4);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		float rot = (float)Math.toRadians(EnumDoorType.TT_CAPSULE.getRotationForState(door.getOpenState()));
		this.door_west_rotate_y.rotateAngleY = rot;
		this.door_east_rotate_y.rotateAngleY = -rot;
		
		this.door.render(0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		return TT2020CapsuleExteriorRenderer.TEXTURE;
	}
}