package net.tardis.mod.client.models.exteriors;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class TTCapsuleExteriorModel extends Model {
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel leftdoor;
	private final RendererModel rightdoor;

	public TTCapsuleExteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(7.5F, 23.0F, -3.5F);
		

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(-22.5F, 1.0F, -6.5F);
		bone2.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 24, 84, 11.0F, -3.0F, 1.0F, 8, 3, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 24, 84, 11.0F, -3.0F, 17.0F, 8, 3, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 94, 9.0F, -3.0F, 2.0F, 2, 3, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 94, 9.0F, -3.0F, 17.0F, 2, 3, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 94, 19.0F, -3.0F, 2.0F, 2, 3, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 94, 19.0F, -3.0F, 17.0F, 2, 3, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 8, 93, 7.0F, -3.0F, 4.0F, 1, 3, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 8, 93, 7.0F, -3.0F, 14.0F, 1, 3, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 8, 93, 22.0F, -3.0F, 4.0F, 1, 3, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 8, 93, 22.0F, -3.0F, 14.0F, 1, 3, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 78, 22.0F, -3.0F, 6.0F, 2, 3, 8, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 78, 6.0F, -3.0F, 6.0F, 2, 3, 8, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 7, 93, 8.0F, -3.0F, 3.0F, 14, 3, 14, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(-5.5F, -2.0F, -6.5F);
		bone2.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 18, 1, -10.5F, -31.0F, 6.5F, 1, 31, 7, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 8, 7, -7.5F, -31.0F, 16.5F, 2, 31, 1, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 8, 7, 1.5F, -31.0F, 16.5F, 2, 31, 1, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 1, 6, -9.5F, -31.0F, 4.5F, 1, 31, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 1, 6, -9.5F, -31.0F, 13.5F, 1, 31, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 55, 7, -8.5F, -31.0F, 15.5F, 1, 31, 1, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 55, 7, 3.5F, -31.0F, 15.5F, 1, 31, 1, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 1, 6, 4.5F, -31.0F, 4.5F, 1, 31, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 1, 6, 4.5F, -31.0F, 13.5F, 1, 31, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 18, 1, 5.5F, -31.0F, 6.5F, 1, 31, 7, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 35, 7, -5.5F, -31.0F, 17.5F, 7, 31, 1, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(-5.5F, -33.0F, -6.5F);
		bone2.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 24, 76, -11.0F, -4.0F, 6.0F, 2, 4, 8, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 7, 92, -9.0F, -4.0F, 3.0F, 14, 4, 14, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 82, -6.0F, -4.0F, 1.0F, 8, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 82, -6.0F, -4.0F, 17.0F, 8, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 92, -8.0F, -4.0F, 2.0F, 2, 4, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 92, -8.0F, -4.0F, 17.0F, 2, 4, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 92, 2.0F, -4.0F, 2.0F, 2, 4, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 92, 2.0F, -4.0F, 17.0F, 2, 4, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 8, 91, -10.0F, -4.0F, 4.0F, 1, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 91, -10.0F, -4.0F, 14.0F, 1, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 91, 5.0F, -4.0F, 4.0F, 1, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 91, 5.0F, -4.0F, 14.0F, 1, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 76, 5.0F, -4.0F, 6.0F, 2, 4, 8, 0.0F, false));

		leftdoor = new RendererModel(this);
		leftdoor.setRotationPoint(-7.0F, 5.5F, -4.0F);
		leftdoor.cubeList.add(new ModelBox(leftdoor, 0, 41, 3.0F, -15.5F, -3.0F, 4, 31, 1, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 23, 42, 1.0F, -15.5F, -2.0F, 2, 31, 1, 0.0F, false));
		leftdoor.cubeList.add(new ModelBox(leftdoor, 31, 42, 0.0F, -15.5F, -1.0F, 1, 31, 1, 0.0F, false));

		rightdoor = new RendererModel(this);
		rightdoor.setRotationPoint(7.0F, 5.5F, -4.0F);
		rightdoor.cubeList.add(new ModelBox(rightdoor, 23, 42, -3.0F, -15.5F, -2.0F, 2, 31, 1, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 31, 42, -1.0F, -15.5F, -1.0F, 1, 31, 1, 0.0F, false));
		rightdoor.cubeList.add(new ModelBox(rightdoor, 11, 41, -7.0F, -15.5F, -3.0F, 4, 31, 1, 0.0F, true));
	}

	public void render(ExteriorTile tile) {
		
		this.leftdoor.rotateAngleY = this.rightdoor.rotateAngleY = 0;
		float rot = -(float)Math.toRadians(EnumDoorType.TT_CAPSULE.getRotationForState(tile.getOpen()));
		
		if(tile.getOpen() == EnumDoorState.ONE) {
			this.rightdoor.rotateAngleY = -rot;
			this.leftdoor.rotateAngleY = 0;
		}
		else {
			this.leftdoor.rotateAngleY = rot;
			this.rightdoor.rotateAngleY = -rot;
		}
		
		bone2.render(0.0625F);
		leftdoor.render(0.0625F);
		rightdoor.render(0.0625F);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}