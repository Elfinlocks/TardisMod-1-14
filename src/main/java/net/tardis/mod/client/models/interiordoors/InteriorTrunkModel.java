package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.texturevariants.TextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;

public class InteriorTrunkModel extends Model implements IInteriorDoorRenderer{
	private final RendererModel boti;
	private final RendererModel lid_rotate_y;
	private final RendererModel walls2;
	private final RendererModel back_wall2;
	private final RendererModel sidewall_right2;
	private final RendererModel sidewall_left2;
	private final RendererModel top_wall2;
	private final RendererModel bottom_wall2;
	private final RendererModel trim2;
	private final RendererModel corners2;
	private final RendererModel knobs9;
	private final RendererModel knobs10;
	private final RendererModel knobs11;
	private final RendererModel knobs12;
	private final RendererModel knobs13;
	private final RendererModel knobs14;
	private final RendererModel knobs15;
	private final RendererModel knobs16;
	private final RendererModel mount2;
	private final RendererModel latch2;
	private final RendererModel sticker3;
	private final RendererModel sticker4;
	private final RendererModel frame;
	private final RendererModel trim3;
	private final RendererModel knobs2;
	private final RendererModel knobs3;
	private final RendererModel knobs4;
	private final RendererModel knobs5;

	public InteriorTrunkModel() {
		textureWidth = 256;
		textureHeight = 256;

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, -5.8125F);
		boti.cubeList.add(new ModelBox(boti, 4, 181, -15.0F, -64.0F, -0.25F, 30, 62, 2, 0.0F, false));

		lid_rotate_y = new RendererModel(this);
		lid_rotate_y.setRotationPoint(17.0F, 24.0F, -4.5F);

		walls2 = new RendererModel(this);
		walls2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(walls2);

		back_wall2 = new RendererModel(this);
		back_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(back_wall2);
		back_wall2.cubeList.add(new ModelBox(back_wall2, 123, 80, -16.0F, -65.0F, -15.0F, 32, 64, 1, 0.0F, false));
		back_wall2.cubeList.add(new ModelBox(back_wall2, 1, 2, -15.0F, -64.0F, -7.0F, 30, 62, 1, 0.0F, false));

		sidewall_right2 = new RendererModel(this);
		sidewall_right2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(sidewall_right2);
		sidewall_right2.cubeList.add(new ModelBox(sidewall_right2, 124, 78, 15.0F, -64.0F, -14.0F, 1, 62, 9, 0.0F, false));

		sidewall_left2 = new RendererModel(this);
		sidewall_left2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(sidewall_left2);
		sidewall_left2.cubeList.add(new ModelBox(sidewall_left2, 128, 76, -16.0F, -64.0F, -14.0F, 1, 62, 9, 0.0F, false));

		top_wall2 = new RendererModel(this);
		top_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(top_wall2);
		top_wall2.cubeList.add(new ModelBox(top_wall2, 120, 117, -16.0F, -65.0F, -14.0F, 32, 1, 9, 0.0F, false));

		bottom_wall2 = new RendererModel(this);
		bottom_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		walls2.addChild(bottom_wall2);
		bottom_wall2.cubeList.add(new ModelBox(bottom_wall2, 152, 117, -16.0F, -2.0F, -14.0F, 32, 1, 9, 0.0F, false));

		trim2 = new RendererModel(this);
		trim2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(trim2);
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -62.0F, -6.5F, 2, 58, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -62.0F, -6.5F, 2, 58, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -15.0F, -65.5F, -15.5F, 30, 2, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -15.0F, -2.5F, -15.5F, 30, 2, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -2.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -2.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -65.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -65.5F, -12.0F, 2, 2, 4, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, 14.5F, -64.0F, -15.5F, 2, 62, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -16.5F, -64.0F, -15.5F, 2, 62, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -13.0F, -2.5F, -6.5F, 26, 2, 2, 0.0F, false));
		trim2.cubeList.add(new ModelBox(trim2, 77, 3, -13.0F, -65.5F, -6.5F, 26, 2, 2, 0.0F, false));

		corners2 = new RendererModel(this);
		corners2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(corners2);

		knobs9 = new RendererModel(this);
		knobs9.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs9);
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -17.0F, -4.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -16.0F, -3.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -17.5F, -3.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs9.cubeList.add(new ModelBox(knobs9, 74, 71, -16.0F, -1.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs10 = new RendererModel(this);
		knobs10.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs10);
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 13.0F, -4.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 14.0F, -3.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 15.5F, -3.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs10.cubeList.add(new ModelBox(knobs10, 74, 71, 14.0F, -1.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs11 = new RendererModel(this);
		knobs11.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs11);
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 13.0F, -66.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 14.0F, -65.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 15.5F, -65.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs11.cubeList.add(new ModelBox(knobs11, 74, 71, 14.0F, -66.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs12 = new RendererModel(this);
		knobs12.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs12);
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -17.0F, -66.0F, -16.0F, 4, 4, 4, 0.0F, false));
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -16.0F, -65.0F, -16.5F, 2, 2, 2, 0.0F, false));
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -17.5F, -65.0F, -15.0F, 2, 2, 2, 0.0F, false));
		knobs12.cubeList.add(new ModelBox(knobs12, 74, 71, -16.0F, -66.5F, -15.0F, 2, 2, 2, 0.0F, false));

		knobs13 = new RendererModel(this);
		knobs13.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs13);
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -17.0F, -66.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -17.0F, -66.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -17.5F, -65.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -17.5F, -65.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -16.0F, -66.5F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs13.cubeList.add(new ModelBox(knobs13, 74, 71, -16.0F, -66.5F, -7.5F, 2, 2, 2, 0.0F, false));

		knobs14 = new RendererModel(this);
		knobs14.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs14);
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 13.0F, -66.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 13.0F, -66.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 15.5F, -65.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 15.5F, -65.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 14.0F, -66.5F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs14.cubeList.add(new ModelBox(knobs14, 74, 71, 14.0F, -66.5F, -7.5F, 2, 2, 2, 0.0F, false));

		knobs15 = new RendererModel(this);
		knobs15.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs15);
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 13.0F, -4.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 13.0F, -4.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 15.5F, -3.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 15.5F, -3.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 14.0F, -1.5F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs15.cubeList.add(new ModelBox(knobs15, 74, 71, 14.0F, -1.5F, -7.5F, 2, 2, 2, 0.0F, false));

		knobs16 = new RendererModel(this);
		knobs16.setRotationPoint(0.0F, 0.0F, 0.0F);
		corners2.addChild(knobs16);
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -17.0F, -4.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -17.0F, -4.0F, -8.5F, 4, 4, 4, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -17.5F, -3.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -17.5F, -3.0F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -16.0F, -1.5F, -7.5F, 2, 2, 2, 0.0F, false));
		knobs16.cubeList.add(new ModelBox(knobs16, 74, 71, -16.0F, -1.5F, -7.5F, 2, 2, 2, 0.0F, false));

		mount2 = new RendererModel(this);
		mount2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(mount2);
		mount2.cubeList.add(new ModelBox(mount2, 61, 73, 16.0F, -12.0F, -9.0F, 1, 2, 4, 0.0F, false));
		mount2.cubeList.add(new ModelBox(mount2, 61, 73, 16.0F, -36.0F, -9.0F, 1, 2, 4, 0.0F, false));
		mount2.cubeList.add(new ModelBox(mount2, 61, 73, 16.0F, -56.0F, -9.0F, 1, 2, 4, 0.0F, false));

		latch2 = new RendererModel(this);
		latch2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		lid_rotate_y.addChild(latch2);
		latch2.cubeList.add(new ModelBox(latch2, 51, 76, -17.0F, -36.0F, -9.0F, 1, 4, 6, 0.0F, false));

		sticker3 = new RendererModel(this);
		sticker3.setRotationPoint(-20.0F, -45.0F, -11.0F);
		setRotationAngle(sticker3, 0.0F, 0.0F, -0.6109F);
		lid_rotate_y.addChild(sticker3);
		sticker3.cubeList.add(new ModelBox(sticker3, 3, 158, -7.0F, -3.0F, 0.4F, 14, 6, 1, 0.0F, false));

		sticker4 = new RendererModel(this);
		sticker4.setRotationPoint(0.0F, 0.0F, -0.5F);
		setRotationAngle(sticker4, 0.0F, 0.0F, 0.7854F);
		sticker3.addChild(sticker4);
		sticker4.cubeList.add(new ModelBox(sticker4, 10, 154, -4.0F, -4.0F, 0.8F, 8, 8, 1, 0.0F, false));

		frame = new RendererModel(this);
		frame.setRotationPoint(17.0F, 24.0F, -1.9688F);

		trim3 = new RendererModel(this);
		trim3.setRotationPoint(-17.0F, 0.0F, 2.5F);
		frame.addChild(trim3);
		trim3.cubeList.add(new ModelBox(trim3, 77, 3, -16.5F, -62.0F, -5.0F, 2, 58, 1, 0.0F, false));
		trim3.cubeList.add(new ModelBox(trim3, 77, 3, 14.5F, -62.0F, -5.0F, 2, 58, 1, 0.0F, false));
		trim3.cubeList.add(new ModelBox(trim3, 77, 3, -13.0F, -2.5F, -5.0F, 26, 2, 1, 0.0F, false));
		trim3.cubeList.add(new ModelBox(trim3, 77, 3, -13.0F, -65.5F, -5.0F, 26, 2, 1, 0.0F, false));

		knobs2 = new RendererModel(this);
		knobs2.setRotationPoint(-17.0F, 0.0F, 4.5F);
		frame.addChild(knobs2);
		knobs2.cubeList.add(new ModelBox(knobs2, 74, 71, -17.0F, -4.0F, -7.0F, 4, 4, 2, 0.0F, false));

		knobs3 = new RendererModel(this);
		knobs3.setRotationPoint(-17.0F, 0.0F, 4.5F);
		frame.addChild(knobs3);
		knobs3.cubeList.add(new ModelBox(knobs3, 74, 71, 13.0F, -4.0F, -7.0F, 4, 4, 2, 0.0F, false));

		knobs4 = new RendererModel(this);
		knobs4.setRotationPoint(-17.0F, 0.0F, 4.5F);
		frame.addChild(knobs4);
		knobs4.cubeList.add(new ModelBox(knobs4, 74, 71, 13.0F, -66.0F, -7.0F, 4, 4, 2, 0.0F, false));

		knobs5 = new RendererModel(this);
		knobs5.setRotationPoint(-17.0F, 0.0F, 4.5F);
		frame.addChild(knobs5);
		knobs5.cubeList.add(new ModelBox(knobs5, 74, 71, -17.0F, -66.0F, -7.0F, 4, 4, 2, 0.0F, false));
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 0.7, -0.3);
		GlStateManager.scaled(0.5, 0.5, 0.5);
		this.lid_rotate_y.rotateAngleY = (float) Math.toRadians(IDoorType.EnumDoorType.TRUNK.getRotationForState(door.getOpenState()));
		
		GlStateManager.depthMask(false);
		this.boti.render(0.0625F);
		GlStateManager.depthMask(true);
		
		lid_rotate_y.render(0.0625F);
		frame.render(0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		TileEntity te = Minecraft.getInstance().world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile) {
			int i = ((ConsoleTile)te).getExteriorManager().getExteriorVariant();
			if(i < TextureVariants.TRUNK.length)
				return TextureVariants.TRUNK[i].getTexture();
		}
		return TrunkExteriorRenderer.TEXTURE;
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}