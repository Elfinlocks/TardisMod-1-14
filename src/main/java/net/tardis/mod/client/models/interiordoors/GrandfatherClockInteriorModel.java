package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.ClockExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class GrandfatherClockInteriorModel extends Model implements IInteriorDoorRenderer{
	private final RendererModel boti;
	private final RendererModel door_rotate_y;
	private final RendererModel pendulum_rotate_z;
	private final RendererModel disk;
	private final RendererModel pendulum_backwall;
	private final RendererModel pully;
	private final RendererModel pully2;
	private final RendererModel door_frame;
	private final RendererModel door_latch;
	private final RendererModel frame;
	private final RendererModel top;
	private final RendererModel posts;
	private final RendererModel foot1;
	private final RendererModel foot2;
	private final RendererModel foot3;
	private final RendererModel foot4;
	private final RendererModel torso;
	private final RendererModel side_details;
	private final RendererModel side_details2;
	private final RendererModel back_details;
	private final RendererModel base;
	private final RendererModel clockworks;
	private final RendererModel small_cog_1;
	private final RendererModel small_cog_2;
	private final RendererModel large_cog_1;
	private final RendererModel med_cog_1;
	private final RendererModel center_gear;
	private final RendererModel bone;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel bone7;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel center_frame;
	private final RendererModel bone10;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel bone13;
	private final RendererModel hitbox;

	public GrandfatherClockInteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, -13.0F);
		boti.cubeList.add(new ModelBox(boti, 408, 195, -21.0F, -124.0F, 4.0F, 42, 101, 4, 0.0F, false));

		door_rotate_y = new RendererModel(this);
		door_rotate_y.setRotationPoint(20.0F, -4.0F, -5.0F);
		

		pendulum_rotate_z = new RendererModel(this);
		pendulum_rotate_z.setRotationPoint(-20.0F, -88.0F, 5.0F);
		door_rotate_y.addChild(pendulum_rotate_z);
		pendulum_rotate_z.cubeList.add(new ModelBox(pendulum_rotate_z, 194, 185, -2.0F, 7.0F, 0.0F, 4, 56, 1, 0.0F, false));

		disk = new RendererModel(this);
		disk.setRotationPoint(0.0F, 69.0F, 0.0F);
		pendulum_rotate_z.addChild(disk);
		setRotationAngle(disk, 0.0F, 0.0F, -0.7854F);
		disk.cubeList.add(new ModelBox(disk, 11, 184, -6.0F, -6.0F, -1.0F, 12, 12, 2, 0.0F, false));

		pendulum_backwall = new RendererModel(this);
		pendulum_backwall.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(pendulum_backwall);
		pendulum_backwall.cubeList.add(new ModelBox(pendulum_backwall, 75, 190, -16.0F, -109.0F, 2.0F, 32, 84, 0, 0.0F, false));

		pully = new RendererModel(this);
		pully.setRotationPoint(-4.0F, 0.0F, 5.0F);
		door_rotate_y.addChild(pully);
		pully.cubeList.add(new ModelBox(pully, 198, 195, -24.0F, -81.0F, 0.0F, 2, 12, 1, 0.0F, false));
		pully.cubeList.add(new ModelBox(pully, 19, 192, -25.0F, -69.0F, -1.0F, 4, 12, 2, 0.0F, false));

		pully2 = new RendererModel(this);
		pully2.setRotationPoint(-4.0F, 0.0F, 5.0F);
		door_rotate_y.addChild(pully2);
		pully2.cubeList.add(new ModelBox(pully2, 198, 196, -10.0F, -81.0F, 0.0F, 2, 24, 1, 0.0F, false));

		door_frame = new RendererModel(this);
		door_frame.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(door_frame);
		door_frame.cubeList.add(new ModelBox(door_frame, 149, 90, -16.0F, -105.0F, -4.0F, 4, 72, 3, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, 8.0F, -161.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -161.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -20.0F, -157.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, 12.0F, -157.0F, -5.0F, 8, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -165.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -121.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -113.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 107, 164, -16.0F, -29.0F, -5.0F, 32, 4, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 101, 36, -20.0F, -153.0F, -5.0F, 4, 128, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 120, 36, 16.0F, -153.0F, -5.0F, 4, 128, 8, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 89, 77, -16.0F, -117.0F, -4.0F, 32, 4, 0, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 143, 49, -16.0F, -117.0F, 1.0F, 32, 4, 1, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 121, 103, -16.0F, -109.0F, -4.0F, 32, 4, 3, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 137, 84, 12.0F, -105.0F, -4.0F, 4, 72, 3, 0.0F, false));
		door_frame.cubeList.add(new ModelBox(door_frame, 111, 164, -16.0F, -33.0F, -4.0F, 32, 4, 3, 0.0F, false));

		door_latch = new RendererModel(this);
		door_latch.setRotationPoint(-20.0F, 28.0F, 5.0F);
		door_rotate_y.addChild(door_latch);
		door_latch.cubeList.add(new ModelBox(door_latch, 11, 183, -19.0F, -97.0F, -7.0F, 2, 8, 12, 0.0F, false));

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		top = new RendererModel(this);
		top.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(top);
		top.cubeList.add(new ModelBox(top, 80, 71, 16.0F, -165.0F, -8.0F, 12, 8, 15, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 33, 60, -28.0F, -165.0F, -8.0F, 12, 8, 15, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 34, 49, -28.0F, -169.0F, -9.0F, 56, 4, 16, 0.0F, false));

		posts = new RendererModel(this);
		posts.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(posts);
		

		foot1 = new RendererModel(this);
		foot1.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot1);
		

		foot2 = new RendererModel(this);
		foot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot2);
		

		foot3 = new RendererModel(this);
		foot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot3);
		

		foot4 = new RendererModel(this);
		foot4.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(foot4);
		

		torso = new RendererModel(this);
		torso.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(torso);
		torso.cubeList.add(new ModelBox(torso, 90, 15, -20.0F, -165.0F, 1.0F, 40, 47, 1, 0.0F, false));
		torso.cubeList.add(new ModelBox(torso, 96, 14, -24.0F, -157.0F, -4.0F, 4, 136, 10, 0.0F, false));
		torso.cubeList.add(new ModelBox(torso, 80, 10, 20.0F, -157.0F, -4.0F, 4, 136, 10, 0.0F, false));

		side_details = new RendererModel(this);
		side_details.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(side_details);
		

		side_details2 = new RendererModel(this);
		side_details2.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(side_details2);
		

		back_details = new RendererModel(this);
		back_details.setRotationPoint(0.0F, 0.0F, 0.0F);
		torso.addChild(back_details);
		

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(base);
		base.cubeList.add(new ModelBox(base, 31, 129, -25.0F, -25.0F, -7.0F, 50, 17, 13, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 67, 228, -22.0F, -22.0F, 5.25F, 44, 11, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 31, 129, -20.0F, -120.0F, -1.0F, 40, 2, 7, 0.0F, false));

		clockworks = new RendererModel(this);
		clockworks.setRotationPoint(0.0F, -142.0F, 0.0F);
		frame.addChild(clockworks);
		

		small_cog_1 = new RendererModel(this);
		small_cog_1.setRotationPoint(-7.0156F, 6.0F, 4.625F);
		clockworks.addChild(small_cog_1);
		small_cog_1.cubeList.add(new ModelBox(small_cog_1, 11, 183, -0.9844F, -1.0F, -1.625F, 2, 2, 3, 0.0F, false));
		small_cog_1.cubeList.add(new ModelBox(small_cog_1, 188, 209, -2.0156F, -2.0F, -0.375F, 4, 4, 1, 0.0F, false));

		small_cog_2 = new RendererModel(this);
		small_cog_2.setRotationPoint(8.9844F, 2.5F, 3.625F);
		clockworks.addChild(small_cog_2);
		small_cog_2.cubeList.add(new ModelBox(small_cog_2, 11, 183, -0.9844F, -1.0F, -1.625F, 2, 2, 3, 0.0F, false));
		small_cog_2.cubeList.add(new ModelBox(small_cog_2, 188, 209, -2.0156F, -2.0F, -0.375F, 4, 4, 1, 0.0F, false));

		large_cog_1 = new RendererModel(this);
		large_cog_1.setRotationPoint(-5.8869F, -6.4203F, 6.125F);
		clockworks.addChild(large_cog_1);
		setRotationAngle(large_cog_1, 0.0F, 0.0F, 0.7854F);
		large_cog_1.cubeList.add(new ModelBox(large_cog_1, 11, 183, -0.9844F, -1.0F, -1.125F, 2, 2, 3, 0.0F, false));
		large_cog_1.cubeList.add(new ModelBox(large_cog_1, 188, 209, -4.0156F, -4.0F, -0.875F, 8, 8, 1, 0.0F, false));

		med_cog_1 = new RendererModel(this);
		med_cog_1.setRotationPoint(5.9061F, 6.2922F, 5.125F);
		clockworks.addChild(med_cog_1);
		setRotationAngle(med_cog_1, 0.0F, 0.0F, 1.9199F);
		med_cog_1.cubeList.add(new ModelBox(med_cog_1, 11, 183, -0.9844F, -1.0F, -1.125F, 2, 2, 3, 0.0F, false));
		med_cog_1.cubeList.add(new ModelBox(med_cog_1, 188, 209, -3.0156F, -3.0F, -0.875F, 6, 6, 1, 0.0F, false));

		center_gear = new RendererModel(this);
		center_gear.setRotationPoint(0.0F, 106.5F, 0.625F);
		clockworks.addChild(center_gear);
		center_gear.cubeList.add(new ModelBox(center_gear, 11, 183, -1.0F, -107.5F, -0.625F, 2, 2, 5, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 35.5F, -0.625F);
		center_gear.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 188, 209, -4.0F, -136.0F, 2.0F, 8, 2, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 188, 209, -4.0F, -150.0F, 2.0F, 8, 2, 1, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 188, 209, -1.0938F, -150.75F, 2.5F, 2, 3, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 188, 209, -2.0313F, -144.0F, 3.25F, 4, 4, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 188, 209, -1.0938F, -141.25F, 2.5F, 2, 8, 1, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(-0.0938F, -142.0F, 3.0F);
		bone.addChild(bone3);
		setRotationAngle(bone3, 0.0F, 0.0F, 0.5236F);
		bone3.cubeList.add(new ModelBox(bone3, 188, 209, -1.0F, -9.25F, -0.5F, 2, 3, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 188, 209, -1.0F, 6.25F, -0.5F, 2, 3, 1, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, -106.5F, 0.625F);
		center_gear.addChild(bone4);
		setRotationAngle(bone4, 0.0F, 0.0F, 1.0472F);
		bone4.cubeList.add(new ModelBox(bone4, 188, 209, -4.0F, 6.0F, 0.75F, 8, 2, 1, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 188, 209, -4.0F, -8.0F, 0.75F, 8, 2, 1, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 142.0F, -1.25F);
		bone4.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 188, 209, -1.0938F, -150.75F, 2.5F, 2, 9, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 188, 209, -1.0938F, -136.25F, 2.5F, 2, 3, 1, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(-0.0938F, 0.0F, 1.75F);
		bone4.addChild(bone6);
		setRotationAngle(bone6, 0.0F, 0.0F, 0.5236F);
		bone6.cubeList.add(new ModelBox(bone6, 188, 209, -1.0F, -9.25F, -0.5F, 2, 3, 1, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 188, 209, -1.0F, 6.25F, -0.5F, 2, 3, 1, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, -106.5F, 0.625F);
		center_gear.addChild(bone7);
		setRotationAngle(bone7, 0.0F, 0.0F, 2.0944F);
		bone7.cubeList.add(new ModelBox(bone7, 188, 209, -4.0F, 6.0F, 0.75F, 8, 2, 1, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 188, 209, -4.0F, -8.0F, 0.75F, 8, 2, 1, 0.0F, false));

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 142.0F, -1.25F);
		bone7.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 188, 209, -1.0938F, -150.75F, 2.5F, 2, 3, 1, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 188, 209, -1.0938F, -142.25F, 2.5F, 2, 9, 1, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(-0.0938F, 0.0F, 1.75F);
		bone7.addChild(bone9);
		setRotationAngle(bone9, 0.0F, 0.0F, 0.5236F);
		bone9.cubeList.add(new ModelBox(bone9, 188, 209, -1.0F, -9.25F, -0.5F, 2, 3, 1, 0.0F, false));
		bone9.cubeList.add(new ModelBox(bone9, 188, 209, -1.0F, 6.25F, -0.5F, 2, 3, 1, 0.0F, false));

		center_frame = new RendererModel(this);
		center_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		clockworks.addChild(center_frame);
		

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 142.0F, 0.0F);
		center_frame.addChild(bone10);
		bone10.cubeList.add(new ModelBox(bone10, 11, 183, -1.0F, -143.0F, 0.0F, 2, 2, 5, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, -2.0313F, -144.0F, 3.25F, 4, 4, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, -1.0938F, -141.0F, 2.5F, 2, 11, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, -1.0938F, -154.0F, 2.5F, 2, 11, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, -7.0F, -130.25F, 2.0F, 14, 1, 3, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, -8.0F, -130.75F, 2.5F, 2, 2, 3, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, 6.0F, -155.0F, 2.5F, 2, 2, 3, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 188, 209, -7.0F, -154.5F, 2.0F, 14, 1, 3, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(-0.0313F, -141.9286F, 3.5357F);
		bone10.addChild(bone12);
		setRotationAngle(bone12, 0.0F, 0.0F, 1.0472F);
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, -2.0F, -2.0714F, -0.5357F, 4, 4, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, -1.0625F, 0.9286F, -1.0357F, 2, 11, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, -1.0625F, -12.0714F, -1.0357F, 2, 11, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, -6.9688F, 11.6786F, -1.5357F, 14, 1, 3, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, -7.9688F, 11.1786F, -1.0357F, 2, 2, 3, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, 6.0313F, -13.0714F, -1.0357F, 2, 2, 3, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 188, 209, -6.9688F, -12.5714F, -1.5357F, 14, 1, 3, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(-0.0313F, -141.9286F, 3.5357F);
		bone10.addChild(bone13);
		setRotationAngle(bone13, 0.0F, 0.0F, 2.0944F);
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, -2.0F, -2.0714F, -0.0357F, 4, 4, 1, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, -1.0625F, 0.9286F, -1.0357F, 2, 11, 1, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, -1.0625F, -12.0714F, -1.0357F, 2, 11, 1, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, -6.9688F, 11.6786F, -1.5357F, 14, 1, 3, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, -7.9688F, 11.1786F, -1.0357F, 2, 2, 3, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, 6.0313F, -13.0714F, -1.0357F, 2, 2, 3, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 188, 209, -6.9688F, -12.5714F, -1.5357F, 14, 1, 3, 0.0F, false));

		hitbox = new RendererModel(this);
		hitbox.setRotationPoint(0.0F, 24.0F, 0.0F);
		hitbox.cubeList.add(new ModelBox(hitbox, 0, 0, -32.0F, -28.0F, -12.0F, 64, 28, 40, 0.0F, false));
		hitbox.cubeList.add(new ModelBox(hitbox, 0, 0, -28.0F, -168.0F, -8.0F, 56, 140, 32, 0.0F, false));
		hitbox.cubeList.add(new ModelBox(hitbox, 0, 0, -34.0F, -176.0F, -12.0F, 68, 12, 40, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 1.25, -0.5);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		this.door_rotate_y.rotateAngleY = (float) Math.toRadians(EnumDoorType.CLOCK.getRotationForState(door.getOpenState()));
		
		boti.render(0.0625F);
		door_rotate_y.render(0.0625F);
		frame.render(0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		return ClockExteriorRenderer.TEXTURE;
	}
}