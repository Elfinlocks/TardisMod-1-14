package net.tardis.mod.client.models;

import java.util.function.Supplier;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.misc.WorldText;

public class TextLabelRendererModel extends RendererModel {

	public WorldText textRenderer;
	Supplier<String[]> label;
	Supplier<ResourceLocation> texture;
	
	public TextLabelRendererModel(Model model, WorldText renderer, Supplier<String[]> text, Supplier<ResourceLocation> texture) {
		super(model);
		this.textRenderer = renderer;
		this.label = text;
		this.texture = texture;
	}
	

	@Override
	public void render(float scale) {
		GlStateManager.pushMatrix();
        GlStateManager.translatef(this.rotationPointX * scale, this.rotationPointY * scale, this.rotationPointZ * scale);
        if (this.rotateAngleZ != 0.0F) {
           GlStateManager.rotatef(this.rotateAngleZ * (180F / (float)Math.PI), 0.0F, 0.0F, 1.0F);
        }

        if (this.rotateAngleY != 0.0F) {
           GlStateManager.rotatef(this.rotateAngleY * (180F / (float)Math.PI), 0.0F, 1.0F, 0.0F);
        }

        if (this.rotateAngleX != 0.0F) {
           GlStateManager.rotatef(this.rotateAngleX * (180F / (float)Math.PI), 1.0F, 0.0F, 0.0F);
        }
        
        this.textRenderer.renderMonitor(label.get());
        GlStateManager.popMatrix();
        
        Minecraft.getInstance().getTextureManager().bindTexture(this.texture.get());
	}

	@Override
	public void renderWithRotation(float scale) {
		super.renderWithRotation(scale);
	}

}
