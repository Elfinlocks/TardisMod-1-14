package net.tardis.mod.client.models;

import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public interface IBotiModel {
	
	void renderBoti(float scale, boolean raw);
	default void renderBoti(float scale) {
		this.renderBoti(0.0625F, false);
	}
	void renderOverwrite(BotiContext context);

    interface IRender {
		void render();
	}

    class BotiContext {
		
	}

    class BotiContextExterior extends BotiContext {
		
		ExteriorTile console;
		
		public BotiContextExterior(ExteriorTile console) {
			this.console = console;
		}
		
		public void setConsole(ExteriorTile console) {
			this.console = console;
		}
		
		public ExteriorTile getConsole() {
			return console;
		}
	}
}
