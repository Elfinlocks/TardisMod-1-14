package net.tardis.mod.client.models.exteriors;// Made with Blockbench 3.6.6
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.client.models.IExteriorModel;
import net.tardis.mod.client.models.LightRendererModel;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ModernPoliceBoxExteriorModel extends ExteriorModel implements IExteriorModel {
	private final RendererModel base;
	private final RendererModel doors;
	private final RendererModel left2;
	private final RendererModel window3;
	private final RendererModel frame3;
	private final RendererModel glow2;
	private final RendererModel right2;
	private final RendererModel window4;
	private final RendererModel frame4;
	private final RendererModel glow5;
	private final RendererModel side;
	private final RendererModel left;
	private final RendererModel window;
	private final RendererModel frame;
	private final RendererModel glow3;
	private final RendererModel right;
	private final RendererModel window2;
	private final RendererModel frame2;
	private final RendererModel glow4;
	private final RendererModel side2;
	private final RendererModel left3;
	private final RendererModel window5;
	private final RendererModel frame5;
	private final RendererModel glow6;
	private final RendererModel right3;
	private final RendererModel window6;
	private final RendererModel frame6;
	private final RendererModel glow7;
	private final RendererModel side3;
	private final RendererModel left4;
	private final RendererModel window7;
	private final RendererModel frame7;
	private final RendererModel glow8;
	private final RendererModel right4;
	private final RendererModel window8;
	private final RendererModel frame8;
	private final RendererModel glow10;
	private final RendererModel roof;
	private final RendererModel lamp;
	private final RendererModel glow9;
	private final RendererModel boti;
	private final RendererModel backing;

	private static float LIGHT = 0F;
	private static float ALPHA = 1F;

	public ModernPoliceBoxExteriorModel() {
		textureWidth = 151;
		textureHeight = 151;

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 0, 0, -11.0F, -2.0F, -11.0F, 22, 2, 22, 0.0F, false));

		doors = new RendererModel(this);
		doors.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(doors, 0.0F, 1.5708F, 0.0F);
		doors.cubeList.add(new ModelBox(doors, 76, 64, 8.5F, -32.5F, -8.0F, 1, 1, 16, 0.0F, false));
		doors.cubeList.add(new ModelBox(doors, 66, 0, 7.75F, -33.0F, -8.0F, 2, 1, 16, 0.0F, false));
		doors.cubeList.add(new ModelBox(doors, 0, 66, 8.75F, -35.5F, -9.0F, 2, 3, 18, 0.0F, false));
		doors.cubeList.add(new ModelBox(doors, 66, 85, 8.0F, -36.0F, 8.0F, 2, 35, 2, 0.0F, false));
		doors.cubeList.add(new ModelBox(doors, 22, 54, 11.0F, -35.0F, -7.0F, 0, 2, 14, 0.0F, false));

		left2 = new RendererModel(this);
		left2.setRotationPoint(8.0F, -1.0F, 8.0F);
		doors.addChild(left2);
		left2.cubeList.add(new ModelBox(left2, 92, 6, 0.0F, -2.0F, -6.75F, 1, 2, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 86, 86, 0.0F, -31.0F, -1.0F, 1, 31, 1, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 16, 87, 0.0F, -31.0F, -7.5F, 1, 31, 1, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 91, 25, 0.0F, -31.0F, -6.75F, 1, 2, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 99, 21, 0.0F, -9.0F, -6.75F, 1, 1, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 99, 21, 0.0F, -16.0F, -6.75F, 1, 1, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 98, 92, 0.0F, -23.0F, -6.75F, 1, 1, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 36, 88, 0.75F, -8.0F, -6.75F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 24, 88, 0.25F, -8.0F, -6.75F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 36, 88, 0.75F, -15.0F, -6.75F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 24, 88, 0.25F, -15.0F, -6.75F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 36, 88, 0.75F, -22.0F, -6.75F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 24, 88, 0.25F, -22.0F, -6.75F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 64, 59, 0.5F, -17.0F, -7.0F, 1, 3, 0, 0.0F, false));

		window3 = new RendererModel(this);
		window3.setRotationPoint(-8.25F, 1.0F, -7.75F);
		left2.addChild(window3);
		

		frame3 = new RendererModel(this);
		frame3.setRotationPoint(0.0F, 0.0F, 0.0F);
		window3.addChild(frame3);
		frame3.cubeList.add(new ModelBox(frame3, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));
		frame3.cubeList.add(new ModelBox(frame3, 0, 66, 8.5F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow2 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow2.setRotationPoint(0.0F, 0.0F, 0.0F);
		window3.addChild(glow2);
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 8.375F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 8.375F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		right2 = new RendererModel(this);
		right2.setRotationPoint(8.0F, -1.0F, -8.0F);
		doors.addChild(right2);
		right2.cubeList.add(new ModelBox(right2, 92, 6, 0.0F, -2.0F, 0.75F, 1, 2, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 20, 87, 0.0F, -31.0F, 6.5F, 1, 31, 1, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 16, 87, 0.0F, -31.0F, 0.0F, 1, 31, 1, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 91, 25, 0.0F, -31.0F, 0.75F, 1, 2, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 99, 21, 0.0F, -9.0F, 0.75F, 1, 1, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 99, 21, 0.0F, -16.0F, 0.75F, 1, 1, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 98, 92, 0.0F, -23.0F, 0.75F, 1, 1, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 36, 82, 0.75F, -8.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 24, 82, 0.25F, -8.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 36, 82, 0.75F, -15.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 24, 82, 0.25F, -15.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 24, 82, 0.25F, -22.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 0, 0, 0.75F, -22.5F, 0.25F, 0, 7, 7, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 0, 0, 1.0F, -21.0F, 2.25F, 0, 3, 3, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 14, 14, 1.0F, -20.0F, 2.75F, 0, 3, 2, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 68, 14, 0.25F, -20.0F, 1.25F, 1, 2, 0, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 12, 87, 0.25F, -31.0F, 7.5F, 1, 31, 1, 0.0F, false));

		window4 = new RendererModel(this);
		window4.setRotationPoint(-8.25F, 1.0F, -0.25F);
		right2.addChild(window4);
		

		frame4 = new RendererModel(this);
		frame4.setRotationPoint(0.0F, 0.0F, 0.0F);
		window4.addChild(frame4);
		frame4.cubeList.add(new ModelBox(frame4, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));
		frame4.cubeList.add(new ModelBox(frame4, 0, 66, 8.5F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow5 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow5.setRotationPoint(0.0F, 0.0F, 0.0F);
		window4.addChild(glow5);
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 8.375F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 8.375F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		side = new RendererModel(this);
		side.setRotationPoint(0.0F, 23.0F, 0.0F);
		side.cubeList.add(new ModelBox(side, 66, 85, 8.0F, -36.0F, 8.0F, 2, 35, 2, 0.0F, false));
		side.cubeList.add(new ModelBox(side, 12, 87, 8.25F, -32.0F, -0.5F, 1, 31, 1, 0.0F, false));
		side.cubeList.add(new ModelBox(side, 76, 64, 8.5F, -32.5F, -8.0F, 1, 1, 16, 0.0F, false));
		side.cubeList.add(new ModelBox(side, 66, 0, 7.75F, -33.0F, -8.0F, 2, 1, 16, 0.0F, false));
		side.cubeList.add(new ModelBox(side, 0, 66, 8.75F, -35.5F, -9.0F, 2, 3, 18, 0.0F, false));
		side.cubeList.add(new ModelBox(side, 22, 52, 11.0F, -35.0F, -7.0F, 0, 2, 14, 0.0F, false));

		left = new RendererModel(this);
		left.setRotationPoint(0.0F, 0.0F, 0.25F);
		side.addChild(left);
		left.cubeList.add(new ModelBox(left, 92, 6, 8.0F, -3.0F, 1.0F, 1, 2, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 86, 86, 8.0F, -32.0F, 6.75F, 1, 31, 1, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 16, 87, 8.0F, -32.0F, 0.25F, 1, 31, 1, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 91, 25, 8.0F, -32.0F, 1.0F, 1, 2, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 99, 21, 8.0F, -10.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 99, 21, 8.0F, -17.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 98, 92, 8.0F, -24.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 36, 88, 8.75F, -9.0F, 1.0F, 0, 6, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 36, 88, 8.75F, -16.0F, 1.0F, 0, 6, 6, 0.0F, false));
		left.cubeList.add(new ModelBox(left, 36, 88, 8.75F, -23.0F, 1.0F, 0, 6, 6, 0.0F, false));

		window = new RendererModel(this);
		window.setRotationPoint(-0.25F, 0.0F, 0.0F);
		left.addChild(window);
		

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		window.addChild(frame);
		frame.cubeList.add(new ModelBox(frame, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow3 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow3.setRotationPoint(0.0F, 0.0F, 0.0F);
		window.addChild(glow3);
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow3.cubeList.add(new ModelBox(glow3, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		right = new RendererModel(this);
		right.setRotationPoint(0.0F, 0.0F, -8.25F);
		side.addChild(right);
		right.cubeList.add(new ModelBox(right, 92, 6, 8.0F, -3.0F, 1.0F, 1, 2, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 86, 86, 8.0F, -32.0F, 6.75F, 1, 31, 1, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 16, 87, 8.0F, -32.0F, 0.25F, 1, 31, 1, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 91, 25, 8.0F, -32.0F, 1.0F, 1, 2, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 99, 21, 8.0F, -10.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 99, 21, 8.0F, -17.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 98, 92, 8.0F, -24.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 36, 82, 8.75F, -9.0F, 1.0F, 0, 6, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 36, 82, 8.75F, -16.0F, 1.0F, 0, 6, 6, 0.0F, false));
		right.cubeList.add(new ModelBox(right, 36, 82, 8.75F, -23.0F, 1.0F, 0, 6, 6, 0.0F, false));

		window2 = new RendererModel(this);
		window2.setRotationPoint(-0.25F, 0.0F, 0.0F);
		right.addChild(window2);
		

		frame2 = new RendererModel(this);
		frame2.setRotationPoint(0.0F, 0.0F, 0.0F);
		window2.addChild(frame2);
		frame2.cubeList.add(new ModelBox(frame2, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow4 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow4.setRotationPoint(0.0F, 0.0F, 0.0F);
		window2.addChild(glow4);
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow4.cubeList.add(new ModelBox(glow4, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		side2 = new RendererModel(this);
		side2.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(side2, 0.0F, -1.5708F, 0.0F);
		side2.cubeList.add(new ModelBox(side2, 66, 85, 8.0F, -36.0F, 8.0F, 2, 35, 2, 0.0F, false));
		side2.cubeList.add(new ModelBox(side2, 12, 87, 8.25F, -32.0F, -0.5F, 1, 31, 1, 0.0F, false));
		side2.cubeList.add(new ModelBox(side2, 76, 64, 8.5F, -32.5F, -8.0F, 1, 1, 16, 0.0F, false));
		side2.cubeList.add(new ModelBox(side2, 66, 0, 7.75F, -33.0F, -8.0F, 2, 1, 16, 0.0F, false));
		side2.cubeList.add(new ModelBox(side2, 0, 66, 8.75F, -35.5F, -9.0F, 2, 3, 18, 0.0F, false));
		side2.cubeList.add(new ModelBox(side2, 22, 52, 11.0F, -35.0F, -7.0F, 0, 2, 14, 0.0F, false));

		left3 = new RendererModel(this);
		left3.setRotationPoint(0.0F, 0.0F, 0.25F);
		side2.addChild(left3);
		left3.cubeList.add(new ModelBox(left3, 92, 6, 8.0F, -3.0F, 1.0F, 1, 2, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 86, 86, 8.0F, -32.0F, 6.75F, 1, 31, 1, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 16, 87, 8.0F, -32.0F, 0.25F, 1, 31, 1, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 91, 25, 8.0F, -32.0F, 1.0F, 1, 2, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 99, 21, 8.0F, -10.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 99, 21, 8.0F, -17.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 98, 92, 8.0F, -24.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 36, 88, 8.75F, -9.0F, 1.0F, 0, 6, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 36, 88, 8.75F, -16.0F, 1.0F, 0, 6, 6, 0.0F, false));
		left3.cubeList.add(new ModelBox(left3, 36, 88, 8.75F, -23.0F, 1.0F, 0, 6, 6, 0.0F, false));

		window5 = new RendererModel(this);
		window5.setRotationPoint(-0.25F, 0.0F, 0.0F);
		left3.addChild(window5);
		

		frame5 = new RendererModel(this);
		frame5.setRotationPoint(0.0F, 0.0F, 0.0F);
		window5.addChild(frame5);
		frame5.cubeList.add(new ModelBox(frame5, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow6 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow6.setRotationPoint(0.0F, 0.0F, 0.0F);
		window5.addChild(glow6);
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow6.cubeList.add(new ModelBox(glow6, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		right3 = new RendererModel(this);
		right3.setRotationPoint(0.0F, 0.0F, -8.25F);
		side2.addChild(right3);
		right3.cubeList.add(new ModelBox(right3, 92, 6, 8.0F, -3.0F, 1.0F, 1, 2, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 86, 86, 8.0F, -32.0F, 6.75F, 1, 31, 1, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 16, 87, 8.0F, -32.0F, 0.25F, 1, 31, 1, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 91, 25, 8.0F, -32.0F, 1.0F, 1, 2, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 99, 21, 8.0F, -10.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 99, 21, 8.0F, -17.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 98, 92, 8.0F, -24.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 36, 82, 8.75F, -9.0F, 1.0F, 0, 6, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 36, 82, 8.75F, -16.0F, 1.0F, 0, 6, 6, 0.0F, false));
		right3.cubeList.add(new ModelBox(right3, 36, 82, 8.75F, -23.0F, 1.0F, 0, 6, 6, 0.0F, false));

		window6 = new RendererModel(this);
		window6.setRotationPoint(-0.25F, 0.0F, 0.0F);
		right3.addChild(window6);
		

		frame6 = new RendererModel(this);
		frame6.setRotationPoint(0.0F, 0.0F, 0.0F);
		window6.addChild(frame6);
		frame6.cubeList.add(new ModelBox(frame6, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow7 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow7.setRotationPoint(0.0F, 0.0F, 0.0F);
		window6.addChild(glow7);
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow7.cubeList.add(new ModelBox(glow7, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		side3 = new RendererModel(this);
		side3.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(side3, 0.0F, 3.1416F, 0.0F);
		side3.cubeList.add(new ModelBox(side3, 66, 85, 8.0F, -36.0F, 8.0F, 2, 35, 2, 0.0F, false));
		side3.cubeList.add(new ModelBox(side3, 12, 87, 8.25F, -32.0F, -0.5F, 1, 31, 1, 0.0F, false));
		side3.cubeList.add(new ModelBox(side3, 76, 64, 8.5F, -32.5F, -8.0F, 1, 1, 16, 0.0F, false));
		side3.cubeList.add(new ModelBox(side3, 66, 0, 7.75F, -33.0F, -8.0F, 2, 1, 16, 0.0F, false));
		side3.cubeList.add(new ModelBox(side3, 0, 66, 8.75F, -35.5F, -9.0F, 2, 3, 18, 0.0F, false));
		side3.cubeList.add(new ModelBox(side3, 22, 52, 11.0F, -35.0F, -7.0F, 0, 2, 14, 0.0F, false));

		left4 = new RendererModel(this);
		left4.setRotationPoint(0.0F, 0.0F, 0.25F);
		side3.addChild(left4);
		left4.cubeList.add(new ModelBox(left4, 92, 6, 8.0F, -3.0F, 1.0F, 1, 2, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 86, 86, 8.0F, -32.0F, 6.75F, 1, 31, 1, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 16, 87, 8.0F, -32.0F, 0.25F, 1, 31, 1, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 91, 25, 8.0F, -32.0F, 1.0F, 1, 2, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 99, 21, 8.0F, -10.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 99, 21, 8.0F, -17.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 98, 92, 8.0F, -24.0F, 1.0F, 1, 1, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 36, 88, 8.75F, -9.0F, 1.0F, 0, 6, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 36, 88, 8.75F, -16.0F, 1.0F, 0, 6, 6, 0.0F, false));
		left4.cubeList.add(new ModelBox(left4, 36, 88, 8.75F, -23.0F, 1.0F, 0, 6, 6, 0.0F, false));

		window7 = new RendererModel(this);
		window7.setRotationPoint(-0.25F, 0.0F, 0.0F);
		left4.addChild(window7);
		

		frame7 = new RendererModel(this);
		frame7.setRotationPoint(0.0F, 0.0F, 0.0F);
		window7.addChild(frame7);
		frame7.cubeList.add(new ModelBox(frame7, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow8 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow8.setRotationPoint(0.0F, 0.0F, 0.0F);
		window7.addChild(glow8);
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow8.cubeList.add(new ModelBox(glow8, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		right4 = new RendererModel(this);
		right4.setRotationPoint(0.0F, 0.0F, -8.25F);
		side3.addChild(right4);
		right4.cubeList.add(new ModelBox(right4, 92, 6, 8.0F, -3.0F, 1.0F, 1, 2, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 86, 86, 8.0F, -32.0F, 6.75F, 1, 31, 1, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 16, 87, 8.0F, -32.0F, 0.25F, 1, 31, 1, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 91, 25, 8.0F, -32.0F, 1.0F, 1, 2, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 99, 21, 8.0F, -10.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 99, 21, 8.0F, -17.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 98, 92, 8.0F, -24.0F, 1.0F, 1, 1, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 36, 82, 8.75F, -9.0F, 1.0F, 0, 6, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 36, 82, 8.75F, -16.0F, 1.0F, 0, 6, 6, 0.0F, false));
		right4.cubeList.add(new ModelBox(right4, 36, 82, 8.75F, -23.0F, 1.0F, 0, 6, 6, 0.0F, false));

		window8 = new RendererModel(this);
		window8.setRotationPoint(-0.25F, 0.0F, 0.0F);
		right4.addChild(window8);
		

		frame8 = new RendererModel(this);
		frame8.setRotationPoint(0.0F, 0.0F, 0.0F);
		window8.addChild(frame8);
		frame8.cubeList.add(new ModelBox(frame8, 0, 72, 9.0F, -30.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow10 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow10.setRotationPoint(0.0F, 0.0F, 0.0F);
		window8.addChild(glow10);
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -29.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -29.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -29.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -29.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -29.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -29.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -26.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -26.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -26.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -26.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -26.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 37, 9.125F, -26.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -28.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -28.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -28.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -28.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -28.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -28.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -25.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -25.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -25.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -25.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -25.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow10.cubeList.add(new ModelBox(glow10, 63, 38, 9.125F, -25.5F, 3.585F, 0, 1, 1, 0.0F, false));

		roof = new RendererModel(this);
		roof.setRotationPoint(0.0F, 24.75F, 0.0F);
		roof.cubeList.add(new ModelBox(roof, 0, 24, -9.5F, -38.25F, -9.5F, 19, 4, 19, 0.0F, false));
		roof.cubeList.add(new ModelBox(roof, 2, 48, -8.5F, -39.25F, -8.5F, 17, 1, 17, 0.0F, false));
		roof.cubeList.add(new ModelBox(roof, 56, 48, -7.5F, -40.25F, -7.5F, 15, 1, 15, 0.0F, false));

		lamp = new RendererModel(this);
		lamp.setRotationPoint(0.0F, 24.0F, 0.0F);
		lamp.cubeList.add(new ModelBox(lamp, 8, 0, -1.5F, -40.0F, -1.5F, 3, 1, 3, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 0, 0, -0.5F, -45.0F, -0.5F, 1, 1, 1, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 75, 2, -1.0F, -44.0F, -1.25F, 2, 4, 0, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 70, 2, -1.0F, -44.0F, 1.25F, 2, 4, 0, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 70, 0, 1.25F, -44.0F, -1.0F, 0, 4, 2, 0.0F, false));
		lamp.cubeList.add(new ModelBox(lamp, 75, 0, -1.25F, -44.0F, -1.0F, 0, 4, 2, 0.0F, false));

		glow9 = new LightRendererModel(this, () -> LIGHT, () -> ALPHA);
		glow9.setRotationPoint(0.0F, 0.0F, 0.0F);
		lamp.addChild(glow9);
		glow9.cubeList.add(new ModelBox(glow9, 14, 4, -1.0F, -44.5F, -1.0F, 2, 5, 2, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(1.0F, 22.0F, -8.75F);
		boti.cubeList.add(new ModelBox(boti, 115, 119, -9.0F, -32.0F, 1.0F, 16, 32, 0, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 115, 119, -9.0F, -32.0F, 16.5F, 16, 32, 0, 0.0F, false));

		backing = new RendererModel(this);
		backing.setRotationPoint(0.0F, 24.0F, 0.0F);
		backing.cubeList.add(new ModelBox(backing, 115, 119, 7.75F, -34.0F, -8.0F, 0, 32, 16, 0.0F, false));
		backing.cubeList.add(new ModelBox(backing, 115, 119, -7.75F, -34.0F, -8.0F, 0, 32, 16, 0.0F, false));
	}

	@Override
	public void render(ExteriorTile tile) {
		EnumDoorState state = tile.getOpen();
		switch(state) {
			case ONE:
				this.left2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				//this.rightwindow.rotateAngleY = (float) Math.toRadians(
						//IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				this.right2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				//this.leftwindow.rotateAngleY = (float) Math.toRadians(
						//IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				break;


			case BOTH:
				this.left2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				//this.rightwindow.rotateAngleY = (float) Math.toRadians(
					//	IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				this.right2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				//this.leftwindow.rotateAngleY = (float) Math.toRadians(
				//		IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				break;


			case CLOSED://close both doors
				this.left2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				this.right2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				//this.rightwindow.rotateAngleY = (float) Math.toRadians(
						//IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				//this.leftwindow.rotateAngleY = (float) Math.toRadians(
					//	IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				break;
			default:
				break;
		}
		//this.botifake.render(0.0625F);
		base.render(0.0625F);
		doors.render(0.0625F);
		side.render(0.0625F);
		side2.render(0.0625F);
		side3.render(0.0625F);
		roof.render(0.0625F);
		lamp.render(0.0625F);
		boti.render(0.0625F);
		backing.render(0.0625F);

		LIGHT = tile.lightLevel;

		if(tile.getMatterState() != EnumMatterState.SOLID)
			ALPHA = tile.alpha;
		else ALPHA = 1F;

	}

	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}

	@Override
	public void renderEntity(TardisEntity ent) {
		GlStateManager.pushMatrix();

		GlStateManager.translated(0, 10, 0);
		GlStateManager.enableRescaleNormal();
		//GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getTextureManager().bindTexture(TrunkExteriorRenderer.TEXTURE);

		base.render(0.0625F);
		//doors.render(0.0625F);
		left2.render(0.0625F);
		window3.render(0.0625F);
		frame3.render(0.0625F);
		glow2.render(0.0625F);
		right2.render(0.0625F);
		window4.render(0.0625F);
		frame4.render(0.0625F);
		glow5.render(0.0625F);
		side.render(0.0625F);
		left.render(0.0625F);
		window.render(0.0625F);
		frame.render(0.0625F);
		glow3.render(0.0625F);
		right.render(0.0625F);
		window2.render(0.0625F);
		frame2.render(0.0625F);
		glow4.render(0.0625F);
		side2.render(0.0625F);
		left3.render(0.0625F);
		window5.render(0.0625F);
		frame5.render(0.0625F);
		glow6.render(0.0625F);
		right3.render(0.0625F);
		window6.render(0.0625F);
		frame6.render(0.0625F);
		glow7.render(0.0625F);
		side3.render(0.0625F);
		left4.render(0.0625F);
		window7.render(0.0625F);
		frame7.render(0.0625F);
		glow8.render(0.0625F);
		right4.render(0.0625F);
		window8.render(0.0625F);
		frame8.render(0.0625F);
		glow10.render(0.0625F);
		roof.render(0.0625F);
		lamp.render(0.0625F);
		glow9.render(0.0625F);
		boti.render(0.0625F);
		backing.render(0.0625F);

		GlStateManager.disableRescaleNormal();

		GlStateManager.popMatrix();

	}
}