package net.tardis.mod.client.models;

import java.util.function.Supplier;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.tardis.mod.helper.RenderHelper;

public class LightRendererModel extends RendererModel{

	Supplier<Float> light;
	Supplier<Float> alpha;
	
	public LightRendererModel(Model model, Supplier<Float> light) {
		super(model);
		this.light = light;
	}
	
	public LightRendererModel(Model model, Supplier<Float> light, Supplier<Float> alpha) {
		this(model, light);
		this.alpha = alpha;
	}
	
	@Override
	public void render(float scale) {
		this.customRender(() -> super.render(scale));
	}

	public void customRender(Runnable run) {
		GlStateManager.pushMatrix();
		float coord = 240.0F * light.get();
		RenderHelper.setLightmapTextureCoords(coord, coord);
		
		run.run();
		
		RenderHelper.restoreLightMap();
		GlStateManager.popMatrix();
	}

}
