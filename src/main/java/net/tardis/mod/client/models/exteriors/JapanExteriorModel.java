package net.tardis.mod.client.models.exteriors;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.6.5
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class JapanExteriorModel extends Model {
	private final RendererModel Roof;
	private final RendererModel Posts;
	private final RendererModel Signage;
	private final RendererModel panels;
	private final RendererModel lAmp;
	private final RendererModel door;
	private final RendererModel glow_lamp;
	private final RendererModel boti;

	public JapanExteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		Roof = new RendererModel(this);
		Roof.setRotationPoint(14.0F, -18.0F, -14.0F);
		Roof.cubeList.add(new ModelBox(Roof, 112, 0, -27.0F, -4.0F, 1.0F, 26, 3, 26, 0.0F, false));
		Roof.cubeList.add(new ModelBox(Roof, 0, 32, -25.0F, -6.0F, 3.0F, 22, 2, 22, 0.0F, false));

		Posts = new RendererModel(this);
		Posts.setRotationPoint(-12.0F, 20.0F, 10.0F);
		Posts.cubeList.add(new ModelBox(Posts, 131, 31, -1.0F, -39.0F, -23.0F, 3, 39, 3, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 161, 70, -3.0F, -40.0F, -25.0F, 4, 2, 4, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 161, 70, 23.0F, -40.0F, -25.0F, 4, 2, 4, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 161, 70, -3.0F, -40.0F, 1.0F, 4, 2, 4, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, -2.0F, -39.0F, 2.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, 24.0F, -39.0F, 2.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, 24.0F, -39.0F, -24.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, -2.0F, -39.0F, -24.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, 25.0F, -42.0F, 3.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, -3.0F, -42.0F, 3.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, 25.0F, -42.0F, -25.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, -3.0F, -42.0F, -25.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 161, 70, 23.0F, -40.0F, 1.0F, 4, 2, 4, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 131, 31, 22.0F, -39.0F, -23.0F, 3, 39, 3, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 131, 31, 22.0F, -39.0F, 0.0F, 3, 39, 3, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 131, 31, -1.0F, -39.0F, 0.0F, 3, 39, 3, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 0, 0, -2.0F, 0.0F, -24.0F, 28, 4, 28, 0.0F, false));

		Signage = new RendererModel(this);
		Signage.setRotationPoint(14.0F, -18.0F, -14.0F);
		Signage.cubeList.add(new ModelBox(Signage, 32, 78, -26.0F, -1.0F, 0.0F, 24, 4, 3, 0.0F, false));
		Signage.cubeList.add(new ModelBox(Signage, 42, 87, -28.0F, -1.0F, 2.0F, 3, 4, 24, 0.0F, false));
		Signage.cubeList.add(new ModelBox(Signage, 32, 78, -26.0F, -1.0F, 25.0F, 24, 4, 3, 0.0F, false));
		Signage.cubeList.add(new ModelBox(Signage, 42, 87, -3.0F, -1.0F, 2.0F, 3, 4, 24, 0.0F, false));

		panels = new RendererModel(this);
		panels.setRotationPoint(9.0F, 20.0F, -12.0F);
		panels.cubeList.add(new ModelBox(panels, 88, 32, 2.0F, -35.0F, 2.0F, 1, 35, 20, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 88, 32, -21.0F, -35.0F, 2.0F, 1, 35, 20, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 0, 86, -19.0F, -35.0F, 0.0F, 20, 35, 1, 0.0F, false));
		panels.cubeList.add(new ModelBox(panels, 96, 87, -9.0F, -35.0F, 23.0F, 10, 35, 1, 0.0F, false));

		lAmp = new RendererModel(this);
		lAmp.setRotationPoint(14.0F, -24.0F, -14.0F);
		lAmp.cubeList.add(new ModelBox(lAmp, 147, 33, -17.0F, -2.0F, 11.0F, 6, 2, 6, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 147, 41, -17.0F, -7.0F, 11.0F, 6, 1, 6, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 145, 70, -16.0F, -9.0F, 12.0F, 4, 2, 4, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 153, 76, -15.0F, -11.0F, 13.0F, 2, 2, 2, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 161, 76, -12.5F, -6.0F, 11.5F, 1, 4, 1, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 161, 76, -16.5F, -6.0F, 11.5F, 1, 4, 1, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 161, 76, -16.5F, -6.0F, 15.5F, 1, 4, 1, 0.0F, false));
		lAmp.cubeList.add(new ModelBox(lAmp, 161, 76, -12.5F, -6.0F, 15.5F, 1, 4, 1, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(0.0F, 20.0F, 10.0F);
		door.cubeList.add(new ModelBox(door, 118, 87, -4.0F, -35.0F, 0.0F, 10, 35, 1, 0.0F, false));

		glow_lamp = new RendererModel(this);
		glow_lamp.setRotationPoint(1.0F, -26.0F, 0.0F);
		glow_lamp.cubeList.add(new ModelBox(glow_lamp, 147, 48, -3.0F, -4.0F, -2.0F, 4, 4, 4, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(9.0F, 20.0F, 9.0F);
		boti.cubeList.add(new ModelBox(boti, 0, 126, -19.0F, -35.0F, 0.0F, 20, 35, 1, 0.0F, false));
	}

	public void render(ExteriorTile tile) {
		
		this.door.offsetX = (float)EnumDoorType.JAPAN.getRotationForState(tile.getOpen());
		
		Roof.render(0.0625F);
		Posts.render(0.0625F);
		Signage.render(0.0625F);
		panels.render(0.0625F);
		lAmp.render(0.0625F);
		door.render(0.0625F);
		boti.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1.0F, this.glow_lamp);
		
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}