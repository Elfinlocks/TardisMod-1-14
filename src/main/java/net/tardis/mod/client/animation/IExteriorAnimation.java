package net.tardis.mod.client.animation;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public interface IExteriorAnimation{

	void tick();
	ExteriorAnimationEntry<?> getType();

    //resets any internal variables this my hold
    default void reset() {
    }

    interface IAnimSpawn<T extends ExteriorAnimation> {
        T create(ExteriorAnimationEntry<T> entry, ExteriorTile tile);
    }

    class ExteriorAnimationEntry<T extends ExteriorAnimation> implements IRegisterable<ExteriorAnimationEntry<T>> {

		private ResourceLocation name;
		private IAnimSpawn<T> spawn;
        private String displayKey;

        public ExteriorAnimationEntry(String display, IAnimSpawn<T> spawn) {
			this.spawn = spawn;
            this.displayKey = display;
        }

		@Override
		public ExteriorAnimationEntry<T> setRegistryName(ResourceLocation regName) {
			this.name = regName;
			return this;
		}

		@Override
		public ResourceLocation getRegistryName() {
			return this.name;
		}

		public T create(ExteriorTile tile) {
			return spawn.create(this, tile);
		}

        public TranslationTextComponent getTranslation() {
            return new TranslationTextComponent(this.displayKey);
        }

	}
}
