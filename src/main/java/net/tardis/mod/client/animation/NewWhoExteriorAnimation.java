package net.tardis.mod.client.animation;

import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class NewWhoExteriorAnimation extends ExteriorAnimation {

    private int pulses = 0;
    private int ticks = 0;
	    
    public NewWhoExteriorAnimation(ExteriorAnimationEntry<?> entry, ExteriorTile exterior) {
		super(entry, exterior);
	}

    @Override
	public void tick() {
        if (this.exterior.getMatterState() == EnumMatterState.DEMAT) {
            if (ticks % 60 < 30) {
                if (pulses <= 2)
                    this.exterior.alpha -= 0.01;
                else this.exterior.alpha -= 0.02;
            } else {
                this.exterior.alpha += 0.01;
            }

            if (ticks % 60 == 0)
                ++this.pulses;
        }
        //Remat
        else {
            if (ticks % 60 < 30) {
                if (pulses < 2)
                    this.exterior.alpha += 0.015;
                else this.exterior.alpha += 0.02;
            } else {
                this.exterior.alpha -= 0.01;
            }

            if (ticks % 60 == 0)
                ++this.pulses;
        }
        ++ticks;
    }


    @Override
    public void reset() {
        this.pulses = 0;
        this.ticks = 0;
    }

}
