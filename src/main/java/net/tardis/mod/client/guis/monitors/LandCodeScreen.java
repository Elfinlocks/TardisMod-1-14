package net.tardis.mod.client.guis.monitors;


import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.LandType;
import net.tardis.mod.tileentities.ConsoleTile;

public class LandCodeScreen extends MonitorScreen {
	
	private TextFieldWidget code;
	private Button setCode;
	private Button cancel;
	private final TranslationTextComponent title = new TranslationTextComponent(Constants.Strings.GUI + "land_code.title");
	private final TranslationTextComponent desc = new TranslationTextComponent(Constants.Strings.GUI + "land_code.desc");
	private final TranslationTextComponent land_code_set = new TranslationTextComponent(Constants.Strings.GUI + "land_code.set");
	private final TranslationTextComponent land_code_suggestion_default = new TranslationTextComponent(Constants.Strings.GUI + "land_code.suggestion.default");
	private final TranslationTextComponent land_code_suggestion_current_value = new TranslationTextComponent(Constants.Strings.GUI + "land_code.suggestion.current_value");

	public LandCodeScreen(IMonitorGui monitor) {
		super(monitor, "land_code");
	}

	@Override
	protected void init() {
		super.init();
		this.buttons.clear();
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		this.setCode = this.createButton(this.parent.getMinX(), this.parent.getMinY(), land_code_set, but -> {
				Network.sendToServer(new ConsoleUpdateMessage(DataTypes.LAND_CODE, new LandType(code.getText())));
				onClose();
		});
		this.cancel = this.createButton(this.parent.getMinX(), this.parent.getMinY(), Constants.Translations.GUI_CANCEL, but -> {
			onClose();
	    });
		int height = this.font.FONT_HEIGHT + 10;
		this.addButton(code = new TextFieldWidget(font,
				centerX - 75,
				this.parent.getMinY() - parent.getHeight() / 2,
				parent.getWidth() - 50,
				height, ""));
		this.addButton(this.setCode);
		this.addButton(this.cancel);
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
		this.drawCenteredString(this.font, title.getFormattedText(),centerX, this.parent.getMaxY() + 10, 0xFFFFFF);
		this.drawCenteredString(this.font, desc.getFormattedText(),centerX, this.parent.getMinY() - (parent.getHeight() / 2) - 15, 0xFFFFFF);
		if (this.code.isFocused()) {
			this.code.setSuggestion("");
		}
		if (this.code.isMouseOver(mouseX, mouseY) && !this.code.isFocused()) {
			
		}
		else if (this.code.getText().isEmpty() && !this.code.isFocused()){
			TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile && te !=null) {
				ConsoleTile console = (ConsoleTile)te;
				this.code.setSuggestion(land_code_suggestion_default.getFormattedText() + console.getLandingCode());
			}
			else {
				this.code.setSuggestion(land_code_suggestion_current_value.getFormattedText());
			}
		}
	}

	@Override
	public void onClose() {
		super.onClose();
	}

}
