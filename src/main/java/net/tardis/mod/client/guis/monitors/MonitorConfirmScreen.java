package net.tardis.mod.client.guis.monitors;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.booleans.BooleanConsumer;
import net.minecraft.util.text.StringTextComponent;


/**
 * A Basic Confirmation GUI that can be used to make user confirm their decision
 * @implSpec Create a method in your parent gui that will open this GUI, then call the method in one of your buttons
 */
public class MonitorConfirmScreen extends MonitorScreen{
	
	
	private final List<String> listLines = Lists.newArrayList();
	protected String message1;
	protected String message2;
	protected String confirmButtonText;
	protected String cancelButtonText;
	protected BooleanConsumer callbackFunction;

	public MonitorConfirmScreen(IMonitorGui monitor, String type) {
		super(monitor, type);
	}
	
	public MonitorConfirmScreen(IMonitorGui monitor, String type, BooleanConsumer callbackFunction,String message1,String message2,String confirmButtonText, String cancelButtonText) {
		super(monitor, type);
		this.callbackFunction = callbackFunction;
		this.message1 = message1;
		this.message2 = message2;
		this.confirmButtonText = confirmButtonText;
		this.cancelButtonText = cancelButtonText;
	}

	@Override
	protected void init() {
		super.init();
		this.buttons.clear();
		this.listLines.clear();
		this.addSubmenu(new StringTextComponent(this.confirmButtonText), (action) -> {
			this.callbackFunction.accept(true); //Confirm button
		});
		this.addSubmenu(new StringTextComponent(this.cancelButtonText), (action) -> {
			this.callbackFunction.accept(false); //Cancel button
		});
		
		this.listLines.addAll(this.minecraft.fontRenderer.listFormattedStringToWidth(this.message1, (this.parent.getMaxX()  - this.parent.getMinX())));
		this.listLines.addAll(this.minecraft.fontRenderer.listFormattedStringToWidth(this.message2, (this.parent.getMaxX()  - this.parent.getMinX())));
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		super.render(mouseX, mouseY, p_render_3_);
		AtomicInteger addToDrawHeight = new AtomicInteger(20);
		this.listLines.forEach(message -> {
			this.drawCenteredString(this.font,
					message,
					this.parent.getMinX() + 100, 
					this.parent.getMaxY()
					+ addToDrawHeight.getAndAdd(this.font.FONT_HEIGHT), //Returns the previous value and adds a value to the old value
					0xFFFFFF);
			
		});
	}

}
