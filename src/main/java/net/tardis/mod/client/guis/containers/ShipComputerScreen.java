package net.tardis.mod.client.guis.containers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.ShipComputerContainer;

public class ShipComputerScreen extends ContainerScreen<ShipComputerContainer>{

	private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/generic_54_container.png");
	
	public ShipComputerScreen(ShipComputerContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int x = 176;
		int totalHeight = 97 - (36 / 9) * 18;
		
		this.blit(width / 2 - x / 2, height / 2 - (17 + 4 * 18) + 6, 0, 0, x, 17 + (3 * 18));
		
		this.blit(width / 2 - x / 2, height / 2 - totalHeight / 2, 0, 125, 176, 97);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		  this.font.drawString(this.title.getFormattedText(), 8.0F, (float)(this.ySize - 159), 4210752);
		  this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, (float)(this.ySize - 92), 4210752);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
	
	

}
