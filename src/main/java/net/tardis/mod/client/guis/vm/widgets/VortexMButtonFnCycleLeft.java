package net.tardis.mod.client.guis.vm.widgets;


import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class VortexMButtonFnCycleLeft extends Button{
	
	ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui.png");
	int BUTTON_WIDTH = 4;
	int BUTTON_HEIGHT = 12;

	
	public VortexMButtonFnCycleLeft(int x, int y, int width, int height, String buttonText, IPressable onPress) {
		super(x, y, width, height, buttonText, onPress);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		if(active) {
			Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
			boolean isHovered;
			if (mouseX >= x - 10 && mouseY >= y && mouseX < x + width && mouseY < y + height) {
				isHovered = true;
			}
			else {
				isHovered = false;
			}
			if (isHovered) {
				blit(this.x, this.y, 48, 205, BUTTON_WIDTH, BUTTON_HEIGHT); //Left Function Cycle Button
			} 
			else {
				blit(this.x, this.y, 20, 205, BUTTON_WIDTH, BUTTON_HEIGHT); //Left Function Cycle Button - Hovered
			}
		}
		
	}
	
	public boolean clicked(double mouseX, double mouseY) {
	      return this.active && this.visible && mouseX >= (double)this.x - 10 && mouseY >= (double)this.y && mouseX < (double)(this.x + this.width) && mouseY < (double)(this.y + this.height);
 }

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int s) {
		if (this.active && this.visible) {
	         if (this.isValidClickButton(s)) {
	            boolean flag = this.clicked(mouseX, mouseY);
	            if (flag) {
	               this.playDownSound(Minecraft.getInstance().getSoundHandler());
	               this.onClick(mouseX, mouseY);
	               return true;
	            }
	         }

	         return false;
	      } else {
	         return false;
	      }
	}
}

