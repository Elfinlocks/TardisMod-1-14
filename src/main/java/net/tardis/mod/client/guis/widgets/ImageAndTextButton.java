package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;

/**
 * A button that uses draws an image but also renders text at the centre of the image
 */
public class ImageAndTextButton extends Button{

    private final ResourceLocation resourceLocation;
    private final int xTexStart;
    private final int yTexStart;
    private final int yDiffText;
    private final int textureWidth;
    private final int textureHeight;
    private String text;
    private int textColourCode;

    public ImageAndTextButton(int xIn, int yIn, int widthIn, int heightIn, int xTexStartIn, int yTexStartIn, int yDiffTextIn, ResourceLocation resourceLocationIn, Button.IPressable onPressIn) {
        this(xIn, yIn, widthIn, heightIn, xTexStartIn, yTexStartIn, yDiffTextIn, resourceLocationIn, 256, 256, onPressIn);
    }

    public ImageAndTextButton(int xIn, int yIn, int widthIn, int heightIn, int xTexStartIn, int yTexStartIn, int yDiffTextIn, ResourceLocation resourceLocationIn, int textureWidth, int textureHeight, Button.IPressable onPressIn) {
        this(xIn, yIn, widthIn, heightIn, xTexStartIn, yTexStartIn, yDiffTextIn, resourceLocationIn, textureWidth, textureHeight, "", 0xFFFF, onPressIn);
    }
    public ImageAndTextButton(int xIn, int yIn, int widthIn, int heightIn, int xTexStartIn, int yTexStartIn, int yDiffTextIn, ResourceLocation resourceLocationIn, String textToDisplay, int textColourCode, Button.IPressable onPressIn) {
    	this(xIn, yIn, widthIn, heightIn, xTexStartIn, yTexStartIn, yDiffTextIn, resourceLocationIn, 256, 256, textToDisplay, textColourCode, onPressIn);
    }
	public ImageAndTextButton(int xIn, int yIn, int widthIn, int heightIn, int xTexStartIn, int yTexStartIn, int yDiffTextIn, ResourceLocation resourceLocationIn, int textureWidth, int textureHeight, String textToDisplay, int textColourCode, Button.IPressable onPressIn) {
	      super(xIn, yIn, widthIn, heightIn, textToDisplay, onPressIn);
	      this.textureWidth = textureWidth;
	      this.textureHeight = textureHeight;
	      this.xTexStart = xTexStartIn;
	      this.yTexStart = yTexStartIn;
	      this.yDiffText = yDiffTextIn;
	      this.resourceLocation = resourceLocationIn;
	      this.text = textToDisplay;
	      this.textColourCode = textColourCode;
	}

	@Override
	public void renderButton(int mouseX, int mouseY, float partialTicks) {
		Minecraft minecraft = Minecraft.getInstance();
	    minecraft.getTextureManager().bindTexture(this.resourceLocation);
	    GlStateManager.disableDepthTest();
	    int i = this.yTexStart;
	    if (this.isHovered()) {
	        i += this.yDiffText;
	    }
	    blit(this.x, this.y, (float)this.xTexStart, (float)i, this.width, this.height, this.textureWidth, this.textureHeight);
	    GlStateManager.enableDepthTest();
	    this.drawCenteredString(minecraft.fontRenderer, this.text, this.x + this.width / 2, this.y + (this.height - 8) / 2, this.textColourCode);
	}
	
	

}
