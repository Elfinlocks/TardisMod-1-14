package net.tardis.mod.client.guis.vm.widgets;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.TextFieldWidget;


/**
 * Created by 50ap5ud5
 * on 17 Mar 2020 @ 9:51:12 pm
 */

/*
 * A custom text field widget that doesn't allow user to click in the textbox
 * Mainly used to display text without needing to consider things such as :
 * - Text going over the gui texture if too long
 * - Text not contrasting enough with gui texture
 */
public class DisabledTextFieldWidget extends TextFieldWidget{

	public DisabledTextFieldWidget(FontRenderer fontIn, int p_i51137_2_, int p_i51137_3_, int p_i51137_4_, int p_i51137_5_,
			String msg) {
		super(fontIn, p_i51137_2_, p_i51137_3_, p_i51137_4_, p_i51137_5_, msg);
	}
	
	@Override
	public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
		return false;
	}
	

}
