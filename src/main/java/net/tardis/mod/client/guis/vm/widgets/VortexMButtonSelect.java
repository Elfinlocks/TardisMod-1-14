package net.tardis.mod.client.guis.vm.widgets;


import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;


public class VortexMButtonSelect extends Button{
	

	ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui.png");
	int BUTTON_WIDTH = 14;
	int BUTTON_HEIGHT = 14;
	
	public VortexMButtonSelect(int x, int y, int width, int height, String buttonText, IPressable onPress) {
		super(x, y, width, height, buttonText, onPress);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		if(active) {
			Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
			boolean isHovered;
			if (mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height) {
				isHovered = true;
			}
			else {
				isHovered = false;
			}
			if (isHovered) {
				blit(this.x, this.y, 42, 175, BUTTON_WIDTH, BUTTON_HEIGHT);
			} else {
				blit(this.x, this.y, 14, 175, BUTTON_WIDTH, BUTTON_HEIGHT);
			}
		}
		
	}
}

