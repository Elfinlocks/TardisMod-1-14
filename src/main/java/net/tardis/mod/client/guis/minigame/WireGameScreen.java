package net.tardis.mod.client.guis.minigame;

import java.awt.Color;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.FailEngineMessage;
import net.tardis.mod.sounds.TSounds;

public class WireGameScreen extends Screen{

	private static final ResourceLocation TEXTURE = Helper.createRL("textures/gui/minigame/wire_minigame.png");
	private static final StringTextComponent TITLE = new StringTextComponent("WIRE");
	private List<Wire> connections = Lists.newArrayList();
	private List<Terminal> terminals = Lists.newArrayList();
	private int slotNumber = 0;
	private Direction panel;
	private boolean completed = false;
	
	public WireGameScreen(int slot, Direction panel) {
		super(TITLE);
		this.slotNumber = slot;
		this.panel = panel;
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		this.renderBackground();
		
		super.render(mouseX, mouseY, p_render_3_);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
		
		for(Wire wire : this.connections) {
			wire.updateMouse(mouseX, mouseY);
			wire.render(bb, mouseX, mouseY);
		}
		
		Tessellator.getInstance().draw();
		
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_COLOR);
		for(Terminal term : this.terminals) {
			term.render(bb);
		}
		Tessellator.getInstance().draw();
		
		
	}
	
	public Wire getSelected(int mouseX, int mouseY) {
		for(Wire wire : this.connections) {
			if(Helper.isInBounds(mouseX, mouseY, wire.startPos.getX() - 5, wire.startPos.getZ() - 5, wire.startPos.getX() + 5, wire.startPos.getZ() + 5)) {
				return wire;
			}
		}
		return null;
	}

	@Override
	protected void init() {
		super.init();
		this.connections.clear();
		this.terminals.clear();
		
		int x = this.width / 2 - 100;
		int baseHeight = this.height / 2 + (Type.values().length / 2) * 20;
		
		for(int i = 0; i < Type.values().length; ++i) {
			this.connections.add(new Wire(Type.values()[i], new Vec3i(x, 0, baseHeight - (i * 40))));
		}
		
		List<Integer> startHeights = Lists.newArrayList();
		for(int i = 0; i < Type.values().length; ++i) {
			startHeights.add(i * 40);
		}
		Random rand = Minecraft.getInstance().player.getRNG();
		
		for(Type type : Type.values()) {
			Integer height = startHeights.get(rand.nextInt(startHeights.size()));
			this.terminals.add(new Terminal(new Vec3i(x + 120, 0, baseHeight - height), type));
			startHeights.remove(height);
		}
	}
	
	@Override
	public void tick() {
		super.tick();
		
		boolean finished = true;
		for(Wire wire : this.connections) {
			if(!wire.complete) {
				finished = false;
				break;
			}
		}
		
		//If all are finished
		if(finished) {
			this.minecraft.player.playSound(TSounds.REACHED_DESTINATION, 1F, 1F);
			Minecraft.getInstance().displayGuiScreen(null);
			this.completed = true;
		}
		
		
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	public void fail() {
		//TODO: Fail
		Minecraft.getInstance().displayGuiScreen(null);
		this.minecraft.player.playSound(TSounds.SONIC_FAIL, 1F, 1F);
		Network.sendToServer(new FailEngineMessage(this.panel, this.slotNumber));
	}
	
	public static class Wire{
		
		Type type;
		Vec3i startPos;
		Vec3i endPos;
		boolean isSelected = false;
		boolean complete = false;
		
		public Wire(Type type, Vec3i start) {
			this.startPos = start;
			this.endPos = new Vec3i(start.getX() + 5, start.getY(), start.getZ());
			this.type = type;
		}
		
		public void updateMouse(int x, int z) {
			if(this.isSelected && !this.complete)
				this.endPos = new Vec3i(x, 0, z);
		}
		
		public void setSelected(boolean select) {
			this.isSelected = select;
		}
		
		public void render(BufferBuilder bb, int mouseX, int mouseY) {
			
			double maxU = 7 / 256.0;
			double maxV = 7 / 256.0;
			
			bb.pos(this.startPos.getX(), this.startPos.getZ(), 0).tex(0, 0).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			bb.pos(this.startPos.getX(), this.startPos.getZ() + 5, 0).tex(0, maxV).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			bb.pos(this.endPos.getX() + 5, this.endPos.getZ() + 5, 0).tex(maxU, maxV).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			bb.pos(this.endPos.getX() + 5, this.endPos.getZ(), 0).tex(maxU, 0).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();

		}

		public void setComplete(boolean complete) {
			this.complete = complete;
		}
		
	}
	
	public static class Terminal{
		
		Vec3i pos;
		Type type;
		int size = 10;
		
		public Terminal(Vec3i pos, Type type) {
			this.pos = pos;
			this.type = type;
		}
		
		public void render(BufferBuilder bb) {
			int x = this.pos.getX(), z = this.pos.getZ();
			
			double texSize = 7 / 256.0;
			double u = 0, v = 7 / 256.0;
			
			
			double termOverlayX = 7 / 256.0;
			double termOverlayY = 10 / 256.0;
			
			int oX = x + 5;
			int oZ = z + 2;
			
			//Terminal color overlay
			bb.pos(oX, oZ, 0).tex(termOverlayX, termOverlayY).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			bb.pos(oX, oZ + 10, 0).tex(termOverlayX, termOverlayY + (3 / 256.0)).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			bb.pos(oX + 4, oZ + 10, 0).tex(termOverlayX + (1 / 256.0), termOverlayY + (3 / 256.0)).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			bb.pos(oX + 4, oZ, 0).tex(termOverlayX + (1/256.0), termOverlayY).color(type.color.getRed() / 255.0F, type.color.getGreen() / 255.0F, type.color.getBlue() / 255.0F, 1.0F).endVertex();
			
			//Terminal casing
			bb.pos(x, z, 0).tex(u, v).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
			bb.pos(x, z + size, 0).tex(u, v + texSize).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
			bb.pos(x + size, z + size, 0).tex(u + texSize, v + texSize).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
			bb.pos(x + size, z, 0).tex(u + texSize, v).color(1.0F, 1.0F, 1.0F, 1.0F).endVertex();
			
			
		}
		
		public int getSize() {
			return size;
		}
		
	}


	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int p_mouseClicked_5_) {
		boolean result = super.mouseClicked(mouseX, mouseY, p_mouseClicked_5_);
		
		//Check if connecting to terminal
		for(Wire wire : this.connections) {
			//If this is the held wire
			if(wire.isSelected) {
				
				for(Terminal term : this.terminals) {
					//If we're clicking on this terminal
					if(Helper.isInBounds((int)mouseX, (int)mouseY, term.pos.getX(), term.pos.getZ(), term.pos.getX() + term.getSize(), term.pos.getZ() + term.getSize())) {
						//If this is the right wire
						if(term.type == wire.type) {
							wire.setSelected(false);
							wire.setComplete(true);
							wire.endPos = new Vec3i(term.pos.getX() - (term.getSize() / 2.0) + 2, 0, term.pos.getZ() + 2);
							return result;
						}
						else fail();
					}
				}
				
			}
		}
		
		for(Wire wire : connections) {
			wire.setSelected(false);
			
		}
		
		Wire wire = this.getSelected((int)mouseX, (int)mouseY);
		if(wire != null && !wire.complete)
			wire.setSelected(true);
			
		return result;
	}
	
	public static enum Type{
		RED(1, 0, 0),
		GREEN(0, 1, 0),
		PURPLE(0xab0dfd),
		YELLOW(0xcbfd0d);
		
		Color color = Color.BLACK;
		
		Type(float red, float green, float blue) {
			this.color = new Color(red, green, blue);
		}
		
		Type(int hexCode) {
			this.color = new Color(hexCode);
		}
	}

	@Override
	public void onClose() {
		super.onClose();
		if(!this.completed)
			this.fail();
	}
}
