package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.models.consoles.KeltConsoleModel;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class KeltConsoleRenderer extends TileEntityRenderer<KeltConsoleTile> {

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/consoles/kelt.png");
	public static final KeltConsoleModel MODEL = new KeltConsoleModel();
	
	@Override
	public void render(KeltConsoleTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.8, z + 0.5);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(1.2, 1.2, 1.2);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
		
		//Scanner
		Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
			MonitorControl monitor = tile.getControl(MonitorControl.class);
			if(monitor.getMode() == MonitorMode.SCANNER) {
				GlStateManager.pushMatrix();
				GlStateManager.translated(x + 0.5, y + 1, z + 0.5);
				GlStateManager.color3f(1, 1, 1);
				RenderHelper.setupRenderLightning();
				RenderScanner.renderWorldToPlane(data, -0.128F, -0.052F, 0.15F, 0.14F, -0.82F, monitor.getView().getAngle(), partialTicks);
				RenderHelper.finishRenderLightning();
				GlStateManager.popMatrix();
			}
		});
		
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
