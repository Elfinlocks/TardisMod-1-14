package net.tardis.mod.client.renderers;

import javax.annotation.Nullable;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.EntityRayTraceResult;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.items.TItems;

public class InvisEntityRenderer extends EntityRenderer<Entity> {

	public InvisEntityRenderer(EntityRendererManager p_i46179_1_) {
        super(p_i46179_1_);
    }

    @Override
    protected void renderName(Entity entity, double x, double y, double z) {
    	GlStateManager.pushMatrix();
        super.renderName(entity, x, y, z);
        if(entity instanceof ControlEntity &&
        		Minecraft.getInstance().objectMouseOver instanceof EntityRayTraceResult && PlayerHelper.isInEitherHand(Minecraft.getInstance().player, TItems.MANUAL))
        	if(((EntityRayTraceResult)Minecraft.getInstance().objectMouseOver).getEntity() == entity)
        		this.renderLivingLabel(entity, entity.getDisplayName().getFormattedText(), x, y - 0.25, z, 64);
        GlStateManager.popMatrix();
    }
    
    //Comment out the if check to view control hitboxes whilst in debug mode
    @Override
   	public void doRender(Entity entity, double x, double y, double z, float entityYaw, float partialTicks) {
    	super.doRender(entity, x, y, z, entityYaw, partialTicks);
		if(entity instanceof ControlEntity && ((ControlEntity)entity).getControl() != null) {
			if(((ControlEntity) entity).getControl().isGlowing() || Minecraft.getInstance().player.getHeldItemMainhand().getItem() == TItems.DEBUG) {
				GlStateManager.pushMatrix();
		    	GlStateManager.translated(x, y, z);
				RenderHelper.renderAABB(entity.getBoundingBox().offset(entity.getPositionVec().scale(-1)), 0.22F, 0.86F, 0.84F, 0.2F);
		        GlStateManager.popMatrix();
			}
		}
   	}


    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(Entity control) {
        return null;
    }
}
