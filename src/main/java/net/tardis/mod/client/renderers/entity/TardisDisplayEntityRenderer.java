package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.TardisDisplayEntity;

public class TardisDisplayEntityRenderer extends EntityRenderer<TardisDisplayEntity>{

	public TardisDisplayEntityRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	protected ResourceLocation getEntityTexture(TardisDisplayEntity entity) {
		return null;
	}

	@Override
	public void doRender(TardisDisplayEntity entity, double x, double y, double z, float entityYaw,float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y, z);
		
		if(entity.getTile() != null) {
		
			TileEntityRendererDispatcher.instance.render(entity.getTile(), 0, 1, 0, partialTicks, -1, true);
		}
		
		GlStateManager.popMatrix();
	}

}
