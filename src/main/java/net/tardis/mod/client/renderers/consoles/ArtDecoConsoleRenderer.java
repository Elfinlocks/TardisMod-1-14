package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ArtDecoConsoleModel;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;

public class ArtDecoConsoleRenderer extends TileEntityRenderer<ArtDecoConsoleTile> {

	private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/art_deco.png");
	private static final ArtDecoConsoleModel MODEL = new ArtDecoConsoleModel();
	
	@Override
	public void render(ArtDecoConsoleTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.75, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tileEntityIn);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

}
