package net.tardis.mod.client.renderers.boti;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Maps;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.boti.BlockStore;
import net.tardis.mod.boti.BotiWorld;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.helper.Helper;

@OnlyIn(Dist.CLIENT)
public class BotiManager {
	
	private static VertexFormat FORMAT = DefaultVertexFormats.BLOCK;
	private VertexBuffer solidVBO;
	private VertexBuffer translucentVBO;
	public Framebuffer fbo;
	public WorldRenderer worldRenderer;
	public BotiWorld world;
	
	public void setupVBO() {
		if(this.solidVBO == null)
			solidVBO = new VertexBuffer(FORMAT);
		solidVBO.bindBuffer();
	}
	
	public void setupTranslucentVBO() {
		if(this.translucentVBO == null)
			this.translucentVBO = new VertexBuffer(FORMAT);
		this.translucentVBO.bindBuffer();
	}
	
	public void drawData(VertexBuffer buffer) {
		GlStateManager.vertexPointer(3, GL11.GL_FLOAT, FORMAT.getSize(), 0);
		GlStateManager.colorPointer(4, GL11.GL_UNSIGNED_BYTE, FORMAT.getSize(), FORMAT.getColorOffset());
		GlStateManager.texCoordPointer(2, GL11.GL_FLOAT, FORMAT.getSize(), FORMAT.getUvOffsetById(0));
		
		GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		GlStateManager.enableClientState(GL11.GL_COLOR_ARRAY);
		buffer.drawArrays(GL11.GL_QUADS);
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.disableClientState(GL11.GL_COLOR_ARRAY);
	}
	
	public void writeBufferData(BufferBuilder data, VertexBuffer buffer) {
		data.finishDrawing();
		data.reset();
		buffer.bufferData(data.getByteBuffer());
	}
	
	public void endVBO() {
		VertexBuffer.unbindBuffer();
	}
	
	//Render the other world here
	@SuppressWarnings("deprecation")
	public void renderWorld(WorldShell shell) {
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		
		shell.getWorld().setShell(shell);
		
		RenderHelper.disableStandardItemLighting();
		Minecraft.getInstance().textureManager.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
		if(this.solidVBO == null || shell.needsUpdate()) {
			this.solidVBO = null;
			
			HashMap<BlockPos, BlockStore> nextPass = Maps.newHashMap();
			
			this.setupVBO();
			
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			bb.begin(GL11.GL_QUADS, FORMAT);
			
			//Solid Layer and parse out second pass
			
			for(Entry<BlockPos, BlockStore> entry : shell.getMap().entrySet()) {
				if (!Helper.canRenderInBOTI(entry.getValue().getState())) //If state is blacklisted, skip it.
					continue;
				BlockRenderLayer layer = entry.getValue().getState().getBlock().getRenderLayer();
				if(layer != BlockRenderLayer.CUTOUT && layer != BlockRenderLayer.TRANSLUCENT) {
					Minecraft.getInstance().getBlockRendererDispatcher()
					.renderBlock(entry.getValue().getState(), entry.getKey(), shell, bb, new Random());
				}
				else nextPass.put(entry.getKey(), entry.getValue());
				
				if(!entry.getValue().getState().getFluidState().isEmpty())
					nextPass.put(entry.getKey(), entry.getValue());
			}
			bb.sortVertexData(shell.getOffset().getX(), shell.getOffset().getY(), shell.getOffset().getZ());
			this.writeBufferData(bb, this.solidVBO);
			
			//Translucent layer
			
			this.translucentVBO = null;
			this.setupTranslucentVBO();
			bb.begin(GL11.GL_QUADS, FORMAT);
			for(Entry<BlockPos, BlockStore> entry : nextPass.entrySet()) {
				
				Minecraft.getInstance().getBlockRendererDispatcher()
					.renderBlock(entry.getValue().getState(), entry.getKey(), shell, bb, new Random());
				
				Minecraft.getInstance().getBlockRendererDispatcher()
				.renderFluid(entry.getKey(), shell, bb, entry.getValue().getState().getFluidState());
				
			}
			bb.sortVertexData(shell.getOffset().getX(), shell.getOffset().getY(), shell.getOffset().getZ());
			this.writeBufferData(bb, translucentVBO);
			
			shell.setupTEs(shell.getWorld());
			
			shell.setNeedsUpdate(false);
		}
		//Render the VBOs
		GlStateManager.pushMatrix();
		this.setupVBO();
		this.drawData(this.solidVBO);
		this.endVBO();
		
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(SourceFactor.ONE, DestFactor.ONE_MINUS_SRC_ALPHA);
		this.setupTranslucentVBO();
		this.drawData(this.translucentVBO);
		this.endVBO();
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
		
		//Render tiles
		
		RenderHelper.enableStandardItemLighting();
		GlStateManager.color3f(1, 1, 1);
		
		//TEs
		GlStateManager.pushMatrix();
		for(Entry<BlockPos, TileEntity> entry : shell.getTiles().entrySet()) {
			entry.getValue().setWorld(shell.getWorld());
			TileEntityRendererDispatcher.instance.render(entry.getValue(), entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ(), 1.0F, 0, false);
		}
		GlStateManager.popMatrix();
		
		//Normal entities
		GlStateManager.pushMatrix();
		for(Entity e : shell.getEntities()) {
		    if (Helper.canRenderInBOTI(e)) //If entity is not blacklisted, render it.
		        Minecraft.getInstance().getRenderManager().getRenderer(e).doRender(e, e.posX, e.posY, e.posZ, e.rotationYaw, 1.0F);
		}
		GlStateManager.color3f(1, 1, 1F);
		
		GlStateManager.popMatrix();
		
	}
	
	public void setupFramebuffer() {
		if(fbo == null) {
			//fbo = new Framebuffer(Minecraft.getInstance().mainWindow.getFramebufferWidth(), Minecraft.getInstance().mainWindow.getFramebufferHeight(), true, Minecraft.IS_RUNNING_ON_MAC);
			fbo = new Framebuffer(1080, 1080, true, Minecraft.IS_RUNNING_ON_MAC);
		}
		
		fbo.framebufferClear(Minecraft.IS_RUNNING_ON_MAC);
		fbo.bindFramebuffer(true);
		
		fbo.checkFramebufferComplete();
	}
	
	public void endFBO() {
		fbo.unbindFramebuffer();
	}
	
	public void drawTex() {
		fbo.bindFramebufferTexture();
	}
	
	public void renderPortal(Runnable renderWorld, Runnable drawPortal) {
		GL11.glEnable(GL11.GL_STENCIL_TEST);

		// Always write to stencil buffer
		GL11.glStencilFunc(GL11.GL_ALWAYS, 1, 0xFF);
		GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_REPLACE);
		GL11.glStencilMask(0xFF);
		GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

		GlStateManager.depthMask(false);
		
		//Draw portal
		drawPortal.run();
		GlStateManager.depthMask(true);

		// Only pass stencil test if equal to 1(So only if rendered before)
		GL11.glStencilMask(0x00);
		GL11.glStencilFunc(GL11.GL_EQUAL, 1, 0xFF);
		
		renderWorld.run();
		
		GL11.glDisable(GL11.GL_STENCIL_TEST);
		GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

		GL11.glColorMask(false, false, false, false);
		GlStateManager.depthMask(false);
		drawPortal.run();

		//Set things back
		GL11.glColorMask(true, true, true, true);
		GlStateManager.depthMask(true);
	}

}
