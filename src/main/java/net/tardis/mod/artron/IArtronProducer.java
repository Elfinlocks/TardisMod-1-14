package net.tardis.mod.artron;

public interface IArtronProducer {

	//returns amount to take
	float takeArtron(float amt);
}
