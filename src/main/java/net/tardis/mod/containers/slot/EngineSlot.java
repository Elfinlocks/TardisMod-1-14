package net.tardis.mod.containers.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.util.Direction;

public class EngineSlot extends Slot{

	Direction panel;
	
	public EngineSlot(Direction panel, IInventory itemHandler, int index, int xPosition, int yPosition) {
		super(itemHandler, index, xPosition, yPosition);
		this.panel = panel;
	}

	public Direction getPanelSide() {
	    return this.panel;
	}
	
	public void setPanelSide(Direction dir) {
        this.panel = dir;
    }

}
