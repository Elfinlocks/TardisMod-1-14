package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.QuantiscopeTile;

public class QuantiscopeSonicContainer extends Container{
	
	//Needed for customization
	public BlockPos pos;
	
	public QuantiscopeSonicContainer(int id, PlayerInventory inv, PacketBuffer extraData) {
		super(TContainers.QUANTISCOPE, id);
		pos = extraData.readBlockPos();
		TileEntity te = inv.player.world.getTileEntity(pos);
		if(te instanceof QuantiscopeTile) {
			init((QuantiscopeTile)te, inv);
		}
	}
	
	public QuantiscopeSonicContainer(int id, PlayerInventory inv, QuantiscopeTile workbench) {
		super(TContainers.QUANTISCOPE, id);
		init(workbench, inv);
	}
	
	public void init(QuantiscopeTile workbench, PlayerInventory inv) {
		
		this.addSlot(new SonicSlot(workbench, 0, 80, 62));

		for(int l = 0; l < 3; ++l) {
	         for(int j1 = 0; j1 < 9; ++j1) {
	            this.addSlot(new Slot(inv, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + 3));
	         }
	      }

	      for(int i1 = 0; i1 < 9; ++i1) {
	         this.addSlot(new Slot(inv, i1, 8 + i1 * 18, 161 + 3));
	      }
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}


	@Override
	public ItemStack transferStackInSlot(PlayerEntity player, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		final Slot slot = inventorySlots.get(index);
		if ((slot != null) && slot.getHasStack()) {
			final ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			final int containerSlots = inventorySlots.size() - player.inventory.mainInventory.size();
			if (index < containerSlots) {
				if (!mergeItemStack(itemstack1, containerSlots, inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!mergeItemStack(itemstack1, 0, containerSlots, false)) {
				return ItemStack.EMPTY;
			}
			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}
			slot.onTake(player, itemstack1);
		}
		return itemstack;
	}
	
	public void setSlotChangeAction(Runnable run) {
		if(this.getSlot(0) instanceof SonicSlot)
			((SonicSlot)this.getSlot(0)).setOnSlotChanged(run);
	}
	
	public static class SonicSlot extends SlotItemHandler{

		private Runnable onSlotChanged;
		
		public SonicSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
			super(itemHandler, index, xPosition, yPosition);
		}

		@Override
		public boolean isItemValid(ItemStack stack) {
			return stack.getItem() == TItems.SONIC;
		}

		@Override
		public void onSlotChanged() {
			super.onSlotChanged();
			if(onSlotChanged != null)
				onSlotChanged.run();
		}
		
		public void setOnSlotChanged(Runnable run) {
			this.onSlotChanged = run;
		}
		
	}
	
}
