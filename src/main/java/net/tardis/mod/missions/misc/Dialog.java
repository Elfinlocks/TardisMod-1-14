package net.tardis.mod.missions.misc;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class Dialog {

	private TranslationTextComponent speaker;
	private List<DialogOption> options = Lists.newArrayList();
	
	public Dialog(String speaker) {
		this(new TranslationTextComponent(speaker));
	}
	
	public Dialog(TranslationTextComponent speaker) {
		this.speaker = speaker;
	}
	
	public Dialog addDialogOption(DialogOption dia) {
		this.options.add(dia);
		return this;
	}
	
	public TranslationTextComponent getSpeakerLine(){
		return this.speaker;
	}
	
	public List<DialogOption> getOptions() {
		return this.options;
	}
	
	public static String createTranslationKey(String string) {
		return "dialog." + Tardis.MODID + ".";
	}
}
