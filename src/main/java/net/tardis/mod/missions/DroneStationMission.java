package net.tardis.mod.missions;

import java.util.ArrayList;

import com.google.common.collect.Lists;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.item.ItemStack;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.humanoid.CrewmateEntity;
import net.tardis.mod.entity.humanoid.ShipCaptainEntity;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.CompleteMissionMessage;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.network.packets.SetMissionStageMessage;
import net.tardis.mod.registries.MissionRegistry;

public class DroneStationMission extends KillMission{
	
	public DroneStationMission(World world, BlockPos pos, int range) {
		super(MissionRegistry.STATION_DRONE, world, pos, range);
	}

	@Override
	public ItemStack getReward() {
		return new ItemStack(TItems.TELE_STRUCTURE_UPGRADE);
	}

	@Override
	public int getMaxStage() {
		return 15;
	}

	@Override
	public EntityType<?> getEntityType() {
		return TEntities.SECURITY_DROID;
	}

	@Override
	public float getProgressBarPercent() {
		return this.getStage() / (float)this.getMaxStage();
	}

	@Override
	public boolean shouldKillAdvance() {
		return this.getStage() > 0 && this.getStage() < 15;
	}

	@Override
	public boolean shouldSpawnMore() {
		return this.getStage() > 0 && !this.isComplete() && this.getValidEntitiesInArea() < 8;
	}

	@Override
	public Dialog getDialogForStage(LivingEntity speaker, int stage) {
		if(getStage() == 0) {
			Dialog root = new Dialog("mission.tardis.dialog.capt.greet");
			
			//Response to help
			Dialog helpResp = new Dialog("mission.tardis.dialog.capt.recieve_help");
			helpResp.addDialogOption(new DialogOption(null, "Alright"));
			
			DialogOption helping = new DialogOption(helpResp, "I'm responding to your S. O. S");
			helping.setOptionAction((speak, player) -> {
				Network.sendToServer(new SetMissionStageMessage(speak.getPosition(), 1));
			});
			
			
			root.addDialogOption(helping);
			return root;
		}
		else if(isComplete()) {
			if(!getAwarded()) {
				Dialog dia = new Dialog("Here, take this, it's part of our navigation systems. It may not look much, but It should be very valuable scrap");
				DialogOption thanks = new DialogOption(null, "Thank you! I'm sure I can find a use for it");
				DialogOption thanksAlt = new DialogOption(null, "Thanks, I should be able to integrate this into my own ship");
				dia.addDialogOption(thanks);
				dia.addDialogOption(thanksAlt);
				Network.sendToServer(new CompleteMissionMessage(speaker.getPosition()));
				return dia;
			}
		}
		else {
			Dialog root = new Dialog("Hurry, please!");
			
			root.addDialogOption(new DialogOption(null, "Working on it"));
			
			return root;
		}
		return null;
	}
	
	public static BlockPos setupMission(ServerWorld world, BlockPos start) {
		int radius = 10000 / 2;
		
		BlockPos test = new BlockPos(
				start.getX() + (-radius + (world.getRandom().nextInt(radius))),
				0,
				start.getX() + (-radius + (world.getRandom().nextInt(radius))));
		
		if(!world.getWorldBorder().contains(test) || !world.getWorldBorder().contains(test.add(128, 0, 128)))
			return BlockPos.ZERO;
		
		ChunkPos cPos = new ChunkPos(test);
		if(!world.chunkExists(cPos.x, cPos.z)) {
			for(int x = 0; x < 5; x++) {
				for(int z = 0; z < 5; z++) {
					world.forceChunk(cPos.x + x, cPos.z + z, true);
				}
			}
			
			ArrayList<BlockPos> landingSpots = Lists.newArrayList();
			
			test = test.add(0, 64 + (world.rand.nextDouble() * 64), 0);
			PlacementSettings settings = new PlacementSettings();
			Template temp = world.getStructureTemplateManager().getTemplate(Helper.createRL("tardis/structures/worldgen/space/spacestation_drone"));
			temp.addBlocksToWorld(world, test, new PlacementSettings());
			
			//Process data blocks
			for(BlockInfo info : temp.func_215381_a(test, settings, Blocks.STRUCTURE_BLOCK)){
				if(info.nbt != null && info.nbt.contains("metadata")) {
					String data = info.nbt.getString("metadata");
					
					if(data.contentEquals("captain_spawn")) {
						final BlockPos entCap = info.pos.toImmutable();
						ShipCaptainEntity cap = TEntities.SHIP_CAPTIAN.create(world);
						cap.setPosition(entCap.getX() + 0.5, entCap.getY() + 1, entCap.getZ() + 0.5);
						cap.onInitialSpawn(world.getWorld(), world.getDifficultyForLocation(entCap), SpawnReason.STRUCTURE, null, null);
						world.addEntity(cap);
					}
					else if(data.contentEquals("mission_marker")) {
						world.getServer().enqueue(new TickDelayedTask(0, () -> {
							world.getCapability(Capabilities.MISSION).ifPresent(missions -> {
								MiniMission mis = MissionRegistry.STATION_DRONE.create(world, info.pos, 64);
								missions.addMission(mis);
								Network.sendToAllInWorld(new MissionUpdateMessage(mis), world);
							});
						}));
					}
					else if(data.contentEquals("crewmates")) {
						int num = 5 + world.getRandom().nextInt(3);
						for(int i = 0 ; i < num; ++i) {
							CrewmateEntity entity = TEntities.CREWMATE.create(world);
							entity.setPosition(info.pos.getX() + 0.5, info.pos.getY() + 1, info.pos.getZ() + 0.5);
							entity.onInitialSpawn(world.getWorld(), world.getDifficultyForLocation(info.pos), SpawnReason.STRUCTURE, null, null);
							world.addEntity(entity);
						}
					}
				}
				world.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 3);
			}
			
			for(BlockInfo info : temp.func_215381_a(test, settings, TBlocks.landing_pad)){
				landingSpots.add(info.pos);
			}
			
			for(int x = 0; x < 5; x++) {
				for(int z = 0; z < 5; z++) {
					world.forceChunk(cPos.x + x, cPos.z + z, false);
				}
			}
			return landingSpots.size() > 0 ? landingSpots.get(world.rand.nextInt(landingSpots.size())) : test.toImmutable();
		}
		return BlockPos.ZERO;
		
	}

}
