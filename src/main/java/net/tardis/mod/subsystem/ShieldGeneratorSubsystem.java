package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;

public class ShieldGeneratorSubsystem extends Subsystem{

	private boolean isOn = false;
	
	public ShieldGeneratorSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("on", this.isOn);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.isOn = nbt.getBoolean("on");
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {}

	@Override
	public void onFlightSecond() {}

	@Override
	public boolean stopsFlight() {
		return false;
	}

	@Override
	public boolean shouldSpark() {
		return false;
	}
	
	public void setActivated(boolean on) {
		this.isOn = on;
	}
	
	public boolean getActivted() {
		return this.isOn;
	}

}
