package net.tardis.mod.compat.jei;

import java.util.ArrayList;
import java.util.List;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.compat.jei.ARSRecipeCategory.ARSRecipe;
import net.tardis.mod.recipe.WeldRecipe;
import net.tardis.mod.tags.TardisBlockTags;

@JeiPlugin
public class JEI implements IModPlugin{

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "jei");

	@Override
	public ResourceLocation getPluginUid() {
		return NAME;
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration reg) {
		reg.addRecipeCategories(new AlembicRecipeContext(reg.getJeiHelpers().getGuiHelper()));
		reg.addRecipeCategories(new ARSRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
		reg.addRecipeCategories(new WeldRecipeCategory(reg.getJeiHelpers().getGuiHelper()));
	}

	@Override
	public void registerRecipes(IRecipeRegistration reg) {
		//Alembic
		List<AlembicRecipe> alemb = new ArrayList<AlembicRecipe>();
		alemb.add(new AlembicRecipe());
		reg.addRecipes(alemb, AlembicRecipeContext.NAME);
		
		//ARS
		List<ARSRecipe> ars = new ArrayList<ARSRecipe>();
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.isIn(TardisBlockTags.ARS))
				ars.add(new ARSRecipe(new ItemStack(block)));
		}
		reg.addRecipes(ars, ARSRecipeCategory.NAME);
		
		//Weld
		reg.addRecipes(WeldRecipe.WELD_RECIPE, WeldRecipeCategory.NAME);
	}

}
