package net.tardis.mod.compat.jei;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.events.CommonEvents;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisItemTags;

public class AlembicRecipe implements IRecipe<AlembicRecipeWrapper>{

	public static final ResourceLocation name = new ResourceLocation(Tardis.MODID, "mercury");
	
	@Override
	public boolean matches(AlembicRecipeWrapper inv, World worldIn) {
		return inv.getStackInSlot(2).getItem().isIn(TardisItemTags.CINNABAR);
	}

	@Override
	public ItemStack getCraftingResult(AlembicRecipeWrapper inv) {
		return new ItemStack(TItems.MERCURY_BOTTLE);
	}

	@Override
	public boolean canFit(int width, int height) {
		return true;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return new ItemStack(TItems.MERCURY_BOTTLE);
	}

	@Override
	public ResourceLocation getId() {
		return name;
	}

	@Override
	public IRecipeSerializer<?> getSerializer() {
		return null;
	}

	@Override
	public IRecipeType<?> getType() {
		return CommonEvents.ALEMBIC;
	}

}
